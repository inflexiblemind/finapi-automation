import org.testng.annotations.Test;
import org.testng.internal.ClassHelper;
import org.testng.internal.PackageUtils;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Set;

/**
 * Created by Sergey.Syomin on 10/10/2018.
 */
public class MethodsNames {

    public static void main(String[] args) throws IOException, ClassNotFoundException {
        String packageName = "core";
        String[] testClasses =
                PackageUtils.findClassesInPackage(packageName, new
                        ArrayList<String>(), new ArrayList<String>());
        ClassLoader cl = Thread.currentThread().getContextClassLoader();
        for (String eachClass : testClasses){
            Class currentClass = cl.loadClass(eachClass);
            System.out.println("Test Suite Name = " + currentClass.getName().replace(packageName + ".", ""));
            Set<Method> allMethods = ClassHelper.getAvailableMethods(currentClass);
            Iterator<Method> iMethods = allMethods.iterator();
            while (iMethods.hasNext()){
                Method eachMethod = iMethods.next();
                Test test = eachMethod.getAnnotation(Test.class);
                if (test != null){
                    System.out.println("Test Name = " + eachMethod.getName());
                }
            }
        }
    }

}
