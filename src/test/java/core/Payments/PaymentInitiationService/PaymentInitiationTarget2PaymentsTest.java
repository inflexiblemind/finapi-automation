package PaymentInitiationService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.util.Arrays;
import java.util.Map;

import static core.helpers.Actions.*;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

/**
 * Created by Sergey.Syomin on 12/4/2018.
 */

@Listeners({TestExecutor.class})
public class PaymentInitiationTarget2PaymentsTest extends TestBase{

    private String product = "target-2-payments";
    private String ERROR_FIELD = "code";
    private String paymentIdMin;
    private String paymentIdMax;
    private String uuid;
    private String PSU_ID = "aspsp2";
    private Response response;
    private String basePath;

    @BeforeClass
    public void setup() {
        basePath = "/v1/payments";
        RestAssured.basePath = basePath;
    }

    @BeforeMethod
    public void setPath(){
        setup();
    }






    /*********************
     *
     *  INITIATE PAYMENT
     *
     **********************/






    @Features("3 C. Payment Initiation Service - Target 2 Payments")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Target 2 Payments Payment for PSU")
    @Test()
    public void initiateTarget2PaymentsPaymentMaxValuesTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/target-2-payments/paymentInitiateTarget2PaymentsMaxValues.json")).
                when().post(product).
                then().extract().response();

        paymentIdMax = response.then().statusCode(201).extract().path("paymentId");

        response.then().
                header("X-Request-ID", equalTo(uuid)).
                header("Location", equalTo(basePath + "/" + paymentIdMax)).
                header("Aspsp-Sca-Approach", notNullValue()).
                body("transactionStatus", equalTo("RCVD")).
                body("paymentId", notNullValue()).
                body("_links.startAuthorisation", equalTo(basePath + "/" + product + "/" + paymentIdMax + "/authorisations"));
    }

    @Features("3 C. Payment Initiation Service - Target 2 Payments")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Target 2 Payments Payment for PSU Schema Conform Test")
    @Test()
    public void initiateTarget2PaymentsPaymentMaxValuesSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/target-2-payments/paymentInitiateTarget2PaymentsMaxValues.json")).
                when().post(product).
                then().statusCode(201).
                body(matchesJsonSchema(new File("src/main/resources/schemas/payment/target-2-payments/Target2PaymentsPaymentInitiation.json")));
    }

    @Features("3 C. Payment Initiation Service - Target 2 Payments")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Target 2 Payments Payment for PSU")
    @Test()
    public void initiateTarget2PaymentsPaymentMinValuesTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/target-2-payments/paymentInitiateTarget2PaymentsMinValues.json")).
                when().post(product).
                then().extract().response();

        paymentIdMin = response.then().statusCode(201).extract().path("paymentId");

        response.then().
                header("X-Request-ID", equalTo(uuid)).
                header("Location", equalTo(basePath + "/" + paymentIdMin)).
                header("Aspsp-Sca-Approach", notNullValue()).
                body("transactionStatus", equalTo("RCVD")).
                body("paymentId", notNullValue()).
                body("_links.startAuthorisation", equalTo(basePath + "/" + product + "/" + paymentIdMin + "/authorisations"));
    }

    @Features("3 C. Payment Initiation Service - Target 2 Payments")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Target 2 Payments Payment for PSU Schema Conform Test")
    @Test()
    public void initiateTarget2PaymentsPaymentMinValuesSchemaConformTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/target-2-payments/paymentInitiateTarget2PaymentsMinValues.json")).
                when().post(product).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/payment/target-2-payments/Target2PaymentsPaymentInitiation.json")));
    }

    /*************************
     *
     *  GET PAYMENT DETAILS
     *
     *************************/







    @Features("3 C. Payment Initiation Service - Target 2 Payments")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Payment Initiation Service - Get Target 2 Payments Payment Maximum Values Details")
    @Test(dependsOnMethods = {"initiateTarget2PaymentsPaymentMaxValuesTest"})
    public void getTarget2PaymentsPaymentMaxValuesDetailsTest() {

        uuid = uuid();
        Map json = convertFileToHashMap("src/main/resources/payment/target-2-payments/paymentInitiateTarget2PaymentsMaxValues.json");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                body("debtorAccount", equalTo(json.get("debtorAccount"))).
                body("creditorAccount", equalTo(json.get("creditorAccount"))).
                body("instructedAmount.currency", equalTo(((Map)json.get("instructedAmount")).get("currency"))).
                body("instructedAmount.amount", equalTo((Float.parseFloat((String)(((Map)json.get("instructedAmount")).get("amount")))))).
                body("creditorName", equalTo(json.get("creditorName")));
    }

    @Features("3 C. Payment Initiation Service - Target 2 Payments")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Payment Initiation Service - Get Target 2 Payments Payment Maximum Values Details Schema Conform Test")
    @Test(dependsOnMethods = {"initiateTarget2PaymentsPaymentMaxValuesTest"})
    public void getTarget2PaymentsPaymentMaxValuesDetailsSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/payment/target-2-payments/Target2PaymentsPaymentDetails.json")));
    }

    @Features("3 C. Payment Initiation Service - Target 2 Payments")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Payment Initiation Service - Get Target 2 Payments Payment Minimum Values Details")
    @Test(dependsOnMethods = {"initiateTarget2PaymentsPaymentMinValuesTest"})
    public void getTarget2PaymentsPaymentMinValuesDetailsTest() {

        uuid = uuid();
        Map json = convertFileToHashMap("src/main/resources/payment/target-2-payments/paymentInitiateTarget2PaymentsMinValues.json");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                body("debtorAccount", equalTo(json.get("debtorAccount"))).
                body("creditorAccount", equalTo(json.get("creditorAccount"))).
                body("instructedAmount.currency", equalTo(((Map)json.get("instructedAmount")).get("currency"))).
                body("instructedAmount.amount", equalTo((Float.parseFloat((String)(((Map)json.get("instructedAmount")).get("amount")))))).
                body("creditorName", equalTo(json.get("creditorName")));
    }

    @Features("3 C. Payment Initiation Service - Target 2 Payments")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Payment Initiation Service - Get Target 2 Payments Payment Minimum Values Details Schema Conform Test")
    @Test(dependsOnMethods = {"initiateTarget2PaymentsPaymentMinValuesTest"})
    public void getTarget2PaymentsPaymentMinValuesDetailsSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/payment/target-2-payments/Target2PaymentsPaymentDetails.json")));
    }




    /********************************
     *
     *    GET TRANSACTION STATUS
     *
     *******************************/




    @Features("3 C. Payment Initiation Service - Target 2 Payments")
    @Stories({"3. Payment Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of Target 2 Payments Payment with Maximum Values by Id")
    @Test(dependsOnMethods = {"initiateTarget2PaymentsPaymentMaxValuesTest"})
    public void getTarget2PaymentsPaymentMaxValuesStatusTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD"));
    }

    @Features("3 C. Payment Initiation Service - Target 2 Payments")
    @Stories({"3. Payment Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of Target 2 Payments Payment with Maximum Values by Id Schema Conform Test")
    @Test(dependsOnMethods = {"initiateTarget2PaymentsPaymentMaxValuesTest"})
    public void getTarget2PaymentsPaymentMaxValuesStatusSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/payment/target-2-payments/Target2PaymentsPaymentStatus.json")));
    }

    @Features("3 C. Payment Initiation Service - Target 2 Payments")
    @Stories({"3. Payment Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of Target 2 Payments Payment with Minimum Values by Id")
    @Test(dependsOnMethods = {"initiateTarget2PaymentsPaymentMinValuesTest"})
    public void getTarget2PaymentsPaymentMinValuesStatusTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD"));
    }

    @Features("3 C. Payment Initiation Service - Target 2 Payments")
    @Stories({"3. Payment Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of Target 2 Payments Payment with Minimum Values by Id Schema Conform Test")
    @Test(dependsOnMethods = {"initiateTarget2PaymentsPaymentMinValuesTest"})
    public void getTarget2PaymentsPaymentMinValuesStatusSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/payment/target-2-payments/Target2PaymentsPaymentStatus.json")));
    }




    /*************************
     *
     *    DELETE PAYMENT
     *
     *************************/



    @Features("3 C. Payment Initiation Service - Target 2 Payments")
    @Stories({"4. Delete Payment"})
    @TestCaseId("Delete Payment - Delete Target 2 Payments Payment with Maximum Values While SCA not finished")
    @Test(alwaysRun = true, dependsOnMethods = {"initiateTarget2PaymentsPaymentMaxValuesTest", "getTarget2PaymentsPaymentMaxValuesDetailsTest","getTarget2PaymentsPaymentMaxValuesStatusTest"})
    public void deleteTarget2PaymentsPaymentWithMaxValuesTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", paymentIdMax).
                when().delete(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid));

        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                body("transactionStatus", equalTo("CANC"));

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+ paymentIdMax +" doesn't exist.")));
    }

    @Features("3 C. Payment Initiation Service - Target 2 Payments")
    @Stories({"4. Delete Payment"})
    @TestCaseId("Delete Payment - Delete Target 2 Payments Payment with Minimum Values While SCA not finished")
    @Test(alwaysRun = true, dependsOnMethods = {"initiateTarget2PaymentsPaymentMinValuesTest", "getTarget2PaymentsPaymentMinValuesDetailsTest","getTarget2PaymentsPaymentMinValuesStatusTest"})
    public void deleteTarget2PaymentsPaymentWithMinValuesTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", paymentIdMin).
                when().delete(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid));

        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                body("transactionStatus", equalTo("CANC"));

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+ paymentIdMin +" doesn't exist.")));
    }

}
