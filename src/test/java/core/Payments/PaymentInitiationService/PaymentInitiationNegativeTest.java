package PaymentInitiationService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.util.Arrays;

import static core.helpers.Actions.date;
import static core.helpers.Actions.uuid;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

/**
 * Created by Sergey.Syomin on 1/10/2019.
 */
@Listeners({TestExecutor.class})
public class PaymentInitiationNegativeTest extends TestBase {

    private String productInstatntSepaCrTr = "instant-sepa-credit-transfers";
    private String productCrossBorgerCrTr = "cross-border-credit-transfers";
    private String productSepaCrTr = "sepa-credit-transfers";
    private String productTarget2Payment = "target-2-payments";
    private String ERROR_FIELD = "code";
    private String paymentId;
    private String cancelledPaymentIdInstatntSepaCrTr;
    private String cancelledPaymentCrossBorgerCrTr;
    private String cancelledPaymentSepaCrTr;
    private String cancelledTarget2Payment;
    private String uuid;
    private String PSU_ID = "aspsp2";
    private String unknown_PSU_ID = "aspsp5";
    private Response response;
    private String basePath;

    @BeforeClass
    public void setup() {
        basePath = "/v1/payments";
        RestAssured.basePath = basePath;
    }

    @BeforeMethod
    public void setPath(){
        setup();
    }

    /**
     * Instant Sepa Credit Transfer
     */

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Payment for PSU missing Mandatory Field Negative Test")
    @Test()
    public void initiateInstantSepaPaymentMissingMandatoryFieldNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaMissingMandatoryFields.json")).
                when().post(productInstatntSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text",equalTo(Arrays.asList("Invalid null value submitted for creditorName")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Payment for PSU Unsupported field Negative Test")
    @Test()
    public void initiateInstantSepaPaymentUnsupportedFieldNegativeTest() {
        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaUnsupportedFields.json")).
                when().post(productInstatntSepaCrTr).
                then().statusCode(201).extract().response();

        paymentId = response.then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                extract().path("paymentId");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentId).
                when().get(productInstatntSepaCrTr + "/{paymentId}").
                then().statusCode(200).
                body("transactionStatus", equalTo("RCVD")).
                body("chargeBearer",nullValue()).
                body(not("unsupportedField"));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Payment Without Currency Negative Test")
    @Test()
    public void initiateInstantSepaPaymentMissingCurrencyNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaWithoutCurrency.json")).
                when().post(productInstatntSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text",equalTo(Arrays.asList("Invalid null value submitted for instructedAmount.currency")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"4. Delete Payment"})
    @TestCaseId("Delete Payment - Delete Non Existing Instant Sepa Payment Negative Test \n https://pmc.specific-group.at/confluence/pages/viewpage.action?pageId=111904579")
    @Test()
    public void deleteNoneExistingInstantSepaPaymentNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                pathParam("paymentId", uuid).
                when().delete(productInstatntSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Payment Initiation Service - Get Instant Sepa Payment Details of a non-existent payment Negative Test")
    @Test
    public void getInstantSepaPaymentDetailsNonExistentPaymentNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", uuid).
                when().get(productInstatntSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Payment Initiation Service - Get Instant Sepa Payment Details of a cancelled payment Negative Test")
    @Test
    public void getInstantSepaPaymentDetailsCancelledPaymentNegativeTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaMaxValues.json")).
                when().post(productInstatntSepaCrTr).
                then().extract().response();
        cancelledPaymentIdInstatntSepaCrTr = response.then().statusCode(201).extract().path("paymentId");
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", cancelledPaymentIdInstatntSepaCrTr).
                when().delete(productInstatntSepaCrTr + "/{paymentId}").
                then().statusCode(200);

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", cancelledPaymentIdInstatntSepaCrTr).
                when().get(productInstatntSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+cancelledPaymentIdInstatntSepaCrTr+" doesn't exist.")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Payment product is not supported")
    @Test()
    public void initiatePaymentProductIsNotSupportedTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaMaxValues.json")).
                when().post(uuid).
                then().statusCode(404).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("PRODUCT_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment product "+uuid+" is not supported.")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Payment Invalid format of amount Negative Test")
    @Test()
    public void initiateInstantSepaPaymentInvalidFormatAmountNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaInvalidFormatAmount.json")).
                when().post(productInstatntSepaCrTr).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus",equalTo("RCVD"));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Payment missing UUID Negative Test")
    @Test()
    public void initiateInstantSepaPaymentMissingUUIDNegativeTest() {

        given().header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaMinValues.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Mandatory header 'X-Request-ID' is missing or empty.")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Payment missing PSU_IP Negative Test")
    @Test()
    public void initiateInstantSepaPaymentMissingPSUIPNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaMinValues.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Format of certain request fields are not matching the XS2A requirements.")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Payment Missing PSU_ID Negative Test")
    @Test()
    public void initiateInstantSepaPaymentMissingPSUIDNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaMinValues.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Format of certain request fields are not matching the XS2A requirements.")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Payment unknown PSU_ID Negative Test")
    @Test()
    public void initiateInstantSepaPaymentUnknownPSUIDNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", unknown_PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaMinValues.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Format of certain request fields are not matching the XS2A requirements.")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Payment using PUT method Negative Test")
    @Test()
    public void initiateInstantSepaPaymentUsingPutMethodNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaMinValues.json")).
                when().put(productSepaCrTr).
                then().statusCode(405).
                body("error",equalTo("Method Not Allowed")).
                body("message",equalTo("Request method 'PUT' not supported")).
                body("path",equalTo("/v1/payments/sepa-credit-transfers"));
    }




    /*******************************************************************
     * Cross Border Credit Transfers
     *******************************************************************/







    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Cross Border Credit Transfers Payment for PSU Missing Mandatory Field Negative Test")
    @Test()
    public void initiateCrossBorderPaymentMissingMandatoryFieldNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/cross-border-credit-transfers/paymentInitiateCrossBorderMissingMandatoryFields.json")).
                when().post(productCrossBorgerCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for creditorName")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Cross Border Credit Transfers Payment for PSU Unsupported Field Negative Test")
    @Test()
    public void initiateCrossBorderPaymentUnsupportedFieldNegativeTest() {
        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/cross-border-credit-transfers/paymentInitiateCrossBorderUnsupportedFields.json")).
                when().post(productCrossBorgerCrTr);

        paymentId = response.then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                extract().path("paymentId");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentId).
                when().get(productCrossBorgerCrTr + "/{paymentId}").
                then().statusCode(200).
                body("transactionStatus", equalTo("RCVD")).
                body("endToEndIdentification",nullValue()).
                body(not("vasia"));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Cross Border Credit Transfers Payment Without Currency Negative Test")
    @Test()
    public void initiateCrossBorderPaymentMissingCurrencyNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/cross-border-credit-transfers/paymentInitiateCrossBorderWithoutCurrency.json")).
                when().post(productCrossBorgerCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for instructedAmount.currency")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"4. Delete Payment"})
    @TestCaseId("Delete Payment - Delete Non Existing Cross Border Credit Transfers Payment Negative Test \n https://pmc.specific-group.at/confluence/pages/viewpage.action?pageId=111904579")
    @Test()
    public void deleteNoneExistingPaymentNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                pathParam("paymentId", uuid).
                when().delete(productCrossBorgerCrTr + "/{paymentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }


    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Payment Initiation Service - Get Cross Border Credit Transfer Payment Details of a cancelled payment Negative Test")
    @Test
    public void getCrossBorderCreditTransferPaymentDetailsCancelledPaymentNegativeTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/cross-border-credit-transfers/paymentInitiateCrossBorderMaxValues.json")).
                when().post(productCrossBorgerCrTr).
                then().extract().response();
        cancelledPaymentCrossBorgerCrTr = response.then().statusCode(201).extract().path("paymentId");
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", cancelledPaymentCrossBorgerCrTr).
                when().delete(productCrossBorgerCrTr + "/{paymentId}").
                then().statusCode(200);

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", cancelledPaymentCrossBorgerCrTr).
                when().get(productCrossBorgerCrTr + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+cancelledPaymentCrossBorgerCrTr+" doesn't exist.")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Cross Border Credit Transfer Payment Invalid format of amount Negative Test")
    @Test()
    public void initiateCrossBorderCreditTransfersPaymentInvalidFormatAmountNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/cross-border-credit-transfers/paymentInitiateCrossBorderInvalidFormatAmount.json")).
                when().post(productCrossBorgerCrTr).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus",equalTo("RCVD"));
    }





    /******************************************************
     * Sepa Credit Transfer
    ******************************************************/





    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Sepa Credit Transfer Payment with Missing mandatory field Negative Test")
    @Test()
    public void initiateSepaPaymentMissingMandatoryFieldNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/sepa-credit-transfer/paymentInitiateSepaMissingMandatoryFields.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for creditorName")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Sepa Credit Transfer Payment with Unsupported field Negative Test")
    @Test()
    public void initiateSepaPaymentUnsupportedFieldNegativeTest() {
        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/sepa-credit-transfer/paymentInitiateSepaUnsupportedFields.json")).
                when().post(productSepaCrTr);

        paymentId = response.then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                extract().path("paymentId");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentId).
                when().get(productSepaCrTr + "/{paymentId}").
                then().statusCode(200).
                body("transactionStatus", equalTo("RCVD")).
                body("chargeBearer",nullValue()).
                body(not("vasia"));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Sepa Credit Transfer Payment with Long EndToEndIdentification field Negative Test")
    @Test()
    public void initiateSepaPaymentLongEndToEndIdentificationNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/sepa-credit-transfer/paymentInitiateSepaLongEndToEntIdentification.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid E2EID0123456789012345678901234567890 value submitted for endToEndIdentification")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Payment for None Existing Product Negative Test")
    @Test()
    public void initiatePaymentForNoneExistingProductNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/sepa-credit-transfer/paymentInitiateSepaMinValues.json")).
                when().post("domestos").
                then().statusCode(404).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("PRODUCT_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment product domestos is not supported.")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Sepa Credit Transfer Payment Without Currency Negative Test")
    @Test()
    public void initiateSepaPaymentMissingCurrencyNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/sepa-credit-transfer/paymentInitiateSepaWithoutCurrency.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for instructedAmount.currency")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Payment Initiation Service - Get None Existing Sepa Payment Details Negative Test")
    @Test()
    public void getNoneExistingSepaPaymentDetailsNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).
                pathParam("paymentId", uuid).
                when().get(productSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"3. Payment Status Request"})
    @TestCaseId("Payment Initiation Service - Get None Existing Sepa Payment Status Negative Test")
    @Test()
    public void getNoneExistingSepaPaymentStatusNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).
                pathParam("paymentId", uuid).
                when().get(productSepaCrTr + "/{paymentId}/status").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"4. Delete Payment"})
    @TestCaseId("Delete Payment - Delete Non Existing Sepa Credit Transfer Payment Negative Test \n https://pmc.specific-group.at/confluence/pages/viewpage.action?pageId=111904579")
    @Test()
    public void deleteNoneExistingSepaCrTrPaymentSepaCrTrNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                pathParam("paymentId", uuid).
                when().delete(productSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Payment Initiation Service - Get Sepa Credit Transfer Payment Details of a cancelled payment Negative Test")
    @Test
    public void getSepaCreditTransferPaymentDetailsCancelledPaymentNegativeTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/sepa-credit-transfer/paymentInitiateSepaMaxValues.json")).
                when().post(productSepaCrTr).
                then().extract().response();
        cancelledPaymentSepaCrTr = response.then().statusCode(201).extract().path("paymentId");
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", cancelledPaymentSepaCrTr).
                when().delete(productSepaCrTr + "/{paymentId}").
                then().statusCode(200);

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", cancelledPaymentSepaCrTr).
                when().get(productSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+cancelledPaymentSepaCrTr+" doesn't exist.")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Sepa Credit Transfer Payment Invalid format of amount Negative Test")
    @Test()
    public void initiateSepaCreditTransfersPaymentInvalidFormatAmountNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/sepa-credit-transfer/paymentInitiateSepaInvalidFormatAmmount.json")).
                when().post(productSepaCrTr).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus",equalTo("RCVD"));
    }





    /***********************************************************************************
     * Target 2 Payments
     ***********************************************************************************/





    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Target 2 Payments Payment Missing Mandatory Field Negative Test")
    @Test()
    public void initiateTarget2PaymentsPaymentMissingMandatoryFieldNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/target-2-payments/paymentInitiateTarget2PaymentsMissingMandatoryFields.json")).
                when().post(productTarget2Payment).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for creditorName")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Target 2 Payments Payment Unsupported Field Negative Test")
    @Test()
    public void initiateTarget2PaymentsPaymentUnsupportedFieldNegativeTest() {

        uuid = uuid();

        paymentId = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/target-2-payments/paymentInitiateTarget2PaymentsUnsupportedFields.json")).
                when().post(productTarget2Payment).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                extract().path("paymentId");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentId).
                when().get(productTarget2Payment + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                body("vasia", nullValue());
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Target 2 Payments Payment for PSU Without Currency Negative Test")
    @Test()
    public void initiateTarget2PaymentsPaymentMissingCurrencyNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/target-2-payments/paymentInitiateTarget2PaymentsWithoutCurrency.json")).
                when().post(productTarget2Payment).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for instructedAmount.currency")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"4. Delete Payment"})
    @TestCaseId("Delete Payment - Delete Non Existing Target 2 Payments Payment Negative Test \n https://pmc.specific-group.at/confluence/pages/viewpage.action?pageId=111904579")
    @Test()
    public void deleteNoneExistingTarget2PaymentNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                pathParam("paymentId", uuid).
                when().delete(productTarget2Payment + "/{paymentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Payment Initiation Service - Get Target 2 Payment Details of a cancelled payment Negative Test")
    @Test
    public void getTarget2PaymentDetailsCancelledPaymentNegativeTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/target-2-payments/paymentInitiateTarget2PaymentsMaxValues.json")).
                when().post(productTarget2Payment).
                then().extract().response();
        cancelledTarget2Payment = response.then().statusCode(201).extract().path("paymentId");
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", cancelledTarget2Payment).
                when().delete(productTarget2Payment + "/{paymentId}").
                then().statusCode(200);

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", cancelledTarget2Payment).
                when().get(productTarget2Payment + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+cancelledTarget2Payment+" doesn't exist.")));
    }

    @Features("3 E. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Target 2 Payment Invalid format of amount Negative Test")
    @Test()
    public void initiateTarget2PaymentInvalidFormatAmountNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/target-2-payments/paymentInitiateTarget2PaymentInvalidFormatAmmount.json")).
                when().post(productTarget2Payment).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus",equalTo("RCVD"));
    }

}
