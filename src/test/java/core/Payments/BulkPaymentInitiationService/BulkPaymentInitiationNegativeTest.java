
package BulkPaymentInitiationService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.util.Arrays;

import static core.helpers.Actions.date;
import static core.helpers.Actions.uuid;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;



public class BulkPaymentInitiationNegativeTest extends TestBase {

    private String productInstatntSepaCrTr = "instant-sepa-credit-transfers";
    private String productCrossBorgerCrTr = "cross-border-credit-transfers";
    private String productSepaCrTr = "sepa-credit-transfers";
    private String productTarget2Payment = "target-2-payments";
    private String ERROR_FIELD = "code";
    private String paymentId;
    private String cancelledPaymentIdInstatntSepaCrTr;
    private String cancelledPaymentCrossBorgerCrTr;
    private String cancelledPaymentSepaCrTr;
    private String cancelledTarget2Payment;
    private String uuid;
    private String PSU_ID = "aspsp2";
    private String unknown_PSU_ID = "aspsp5";
    private Response response;
    private String basePath;

    @BeforeClass
    public void setup() {
        basePath = "/v1/bulk-payments";
        RestAssured.basePath = basePath;
    }

    @BeforeMethod
    public void setPath(){
        setup();
    }

    
    /**
     * Instant Sepa Credit Transfer
     */


    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Bulk Payment for PSU missing Mandatory Field Negative Test")
    @Test()
    public void initiateInstantSepaBulkPaymentMissingMandatoryFieldNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/instant-sepa-credit-transfer/bulkPaymentInitiateInstantSepaMissingMandatoryFields.json")).
                when().post(productInstatntSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text",equalTo(Arrays.asList("Invalid null value submitted for creditorName")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Bulk Payment for PSU Unsupported field Negative Test")
    @Test()
    public void initiateInstantSepaBulkPaymentUnsupportedFieldNegativeTest() {
        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/instant-sepa-credit-transfer/bulkPaymentInitiateInstantSepaUnsupportedFields.json")).
                when().post(productInstatntSepaCrTr).
                then().statusCode(201).extract().response();

        paymentId = response.then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                extract().path("paymentId");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentId).
                when().get(productInstatntSepaCrTr + "/{paymentId}").
                then().statusCode(200).
                body("transactionStatus", equalTo("RCVD")).
                body("chargeBearer",nullValue()).
                body(not("unsupportedField"));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Bulk Payment Without Currency Negative Test")
    @Test()
    public void initiateInstantSepaBulkPaymentMissingCurrencyNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/instant-sepa-credit-transfer/bulkPaymentInitiateInstantSepaWithoutCurrency.json")).
                when().post(productInstatntSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text",equalTo(Arrays.asList("Invalid null value submitted for instructedAmount.currency")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"4. Delete Payment"})
    @TestCaseId("Delete Payment - Delete Non Existing Instant Sepa Bulk Payment Negative Test \n https://pmc.specific-group.at/confluence/pages/viewpage.action?pageId=111904579")
    @Test()
    public void deleteNoneExistingInstantSepaBulkPaymentNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                pathParam("paymentId", uuid).
                when().delete(productInstatntSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Payment Initiation Service - Get Instant Sepa Bulk Payment Details of a non-existent payment Negative Test")
    @Test
    public void getInstantSepaBulkPaymentDetailsNonExistentPaymentNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", uuid).
                when().get(productInstatntSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Payment Initiation Service - Get Instant Sepa Bulk Payment Details of a cancelled payment Negative Test")
    @Test
    public void getInstantSepaBulkPaymentDetailsCancelledPaymentNegativeTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/instant-sepa-credit-transfer/bulkPaymentInitiateInstantSepaMaxValues.json")).
                when().post(productInstatntSepaCrTr).
                then().extract().response();
        cancelledPaymentIdInstatntSepaCrTr = response.then().statusCode(201).extract().path("paymentId");
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", cancelledPaymentIdInstatntSepaCrTr).
                when().delete(productInstatntSepaCrTr + "/{paymentId}").
                then().statusCode(200);

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", cancelledPaymentIdInstatntSepaCrTr).
                when().get(productInstatntSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+cancelledPaymentIdInstatntSepaCrTr+" doesn't exist.")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Bulk Payment product is not supported")
    @Test()
    public void initiateBulkPaymentProductIsNotSupportedTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/instant-sepa-credit-transfer/bulkPaymentInitiateInstantSepaMaxValues.json")).
                when().post(uuid).
                then().statusCode(404).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("PRODUCT_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment product "+uuid+" is not supported.")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Bulk Payment Invalid format of amount Negative Test")
    @Test()
    public void initiateInstantSepaBulkPaymentInvalidFormatAmountNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/instant-sepa-credit-transfer/bulkPaymentInitiateInstantSepaInvalidFormatAmount.json")).
                when().post(productInstatntSepaCrTr).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus",equalTo("RCVD"));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Bulk Payment missing UUID Negative Test")
    @Test()
    public void initiateInstantSepaBulkPaymentMissingUUIDNegativeTest() {

        given().header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/instant-sepa-credit-transfer/bulkPaymentInitiateInstantSepaMinValues.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Mandatory header 'X-Request-ID' is missing or empty.")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Bulk Payment missing PSU_IP Negative Test")
    @Test()
    public void initiateInstantSepaBulkPaymentMissingPSUIPNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/instant-sepa-credit-transfer/bulkPaymentInitiateInstantSepaMinValues.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Format of certain request fields are not matching the XS2A requirements.")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Bulk Payment Missing PSU_ID Negative Test")
    @Test()
    public void initiateInstantSepaBulkPaymentMissingPSUIDNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/instant-sepa-credit-transfer/bulkPaymentInitiateInstantSepaMinValues.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Format of certain request fields are not matching the XS2A requirements.")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Bulk Payment unknown PSU_ID Negative Test")
    @Test()
    public void initiateInstantSepaBulkPaymentUnknownPSUIDNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", unknown_PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/instant-sepa-credit-transfer/bulkPaymentInitiateInstantSepaMinValues.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Format of certain request fields are not matching the XS2A requirements.")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Instant Sepa Credit Transfer Bulk Payment using PUT method Negative Test")
    @Test()
    public void initiateInstantSepaBulkPaymentUsingPutMethodNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/instant-sepa-credit-transfer/bulkPaymentInitiateInstantSepaMinValues.json")).
                when().put(productSepaCrTr).
                then().statusCode(405).
                body("error",equalTo("Method Not Allowed")).
                body("message",equalTo("Request method 'PUT' not supported")).
                body("path",equalTo("/v1/payments/sepa-credit-transfers"));
    }




    
/*******************************************************************
     * Cross Border Credit Transfers
     *******************************************************************/








    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Cross Border Credit Transfers Bulk Payment for PSU Missing Mandatory Field Negative Test")
    @Test()
    public void initiateCrossBorderBulkPaymentMissingMandatoryFieldNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/cross-border-credit-transfers/bulkPaymentInitiateCrossBorderMissingMandatoryFields.json")).
                when().post(productCrossBorgerCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for creditorName")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Cross Border Credit Transfers Bulk Payment for PSU Unsupported Field Negative Test")
    @Test()
    public void initiateCrossBorderBulkPaymentUnsupportedFieldNegativeTest() {
        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/cross-border-credit-transfers/bulkPaymentInitiateCrossBorderUnsupportedFields.json")).
                when().post(productCrossBorgerCrTr);

        paymentId = response.then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                extract().path("paymentId");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentId).
                when().get(productCrossBorgerCrTr + "/{paymentId}").
                then().statusCode(200).
                body("transactionStatus", equalTo("RCVD")).
                body("endToEndIdentification",nullValue()).
                body(not("vasia"));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Cross Border Credit Transfers Bulk Payment Without Currency Negative Test")
    @Test()
    public void initiateCrossBorderBulkPaymentMissingCurrencyNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/cross-border-credit-transfers/bulkPaymentInitiateCrossBorderWithoutCurrency.json")).
                when().post(productCrossBorgerCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for instructedAmount.currency")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"4. Delete Payment"})
    @TestCaseId("Delete Payment - Delete Non Existing Cross Border Credit Transfers Bulk Payment Negative Test \n https://pmc.specific-group.at/confluence/pages/viewpage.action?pageId=111904579")
    @Test()
    public void deleteNoneExistingBulkPaymentNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                pathParam("paymentId", uuid).
                when().delete(productCrossBorgerCrTr + "/{paymentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }


    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Payment Initiation Service - Get Cross Border Credit Transfer Bulk Payment Details of a cancelled payment Negative Test")
    @Test
    public void getCrossBorderCreditTransferBulkPaymentDetailsCancelledPaymentNegativeTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/cross-border-credit-transfers/bulkPaymentInitiateCrossBorderMaxValues.json")).
                when().post(productCrossBorgerCrTr).
                then().extract().response();
        cancelledPaymentCrossBorgerCrTr = response.then().statusCode(201).extract().path("paymentId");
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", cancelledPaymentCrossBorgerCrTr).
                when().delete(productCrossBorgerCrTr + "/{paymentId}").
                then().statusCode(200);

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", cancelledPaymentCrossBorgerCrTr).
                when().get(productCrossBorgerCrTr + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+cancelledPaymentCrossBorgerCrTr+" doesn't exist.")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Cross Border Credit Transfer Bulk Payment Invalid format of amount Negative Test")
    @Test()
    public void initiateCrossBorderCreditTransfersBulkPaymentInvalidFormatAmountNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/cross-border-credit-transfers/bulkPaymentInitiateCrossBorderInvalidFormatAmount.json")).
                when().post(productCrossBorgerCrTr).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus",equalTo("RCVD"));
    }





    
/******************************************************
     * Sepa Credit Transfer
    ******************************************************/






    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Sepa Credit Transfer Bulk Payment with Missing mandatory field Negative Test")
    @Test()
    public void initiateSepaBulkPaymentMissingMandatoryFieldNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/sepa-credit-transfer/bulkPaymentInitiateSepaMissingMandatoryFields.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for creditorName")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Sepa Credit Transfer Bulk Payment with Unsupported field Negative Test")
    @Test()
    public void initiateSepaBulkPaymentUnsupportedFieldNegativeTest() {
        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/sepa-credit-transfer/bulkPaymentInitiateSepaUnsupportedFields.json")).
                when().post(productSepaCrTr);

        paymentId = response.then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                extract().path("paymentId");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentId).
                when().get(productSepaCrTr + "/{paymentId}").
                then().statusCode(200).
                body("transactionStatus", equalTo("RCVD")).
                body("chargeBearer",nullValue()).
                body(not("vasia"));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Sepa Credit Transfer Bulk Payment with Long EndToEndIdentification field Negative Test")
    @Test()
    public void initiateSepaBulkPaymentLongEndToEndIdentificationNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/sepa-credit-transfer/bulkPaymentInitiateSepaLongEndToEntIdentification.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid E2EID0123456789012345678901234567890 value submitted for endToEndIdentification")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Bulk Payment for None Existing Product Negative Test")
    @Test()
    public void initiateBulkPaymentForNoneExistingProductNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/sepa-credit-transfer/bulkPaymentInitiateSepaMinValues.json")).
                when().post("domestos").
                then().statusCode(404).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("PRODUCT_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment product domestos is not supported.")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Sepa Credit Transfer Bulk Payment Without Currency Negative Test")
    @Test()
    public void initiateSepaBulkPaymentMissingCurrencyNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/sepa-credit-transfer/bulkPaymentInitiateSepaWithoutCurrency.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for instructedAmount.currency")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Payment Initiation Service - Get None Existing Sepa Bulk Payment Details Negative Test")
    @Test()
    public void getNoneExistingSepaBulkPaymentDetailsNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).
                pathParam("paymentId", uuid).
                when().get(productSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"3. Payment Status Request"})
    @TestCaseId("Payment Initiation Service - Get None Existing Sepa Bulk Payment Status Negative Test")
    @Test()
    public void getNoneExistingSepaBulkPaymentStatusNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).
                pathParam("paymentId", uuid).
                when().get(productSepaCrTr + "/{paymentId}/status").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"4. Delete Payment"})
    @TestCaseId("Delete Payment - Delete Non Existing Sepa Credit Transfer Bulk Payment Negative Test \n https://pmc.specific-group.at/confluence/pages/viewpage.action?pageId=111904579")
    @Test()
    public void deleteNoneExistingSepaCrTrBulkPaymentSepaCrTrNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                pathParam("paymentId", uuid).
                when().delete(productSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Payment Initiation Service - Get Sepa Credit Transfer Bulk Payment Details of a cancelled payment Negative Test")
    @Test
    public void getSepaCreditTransferBulkPaymentDetailsCancelledPaymentNegativeTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/sepa-credit-transfer/bulkPaymentInitiateSepaMaxValues.json")).
                when().post(productSepaCrTr).
                then().extract().response();
        cancelledPaymentSepaCrTr = response.then().statusCode(201).extract().path("paymentId");
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", cancelledPaymentSepaCrTr).
                when().delete(productSepaCrTr + "/{paymentId}").
                then().statusCode(200);

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", cancelledPaymentSepaCrTr).
                when().get(productSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+cancelledPaymentSepaCrTr+" doesn't exist.")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Sepa Credit Transfer Bulk Payment Invalid format of amount Negative Test")
    @Test()
    public void initiateSepaCreditTransfersBulkPaymentInvalidFormatAmountNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/sepa-credit-transfer/bulkPaymentInitiateSepaInvalidFormatAmmount.json")).
                when().post(productSepaCrTr).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus",equalTo("RCVD"));
    }





    
/***********************************************************************************
     * Target 2 Payments
     ***********************************************************************************/






    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Target 2 Payments Bulk Payment Missing Mandatory Field Negative Test")
    @Test()
    public void initiateTarget2PaymentsBulkPaymentMissingMandatoryFieldNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/target-2-payments/bulkPaymentInitiateTarget2PaymentsMissingMandatoryFields.json")).
                when().post(productTarget2Payment).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for creditorName")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Target 2 Payments Bulk Payment Unsupported Field Negative Test")
    @Test()
    public void initiateTarget2PaymentsBulkPaymentUnsupportedFieldNegativeTest() {

        uuid = uuid();

        paymentId = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/target-2-payments/bulkPaymentInitiateTarget2PaymentsUnsupportedFields.json")).
                when().post(productTarget2Payment).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                extract().path("paymentId");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentId).
                when().get(productTarget2Payment + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                body("vasia", nullValue());
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Target 2 Payments Bulk Payment for PSU Without Currency Negative Test")
    @Test()
    public void initiateTarget2PaymentsBulkPaymentMissingCurrencyNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/target-2-payments/bulkPaymentInitiateTarget2PaymentsWithoutCurrency.json")).
                when().post(productTarget2Payment).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for instructedAmount.currency")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"4. Delete Payment"})
    @TestCaseId("Delete Payment - Delete Non Existing Target 2 Payments Bulk Payment Negative Test \n https://pmc.specific-group.at/confluence/pages/viewpage.action?pageId=111904579")
    @Test()
    public void deleteNoneExistingTarget2PaymentBulkPaymentNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                pathParam("paymentId", uuid).
                when().delete(productTarget2Payment + "/{paymentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Payment Initiation Service - Get Target 2 Payment Details of a cancelled payment Negative Test")
    @Test
    public void getTarget2PaymentDetailsCancelledPaymentNegativeTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/target-2-payments/bulkPaymentInitiateTarget2PaymentsMaxValues.json")).
                when().post(productTarget2Payment).
                then().extract().response();
        cancelledTarget2Payment = response.then().statusCode(201).extract().path("paymentId");
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", cancelledTarget2Payment).
                when().delete(productTarget2Payment + "/{paymentId}").
                then().statusCode(200);

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", cancelledTarget2Payment).
                when().get(productTarget2Payment + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+cancelledTarget2Payment+" doesn't exist.")));
    }

    @Features("3 J. Payment Initiation Service - Negative")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Payment Initiation Service - Initiate Target 2 Payment Invalid format of amount Negative Test")
    @Test()
    public void initiateTarget2PaymentInvalidFormatAmountNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/paymentBulk/target-2-payments/bulkPaymentInitiateTarget2PaymentInvalidFormatAmmount.json")).
                when().post(productTarget2Payment).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus",equalTo("RCVD"));
    }

}
