
package BulkPaymentInitiationService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.util.Arrays;
import java.util.Map;

import static core.helpers.Actions.*;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;




public class BulkPaymentInitiationTarget2PaymentsTest extends TestBase{

    private String product = "target-2-payments";
    private String ERROR_FIELD = "code";
    private String paymentIdMin;
    private String paymentIdMax;
    private String uuid;
    private String PSU_ID = "aspsp2";
    private Response response;
    private String basePath;

    @BeforeClass
    public void setup() {
        basePath = "/v1/bulk-payments";
        RestAssured.basePath = basePath;
    }

    @BeforeMethod
    public void setPath(){
        setup();
    }







    /*********************
     *
     *  INITIATE PAYMENT
     *
     **********************/







    @Features("3 I. Bulk Payment Initiation Service - Target 2 Payments")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Bulk Payment Initiation Service - Initiate Target 2 Payments Bulk Payment for PSU")
    @Test()
    public void initiateTarget2PaymentsBulkPaymentMaxValuesTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(map("src/main/resources/paymentBulk/target-2-payments/bulkPaymentInitiateTarget2PaymentsMaxValues.json")).
                when().post(product).
                then().extract().response();

        paymentIdMax = response.then().statusCode(201).extract().path("paymentId");

        response.then().
                header("X-Request-ID", equalTo(uuid)).
                header("Location", equalTo(basePath + "/" + paymentIdMax)).
                header("Aspsp-Sca-Approach", notNullValue()).
                body("transactionStatus", equalTo("RCVD")).
                body("paymentId", notNullValue()).
                body("_links.startAuthorisation", equalTo(basePath + "/" + product + "/" + paymentIdMax + "/authorisations"));
    }

    @Features("3 I. Bulk Payment Initiation Service - Target 2 Payments")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Bulk Payment Initiation Service - Initiate Target 2 Payments Bulk Payment for PSU Schema Conform Test")
    @Test(enabled = false)
    public void initiateTarget2PaymentsBulkPaymentMaxValuesSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(map("src/main/resources/paymentBulk/target-2-payments/bulkPaymentInitiateTarget2PaymentsMaxValues.json")).
                when().post(product).
                then().statusCode(201).
                body(matchesJsonSchema(new File("src/main/resources/schemas/paymentBulk/target-2-payments/Target2PaymentsBulkPaymentInitiation.json")));
    }

    @Features("3 I. Bulk Payment Initiation Service - Target 2 Payments")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Bulk Payment Initiation Service - Initiate Target 2 Payments Bulk Payment for PSU")
    @Test()
    public void initiateTarget2PaymentsBulkPaymentMinValuesTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(map("src/main/resources/paymentBulk/target-2-payments/bulkPaymentInitiateTarget2PaymentsMinValues.json")).
                when().post(product).
                then().extract().response();

        paymentIdMin = response.then().statusCode(201).extract().path("paymentId");

        response.then().
                header("X-Request-ID", equalTo(uuid)).
                header("Location", equalTo(basePath + "/" + paymentIdMin)).
                header("Aspsp-Sca-Approach", notNullValue()).
                body("transactionStatus", equalTo("RCVD")).
                body("paymentId", notNullValue()).
                body("_links.startAuthorisation", equalTo(basePath + "/" + product + "/" + paymentIdMin + "/authorisations"));
    }

    @Features("3 I. Bulk Payment Initiation Service - Target 2 Payments")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Bulk Payment Initiation Service - Initiate Target 2 Payments Bulk Payment for PSU Schema Conform Test")
    @Test(enabled = false)
    public void initiateTarget2PaymentsBulkPaymentMinValuesSchemaConformTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(map("src/main/resources/paymentBulk/target-2-payments/bulkPaymentInitiateTarget2PaymentsMinValues.json")).
                when().post(product).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/paymentBulk/target-2-payments/Target2PaymentsBulkPaymentInitiation.json")));
    }


     /*************************
     *
     *  GET PAYMENT DETAILS
     *
     *************************/








    @Features("3 I. Bulk Payment Initiation Service - Target 2 Payments")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Bulk Payment Initiation Service - Get Target 2 Payments Bulk Payment Maximum Values Details")
    @Test(dependsOnMethods = {"initiateTarget2PaymentsBulkPaymentMaxValuesTest"})
    public void getTarget2PaymentsBulkPaymentMaxValuesDetailsTest() {

        uuid = uuid();
        Map json = convertFileToHashMap("src/main/resources/paymentBulk/target-2-payments/bulkPaymentInitiateTarget2PaymentsMaxValues.json");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                body("debtorAccount", equalTo(json.get("debtorAccount"))).
                body("payments", equalTo(json.get("payments")));

        /*        body("creditorAccount", equalTo(json.get("creditorAccount"))).
                body("instructedAmount.currency", equalTo(((Map)json.get("instructedAmount")).get("currency"))).
                body("instructedAmount.amount", equalTo((Float.parseFloat((String)(((Map)json.get("instructedAmount")).get("amount")))))).
                body("creditorName", equalTo(json.get("creditorName")));*/
    }

    @Features("3 I. Bulk Payment Initiation Service - Target 2 Payments")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Bulk Payment Initiation Service - Get Target 2 Payments Bulk Payment Maximum Values Details Schema Conform Test")
    @Test(enabled = false, dependsOnMethods = {"initiateTarget2PaymentsBulkPaymentMaxValuesTest"})
    public void getTarget2PaymentsBulkPaymentMaxValuesDetailsSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/paymentBulk/target-2-payments/Target2PaymentsBulkPaymentDetails.json")));
    }

    @Features("3 I. Bulk Payment Initiation Service - Target 2 Payments")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Bulk Payment Initiation Service - Get Target 2 Payments Bulk Payment Minimum Values Details")
    @Test(dependsOnMethods = {"initiateTarget2PaymentsBulkPaymentMinValuesTest"})
    public void getTarget2PaymentsBulkPaymentMinValuesDetailsTest() {

        uuid = uuid();
        Map json = convertFileToHashMap("src/main/resources/paymentBulk/target-2-payments/bulkPaymentInitiateTarget2PaymentsMinValues.json");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                body("debtorAccount", equalTo(json.get("debtorAccount"))).
                body("payments", equalTo(json.get("payments")));

            /*    body("creditorAccount", equalTo(json.get("creditorAccount"))).
                body("instructedAmount.currency", equalTo(((Map)json.get("instructedAmount")).get("currency"))).
                body("instructedAmount.amount", equalTo((Float.parseFloat((String)(((Map)json.get("instructedAmount")).get("amount")))))).
                body("creditorName", equalTo(json.get("creditorName")));*/
    }

    @Features("3 I. Bulk Payment Initiation Service - Target 2 Payments")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Bulk Payment Initiation Service - Get Target 2 Payments Bulk Payment Minimum Values Details Schema Conform Test")
    @Test(enabled = false, dependsOnMethods = {"initiateTarget2PaymentsBulkPaymentMinValuesTest"})
    public void getTarget2PaymentsBulkPaymentMinValuesDetailsSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/paymentBulk/target-2-payments/Target2PaymentsBulkPaymentDetails.json")));
    }




    
     /********************************
     *
     *    GET TRANSACTION STATUS
     *
     *******************************/





    @Features("3 I. Bulk Payment Initiation Service - Target 2 Payments")
    @Stories({"3. Payment Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of Target 2 Payments Bulk Payment with Maximum Values by Id")
    @Test(dependsOnMethods = {"initiateTarget2PaymentsBulkPaymentMaxValuesTest"})
    public void getTarget2PaymentsBulkPaymentMaxValuesStatusTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD"));
    }

    @Features("3 I. Bulk Payment Initiation Service - Target 2 Payments")
    @Stories({"3. Payment Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of Target 2 Payments Bulk Payment with Maximum Values by Id Schema Conform Test")
    @Test(enabled = false, dependsOnMethods = {"initiateTarget2PaymentsBulkPaymentMaxValuesTest"})
    public void getTarget2PaymentsBulkPaymentMaxValuesStatusSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/paymentBulk/target-2-payments/Target2PaymentsBulkPaymentStatus.json")));
    }

    @Features("3 I. Bulk Payment Initiation Service - Target 2 Payments")
    @Stories({"3. Payment Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of Target 2 Payments Bulk Payment with Minimum Values by Id")
    @Test(dependsOnMethods = {"initiateTarget2PaymentsBulkPaymentMinValuesTest"})
    public void getTarget2PaymentsBulkPaymentMinValuesStatusTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD"));
    }

    @Features("3 I. Bulk Payment Initiation Service - Target 2 Payments")
    @Stories({"3. Payment Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of Target 2 Payments Bulk Payment with Minimum Values by Id Schema Conform Test")
    @Test(enabled = false, dependsOnMethods = {"initiateTarget2PaymentsBulkPaymentMinValuesTest"})
    public void getTarget2PaymentsBulkPaymentMinValuesStatusSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/paymentBulk/target-2-payments/Target2PaymentsBulkPaymentStatus.json")));
    }




    
     /*************************
     *
     *    DELETE PAYMENT
     *
     *************************/




    @Features("3 I. Bulk Payment Initiation Service - Target 2 Payments")
    @Stories({"4. Delete Payment"})
    @TestCaseId("Delete Payment - Delete Target 2 Payments Bulk Payment with Maximum Values While SCA not finished")
    @Test(alwaysRun = true, dependsOnMethods = {"initiateTarget2PaymentsBulkPaymentMaxValuesTest", "getTarget2PaymentsBulkPaymentMaxValuesDetailsTest","getTarget2PaymentsBulkPaymentMaxValuesStatusTest"})
    public void deleteTarget2PaymentsBulkPaymentWithMaxValuesTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", paymentIdMax).
                when().delete(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid));

        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                body("transactionStatus", equalTo("CANC"));

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+ paymentIdMax +" doesn't exist.")));
    }

    @Features("3 I. Bulk Payment Initiation Service - Target 2 Payments")
    @Stories({"4. Delete Payment"})
    @TestCaseId("Delete Payment - Delete Target 2 Payments Bulk Payment with Minimum Values While SCA not finished")
    @Test(alwaysRun = true, dependsOnMethods = {"initiateTarget2PaymentsBulkPaymentMinValuesTest", "getTarget2PaymentsBulkPaymentMinValuesDetailsTest","getTarget2PaymentsBulkPaymentMinValuesStatusTest"})
    public void deleteTarget2PaymentsBulkPaymentWithMinValuesTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", paymentIdMin).
                when().delete(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid));

        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                body("transactionStatus", equalTo("CANC"));

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+ paymentIdMin +" doesn't exist.")));
    }
}