package BulkPaymentInitiationService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.util.Arrays;
import java.util.Map;

import static core.helpers.Actions.*;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;



public class BulkPaymentInitiationSepaTest extends TestBase{

    private String product = "sepa-credit-transfers";
    private String ERROR_FIELD = "code";
    private String paymentIdMin;
    private String paymentIdMax;
    private String uuid;
    private String PSU_ID = "aspsp2";
    private Response response;
    private String basePath;

    @BeforeClass
    public void setup() {
        basePath = "/v1/bulk-payments";
        RestAssured.basePath = basePath;
    }

    @BeforeMethod
    public void setPath(){
        setup();
    }






     /********************
     *
     *  INITIATE PAYMENT
     *
     *********************/







    @Features("3 F. Bulk Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Bulk Payment Initiation Service - Initiate Sepa Credit Transfer Bulk Payment with Max values for PSU")
    @Test()
    public void initiateSepaBulkPaymentMaxValuesTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(map("src/main/resources/paymentBulk/sepa-credit-transfer/bulkPaymentInitiateSepaMaxValues.json")).
                when().post(product).
                then().extract().response();

        paymentIdMax = response.then().statusCode(201).extract().path("paymentId");

        response.then().
                header("X-Request-ID", equalTo(uuid)).
                header("Location", equalTo(basePath + "/" + paymentIdMax)).
                header("Aspsp-Sca-Approach", notNullValue()).
                body("transactionStatus", equalTo("RCVD")).
                body("paymentId", notNullValue()).
                body("_links.startAuthorisation", equalTo(basePath + "/" + product + "/" + paymentIdMax + "/authorisations"));
    }

    @Features("3 F. Bulk Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Bulk Payment Initiation Service - Initiate Sepa Credit Transfer Bulk Payment for PSU with Max values  Schema Conform Test")
    @Test(enabled = false)
    public void initiateSepaBulkPaymentMaxValuesSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(map("src/main/resources/paymentBulk/sepa-credit-transfer/bulkPaymentInitiateSepaMaxValues.json")).
                when().post(product).
                then().statusCode(201).
                body(matchesJsonSchema(new File("src/main/resources/schemas/payment/sepa-credit-transfer/SepaPaymentInitiation.json")));
    }

    @Features("3 F. Bulk Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Bulk Payment Initiation Service - Initiate Sepa Credit Transfer Bulk Payment with Min values for PSU")
    @Test()
    public void initiateSepaBulkPaymentMinValuesTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(map("src/main/resources/paymentBulk/sepa-credit-transfer/bulkPaymentInitiateSepaMinValues.json")).
                when().post(product).
                then().extract().response();

        paymentIdMin = response.then().statusCode(201).extract().path("paymentId");

        response.then().
                header("X-Request-ID", equalTo(uuid)).
                header("Location", equalTo(basePath + "/" + paymentIdMin)).
                header("Aspsp-Sca-Approach", notNullValue()).
                body("transactionStatus", equalTo("RCVD")).
                body("paymentId", notNullValue()).
                body("_links.startAuthorisation", equalTo(basePath + "/" + product + "/" + paymentIdMin + "/authorisations"));
    }

    @Features("3 F. Bulk Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"1. Initiate Payment"})
    @TestCaseId("Bulk Payment Initiation Service - Initiate Sepa Credit Transfer Bulk Payment for PSU with Min values  Schema Conform Test")
    @Test(enabled = false)
    public void initiateSepaBulkPaymentMinValuesSchemaConformTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(map("src/main/resources/paymentBulk/sepa-credit-transfer/bulkPaymentInitiateSepaMinValues.json")).
                when().post(product).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/payment/sepa-credit-transfer/SepaPaymentInitiation.json")));
    }



    /********************
     *
     *  GET PAYMENT DETAILS
     *
     ************************/








    @Features("3 F. Bulk Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Bulk Payment Initiation Service - Get Sepa Bulk Payment Maximum Values Details")
    @Test(dependsOnMethods = {"initiateSepaBulkPaymentMaxValuesTest"})
    public void getSepaBulkPaymentMaxValuesDetailsTest() {

        uuid = uuid();
        Map json = convertFileToHashMap("src/main/resources/paymentBulk/sepa-credit-transfer/bulkPaymentInitiateSepaMaxValues.json");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                body("debtorAccount", equalTo(json.get("debtorAccount"))).
                body("payments", equalTo(json.get("payments")));
/*                body("creditorAccount", equalTo(json.get("creditorAccount"))).
                body("instructedAmount.currency", equalTo(((Map)json.get("instructedAmount")).get("currency"))).
                body("instructedAmount.amount", equalTo((Float.parseFloat((String)(((Map)json.get("instructedAmount")).get("amount")))))).
                body("creditorName", equalTo(json.get("creditorName")));*/
    }

    @Features("3 F. Bulk Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Bulk Payment Initiation Service - Get Sepa Bulk Payment Maximum Values Details Schema Conform Test")
    @Test(enabled = false, dependsOnMethods = {"initiateSepaBulkPaymentMaxValuesTest"})
    public void getSepaBulkPaymentMaxValuesDetailsSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/payment/sepa-credit-transfer/SepaPaymentDetails.json")));
    }

    @Features("3 F. Bulk Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Bulk Payment Initiation Service - Get Sepa Bulk Payment Minimum Values Details")
    @Test(dependsOnMethods = {"initiateSepaBulkPaymentMinValuesTest"})
    public void getSepaBulkPaymentMinValuesDetailsTest() {

        uuid = uuid();
        Map json = convertFileToHashMap("src/main/resources/paymentBulk/sepa-credit-transfer/bulkPaymentInitiateSepaMinValues.json");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                body("debtorAccount", equalTo(json.get("debtorAccount"))).
                body("creditorAccount", equalTo(json.get("creditorAccount"))).
                body("payments", equalTo(json.get("payments")));
/*                body("instructedAmount.currency", equalTo(((Map)json.get("instructedAmount")).get("currency"))).
                body("instructedAmount.amount", equalTo((Float.parseFloat((String)(((Map)json.get("instructedAmount")).get("amount")))))).
                body("creditorName", equalTo(json.get("creditorName")));*/
    }

    @Features("3 F. Bulk Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"2. Get Payment Details"})
    @TestCaseId("Bulk Payment Initiation Service - Get Sepa Bulk Payment Minimum Values Details Schema Conform Test")
    @Test(enabled = false, dependsOnMethods = {"initiateSepaBulkPaymentMinValuesTest"})
    public void getSepaBulkPaymentMinValuesDetailsSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/payment/sepa-credit-transfer/SepaPaymentDetails.json")));
    }




     /*******************************
     *
     *    GET TRANSACTION STATUS
     *
     ******************************/





    @Features("3 F. Bulk Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"3. Payment Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of Sepa Bulk Payment with Maximum Values by Id")
    @Test(dependsOnMethods = {"initiateSepaBulkPaymentMaxValuesTest"})
    public void getSepaBulkPaymentMaxValuesStatusTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD"));
    }

    @Features("3 F. Bulk Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"3. Payment Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of Sepa Bulk Payment with Maximum Values by Id Schema Conform Test")
    @Test(enabled = false, dependsOnMethods = {"initiateSepaBulkPaymentMaxValuesTest"})
    public void getSepaBulkPaymentMaxValuesStatusSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/payment/sepa-credit-transfer/SepaPaymentStatus.json")));
    }

    @Features("3 F. Bulk Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"3. Payment Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of Sepa Bulk Payment with Minimum Values by Id")
    @Test(dependsOnMethods = {"initiateSepaBulkPaymentMinValuesTest"})
    public void getSepaBulkPaymentMinValuesStatusTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD"));
    }

    @Features("3 F. Bulk Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"3. Payment Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of Sepa Bulk Payment with Minimum Values by Id Schema Conform Test")
    @Test(enabled = false, dependsOnMethods = {"initiateSepaBulkPaymentMinValuesTest"})
    public void getSepaBulkPaymentMinValuesStatusSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/payment/sepa-credit-transfer/SepaPaymentStatus.json")));
    }




     /********************
     *
     *    DELETE PAYMENT
     *
     ************************/




    @Features("3 F. Bulk Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"4. Delete Payment"})
    @TestCaseId("Delete Payment - Delete Sepa Bulk Payment with Maximum Values While SCA not finished")
    @Test(alwaysRun = true, dependsOnMethods = {"initiateSepaBulkPaymentMaxValuesTest", "getSepaBulkPaymentMaxValuesDetailsTest","getSepaBulkPaymentMaxValuesStatusTest"})
    public void deleteSepaBulkPaymentWithMaxValuesTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", paymentIdMax).
                when().delete(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid));

        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                body("transactionStatus", equalTo("CANC"));

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+ paymentIdMax +" doesn't exist.")));
    }

    @Features("3 F. Bulk Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"4. Delete Payment"})
    @TestCaseId("Delete Payment - Delete Sepa Bulk Payment with Minimum Values While SCA not finished")
    @Test(alwaysRun = true, dependsOnMethods = {"initiateSepaBulkPaymentMinValuesTest", "getSepaBulkPaymentMinValuesDetailsTest","getSepaBulkPaymentMinValuesStatusTest"})
    public void deleteSepaPaymentWithMinValuesTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", paymentIdMin).
                when().delete(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid));

        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                body("transactionStatus", equalTo("CANC"));

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+ paymentIdMin +" doesn't exist.")));
    }
}
