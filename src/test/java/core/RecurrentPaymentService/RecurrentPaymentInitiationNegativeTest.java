package core.RecurrentPaymentService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.util.Arrays;

import static core.helpers.Actions.date;
import static core.helpers.Actions.uuid;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

@Listeners({TestExecutor.class})
public class RecurrentPaymentInitiationNegativeTest extends TestBase {

    private String productInstatntSepaCrTr = "instant-sepa-credit-transfers";
    private String productCrossBorgerCrTr = "cross-border-credit-transfers";
    private String productSepaCrTr = "sepa-credit-transfers";
    private String productTarget2Payment = "target-2-payments";
    private String ERROR_FIELD = "code";
    private String paymentId;
    private String cancelledPaymentIdInstatntSepaCrTr;
    private String cancelledPaymentCrossBorgerCrTr;
    private String cancelledPaymentSepaCrTr;
    private String cancelledTarget2Payment;
    private String uuid;
    private String PSU_ID = "aspsp2";
    private String unknown_PSU_ID = "aspsp5";
    private Response response;
    private String basePath;

    @BeforeClass
    public void setup() {
        basePath = "/v1/periodic-payments";
        RestAssured.basePath = basePath;
    }

    @BeforeMethod
    public void setPath(){
        setup();
    }

    /**
     * Instant Sepa Credit Transfer
     */

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Instant Sepa Credit Transfer Recurrent Payment for PSU missing Mandatory Field Negative Test")
    @Test()
    public void initiateInstantSepaRecurrentPaymentMissingMandatoryFieldNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaMissingMandatoryFields.json")).
                when().post(productInstatntSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text",equalTo(Arrays.asList("Invalid null value submitted for creditorName")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Instant Sepa Credit Transfer Recurrent Payment for PSU Unsupported field Negative Test")
    @Test()
    public void initiateInstantSepaRecurrentPaymentUnsupportedFieldNegativeTest() {
        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaUnsupportedFields.json")).
                when().post(productInstatntSepaCrTr).
                then().statusCode(201).extract().response();

        paymentId = response.then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                extract().path("paymentId");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentId).
                when().get(productInstatntSepaCrTr + "/{paymentId}").
                then().statusCode(200).
                body("transactionStatus", equalTo("RCVD")).
                body("chargeBearer",nullValue()).
                body(not("unsupportedField"));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Instant Sepa Credit Transfer Recurrent Payment Without Currency Negative Test")
    @Test()
    public void initiateInstantSepaRecurrentPaymentMissingCurrencyNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaWithoutCurrency.json")).
                when().post(productInstatntSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text",equalTo(Arrays.asList("Invalid null value submitted for instructedAmount.currency")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"4. Delete Recurrent Payment"})
    @TestCaseId("Delete Recurrent Payment - Delete Non Existing Instant Sepa Recurrent Payment Negative Test \n https://pmc.specific-group.at/confluence/pages/viewpage.action?pageId=111904579")
    @Test()
    public void deleteNoneExistingInstantSepaRecurrentPaymentNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                pathParam("paymentId", uuid).
                when().delete(productInstatntSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"2. Get Recurrent Payment Details"})
    @TestCaseId("Recurrent Payment Initiation Service - Get Instant Sepa Recurrent Payment Details of a non-existent payment Negative Test")
    @Test
    public void getInstantSepaRecurrentPaymentDetailsNonExistentPaymentNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", uuid).
                when().get(productInstatntSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"2. Get Recurrent Payment Details"})
    @TestCaseId("Recurrent Payment Initiation Service - Get Instant Sepa Recurrent Payment Details of a cancelled payment Negative Test")
    @Test
    public void getInstantSepaRecurrentPaymentDetailsCancelledPaymentNegativeTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaMaxValues.json")).
                when().post(productInstatntSepaCrTr).
                then().extract().response();
        cancelledPaymentIdInstatntSepaCrTr = response.then().statusCode(201).extract().path("paymentId");
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", cancelledPaymentIdInstatntSepaCrTr).
                when().delete(productInstatntSepaCrTr + "/{paymentId}").
                then().statusCode(200);

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", cancelledPaymentIdInstatntSepaCrTr).
                when().get(productInstatntSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+cancelledPaymentIdInstatntSepaCrTr+" doesn't exist.")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Recurrent Payment product is not supported")
    @Test()
    public void initiateRecurrentPaymentProductIsNotSupportedTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaMaxValues.json")).
                when().post(uuid).
                then().statusCode(404).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("PRODUCT_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment product "+uuid+" is not supported.")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Instant Sepa Credit Transfer Recurrent Payment Invalid format of amount Negative Test")
    @Test()
    public void initiateInstantSepaRecurrentPaymentInvalidFormatAmountNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaInvalidFormatAmount.json")).
                when().post(productInstatntSepaCrTr).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus",equalTo("RCVD"));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Instant Sepa Credit Transfer Recurrent Payment missing UUID Negative Test")
    @Test()
    public void initiateInstantSepaRecurrentPaymentMissingUUIDNegativeTest() {

        given().header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaMinValues.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Mandatory header 'X-Request-ID' is missing or empty.")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Instant Sepa Credit Transfer Recurrent Payment missing PSU_IP Negative Test")
    @Test()
    public void initiateInstantSepaRecurrentPaymentMissingPSUIPNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaMinValues.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Format of certain request fields are not matching the XS2A requirements.")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Instant Sepa Credit Transfer Recurrent Payment Missing PSU_ID Negative Test")
    @Test()
    public void initiateInstantSepaRecurrentPaymentMissingPSUIDNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaMinValues.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Format of certain request fields are not matching the XS2A requirements.")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Instant Sepa Credit Transfer Recurrent Payment unknown PSU_ID Negative Test")
    @Test()
    public void initiateInstantSepaRecurrentPaymentUnknownPSUIDNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", unknown_PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaMinValues.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Format of certain request fields are not matching the XS2A requirements.")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Instant Sepa Credit Transfer Recurrent Payment using PUT method Negative Test")
    @Test()
    public void initiateInstantSepaRecurrentPaymentUsingPutMethodNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/instant-sepa-credit-transfer/paymentInitiateInstantSepaMinValues.json")).
                when().put(productSepaCrTr).
                then().statusCode(405).
                body("error",equalTo("Method Not Allowed")).
                body("message",equalTo("Request method 'PUT' not supported")).
                body("path",equalTo("/v1/periodic-payments/sepa-credit-transfers"));
    }




    /*******************************************************************
     * Cross Border Credit Transfers
     *******************************************************************/







    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Cross Border Credit Transfers Recurrent Payment for PSU Missing Mandatory Field Negative Test")
    @Test()
    public void initiateCrossBorderRecurrentPaymentMissingMandatoryFieldNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/cross-border-credit-transfers/paymentInitiateCrossBorderMissingMandatoryFields.json")).
                when().post(productCrossBorgerCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for creditorName")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Cross Border Credit Transfers Recurrent Payment for PSU Unsupported Field Negative Test")
    @Test()
    public void initiateCrossBorderRecurrentPaymentUnsupportedFieldNegativeTest() {
        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/cross-border-credit-transfers/paymentInitiateCrossBorderUnsupportedFields.json")).
                when().post(productCrossBorgerCrTr);

        paymentId = response.then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                extract().path("paymentId");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentId).
                when().get(productCrossBorgerCrTr + "/{paymentId}").
                then().statusCode(200).
                body("transactionStatus", equalTo("RCVD")).
                body("endToEndIdentification",nullValue()).
                body(not("vasia"));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Cross Border Credit Transfers Recurrent Payment Without Currency Negative Test")
    @Test()
    public void initiateCrossBorderRecurrentPaymentMissingCurrencyNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/cross-border-credit-transfers/paymentInitiateCrossBorderWithoutCurrency.json")).
                when().post(productCrossBorgerCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for instructedAmount.currency")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"4. Delete Recurrent Payment"})
    @TestCaseId("Delete Recurrent Payment - Delete Non Existing Cross Border Credit Transfers Recurrent Payment Negative Test \n https://pmc.specific-group.at/confluence/pages/viewpage.action?pageId=111904579")
    @Test()
    public void deleteNoneExistingRecurrentPaymentNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                pathParam("paymentId", uuid).
                when().delete(productCrossBorgerCrTr + "/{paymentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }


    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"2. Get Recurrent Payment Details"})
    @TestCaseId("Recurrent Payment Initiation Service - Get Cross Border Credit Transfer Recurrent Payment Details of a cancelled payment Negative Test")
    @Test
    public void getCrossBorderCreditTransferRecurrentPaymentDetailsCancelledPaymentNegativeTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/cross-border-credit-transfers/paymentInitiateCrossBorderMaxValues.json")).
                when().post(productCrossBorgerCrTr).
                then().extract().response();
        cancelledPaymentCrossBorgerCrTr = response.then().statusCode(201).extract().path("paymentId");
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", cancelledPaymentCrossBorgerCrTr).
                when().delete(productCrossBorgerCrTr + "/{paymentId}").
                then().statusCode(200);

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", cancelledPaymentCrossBorgerCrTr).
                when().get(productCrossBorgerCrTr + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+cancelledPaymentCrossBorgerCrTr+" doesn't exist.")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Cross Border Credit Transfer Recurrent Payment Invalid format of amount Negative Test")
    @Test()
    public void initiateCrossBorderCreditTransfersRecurrentPaymentInvalidFormatAmountNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/cross-border-credit-transfers/paymentInitiateCrossBorderInvalidFormatAmount.json")).
                when().post(productCrossBorgerCrTr).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus",equalTo("RCVD"));
    }





    /******************************************************
     * Sepa Credit Transfer
    ******************************************************/





    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Sepa Credit Transfer Recurrent Payment with Missing mandatory field Negative Test")
    @Test()
    public void initiateSepaRecurrentPaymentMissingMandatoryFieldNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/sepa-credit-transfer/paymentInitiateSepaMissingMandatoryFields.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for creditorName")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Sepa Credit Transfer Recurrent Payment with Unsupported field Negative Test")
    @Test()
    public void initiateSepaRecurrentPaymentUnsupportedFieldNegativeTest() {
        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/sepa-credit-transfer/paymentInitiateSepaUnsupportedFields.json")).
                when().post(productSepaCrTr);

        paymentId = response.then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                extract().path("paymentId");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentId).
                when().get(productSepaCrTr + "/{paymentId}").
                then().statusCode(200).
                body("transactionStatus", equalTo("RCVD")).
                body("chargeBearer",nullValue()).
                body(not("vasia"));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate RecurrentPayment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Sepa Credit Transfer Recurrent Payment with Long EndToEndIdentification field Negative Test")
    @Test()
    public void initiateSepaRecurrentPaymentLongEndToEndIdentificationNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/sepa-credit-transfer/paymentInitiateSepaLongEndToEntIdentification.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid E2EID0123456789012345678901234567890 value submitted for endToEndIdentification")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Recurrent Payment for None Existing Product Negative Test")
    @Test()
    public void initiateRecurrentPaymentForNoneExistingProductNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/sepa-credit-transfer/paymentInitiateSepaMinValues.json")).
                when().post("domestos").
                then().statusCode(404).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("PRODUCT_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment product domestos is not supported.")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Sepa Credit Transfer Recurrent Payment Without Currency Negative Test")
    @Test()
    public void initiateSepaRecurrentPaymentMissingCurrencyNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/sepa-credit-transfer/paymentInitiateSepaWithoutCurrency.json")).
                when().post(productSepaCrTr).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for instructedAmount.currency")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"2. Get Recurrent Payment Details"})
    @TestCaseId("Recurrent Payment Initiation Service - Get None Existing Sepa Recurrent Payment Details Negative Test")
    @Test()
    public void getNoneExistingSepaRecurrentPaymentDetailsNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).
                pathParam("paymentId", uuid).
                when().get(productSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"3. Recurrent Payment Status Request"})
    @TestCaseId("Recurrent Payment Initiation Service - Get None Existing Sepa Recurrent Payment Status Negative Test")
    @Test()
    public void getNoneExistingSepaRecurrentPaymentStatusNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).
                pathParam("paymentId", uuid).
                when().get(productSepaCrTr + "/{paymentId}/status").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"4. Delete Recurrent Payment"})
    @TestCaseId("Delete Recurrent Payment - Delete Non Existing Sepa Credit Transfer Recurrent Payment Negative Test \n https://pmc.specific-group.at/confluence/pages/viewpage.action?pageId=111904579")
    @Test()
    public void deleteNoneExistingSepaCrTrRecurrentPaymentSepaCrTrNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                pathParam("paymentId", uuid).
                when().delete(productSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"2. Get Recurrent Payment Details"})
    @TestCaseId("Recurrent Payment Initiation Service - Get Sepa Credit Transfer Recurrent Payment Details of a cancelled payment Negative Test")
    @Test
    public void getSepaCreditTransferRecurrentPaymentDetailsCancelledPaymentNegativeTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/sepa-credit-transfer/paymentInitiateSepaMaxValues.json")).
                when().post(productSepaCrTr).
                then().extract().response();
        cancelledPaymentSepaCrTr = response.then().statusCode(201).extract().path("paymentId");
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", cancelledPaymentSepaCrTr).
                when().delete(productSepaCrTr + "/{paymentId}").
                then().statusCode(200);

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", cancelledPaymentSepaCrTr).
                when().get(productSepaCrTr + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+cancelledPaymentSepaCrTr+" doesn't exist.")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Sepa Credit Transfer Recurrent Payment Invalid format of amount Negative Test")
    @Test()
    public void initiateSepaCreditTransfersRecurrentPaymentInvalidFormatAmountNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/sepa-credit-transfer/paymentInitiateSepaInvalidFormatAmmount.json")).
                when().post(productSepaCrTr).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus",equalTo("RCVD"));
    }





    /***********************************************************************************
     * Target 2 Recurrent Payments
     ***********************************************************************************/





    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Target 2 Recurrent Payments Payment Missing Mandatory Field Negative Test")
    @Test()
    public void initiateTarget2RecurrentPaymentsRecurrentPaymentMissingMandatoryFieldNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/target-2-payments/paymentInitiateTarget2PaymentsMissingMandatoryFields.json")).
                when().post(productTarget2Payment).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for creditorName")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Target 2 Recurrent Payments Payment Unsupported Field Negative Test")
    @Test()
    public void initiateTarget2RecurrentPaymentsPaymentUnsupportedFieldNegativeTest() {

        uuid = uuid();

        paymentId = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/target-2-payments/paymentInitiateTarget2PaymentsUnsupportedFields.json")).
                when().post(productTarget2Payment).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                extract().path("paymentId");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentId).
                when().get(productTarget2Payment + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                body("vasia", nullValue());
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Target 2 Recurrent Payments Payment for PSU Without Currency Negative Test")
    @Test()
    public void initiateTarget2RecurrentPaymentsPaymentMissingCurrencyNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/target-2-payments/paymentInitiateTarget2PaymentsWithoutCurrency.json")).
                when().post(productTarget2Payment).
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for instructedAmount.currency")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"4. Delete Recurrent Payment"})
    @TestCaseId("Delete Recurrent Payment - Delete Non Existing Target 2 Recurrent Payments Payment Negative Test")
    @Test()
    public void deleteNoneExistingTarget2RecurrentPaymentNegativeTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                pathParam("paymentId", uuid).
                when().delete(productTarget2Payment + "/{paymentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD, equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Payment "+uuid+" doesn't exist.")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"2. Get Recurrent Payment Details"})
    @TestCaseId("Recurrent Payment Initiation Service - Get Target 2 Recurrent Payment Details of a cancelled payment Negative Test")
    @Test
    public void getTarget2RecurrentPaymentDetailsCancelledPaymentNegativeTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/target-2-payments/paymentInitiateTarget2PaymentsMaxValues.json")).
                when().post(productTarget2Payment).
                then().extract().response();
        cancelledTarget2Payment = response.then().statusCode(201).extract().path("paymentId");
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", cancelledTarget2Payment).
                when().delete(productTarget2Payment + "/{paymentId}").
                then().statusCode(200);

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", cancelledTarget2Payment).
                when().get(productTarget2Payment + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages." + ERROR_FIELD,equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+cancelledTarget2Payment+" doesn't exist.")));
    }

    @Features("7E. Recurrent Payment Initiation Service - Negative")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Target 2 Recurrent Payment Invalid format of amount Negative Test")
    @Test()
    public void initiateTarget2RecurrentPaymentInvalidFormatAmountNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/target-2-payments/paymentInitiateTarget2PaymentInvalidFormatAmmount.json")).
                when().post(productTarget2Payment).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus",equalTo("RCVD"));
    }

}
