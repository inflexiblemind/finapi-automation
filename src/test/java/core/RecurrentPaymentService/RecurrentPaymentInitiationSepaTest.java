package core.RecurrentPaymentService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.util.Arrays;
import java.util.Map;

import static core.helpers.Actions.*;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

@Listeners({TestExecutor.class})
public class RecurrentPaymentInitiationSepaTest extends TestBase{

    private String product = "sepa-credit-transfers";
    private String paymentIdMin;
    private String paymentIdMax;
    private String uuid;
    private String PSU_ID = "aspsp2";
    private Response response;
    private String basePath;

    @BeforeClass
    public void setup() {
        basePath = "/v1/periodic-payments";
        RestAssured.basePath = basePath;
    }

    @BeforeMethod
    public void setPath(){
        setup();
    }






    /*********************
     *
     *  INITIATE PAYMENT
     *
     **********************/






    @Features("7A. Recurrent Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Sepa Credit Transfer Recurrent Payment with Max values  for PSU")
    @Test()
    public void initiateSepaRecurrentPaymentMaxValuesTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/sepa-credit-transfer/paymentInitiateSepaMaxValues.json")).
                when().post(product).
                then().extract().response();

        paymentIdMax = response.then().statusCode(201).extract().path("paymentId");

        response.then().
                header("X-Request-ID", equalTo(uuid)).
                header("Location", equalTo(basePath + "/" + paymentIdMax)).
                header("Aspsp-Sca-Approach", notNullValue()).
                body("transactionStatus", equalTo("RCVD")).
                body("paymentId", notNullValue()).
                body("_links.startAuthorisation", equalTo(basePath + "/" + product + "/" + paymentIdMax + "/authorisations"));
    }

    @Features("7A. Recurrent Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Sepa Credit Transfer Recurrent Payment for PSU with Max values  Schema Conform Test")
    @Test()
    public void initiateSepaRecurrentPaymentMaxValuesSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/sepa-credit-transfer/paymentInitiateSepaMaxValues.json")).
                when().post(product).
                then().statusCode(201).
                body(matchesJsonSchema(new File("src/main/resources/schemas/recurrent-payment/sepa-credit-transfer/SepaPaymentInitiation.json")));
    }

    @Features("7A. Recurrent Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Sepa Credit Transfer Recurrent Payment with Min values for PSU")
    @Test()
    public void initiateSepaRecurrentPaymentMinValuesTest() {

        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/sepa-credit-transfer/paymentInitiateSepaMinValues.json")).
                when().post(product).
                then().extract().response();

        paymentIdMin = response.then().statusCode(201).extract().path("paymentId");

        response.then().
                header("X-Request-ID", equalTo(uuid)).
                header("Location", equalTo(basePath + "/" + paymentIdMin)).
                header("Aspsp-Sca-Approach", notNullValue()).
                body("transactionStatus", equalTo("RCVD")).
                body("paymentId", notNullValue()).
                body("_links.startAuthorisation", equalTo(basePath + "/" + product + "/" + paymentIdMin + "/authorisations"));
    }

    @Features("7A. Recurrent Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"1. Initiate Recurrent Payment"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Sepa Credit Transfer Recurrent Payment for PSU with Min values  Schema Conform Test")
    @Test()
    public void initiateSepaRecurrentPaymentMinValuesSchemaConformTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/recurrent-payment/sepa-credit-transfer/paymentInitiateSepaMinValues.json")).
                when().post(product).
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/recurrent-payment/sepa-credit-transfer/SepaPaymentInitiation.json")));
    }


    /*************************
     *
     *  GET PAYMENT DETAILS
     *
     *************************/







    @Features("7A. Recurrent Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"2. Get Recurrent Payment Details"})
    @TestCaseId("Recurrent Payment Initiation Service - Get Sepa Recurrent Payment Maximum Values Details")
    @Test(dependsOnMethods = {"initiateSepaRecurrentPaymentMaxValuesTest"})
    public void getSepaRecurrentPaymentMaxValuesDetailsTest() {

        uuid = uuid();
        Map json = convertFileToHashMap("src/main/resources/recurrent-payment/sepa-credit-transfer/paymentInitiateSepaMaxValues.json");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                body("debtorAccount", equalTo(json.get("debtorAccount"))).
                body("creditorAccount", equalTo(json.get("creditorAccount"))).
                body("instructedAmount.currency", equalTo(((Map)json.get("instructedAmount")).get("currency"))).
                body("instructedAmount.amount", equalTo((Float.parseFloat((String)(((Map)json.get("instructedAmount")).get("amount")))))).
                body("creditorName", equalTo(json.get("creditorName")));
    }

    @Features("7A. Recurrent Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"2. Get Recurrent Payment Details"})
    @TestCaseId("Recurrent Payment Initiation Service - Get Sepa Recurrent Payment Maximum Values Details Schema Conform Test")
    @Test(dependsOnMethods = {"initiateSepaRecurrentPaymentMaxValuesTest"})
    public void getSepaRecurrentPaymentMaxValuesDetailsSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/recurrent-payment/sepa-credit-transfer/SepaPaymentDetails.json")));
    }

    @Features("7A. Recurrent Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"2. Get Recurrent Payment Details"})
    @TestCaseId("Recurrent Payment Initiation Service - Get Sepa Recurrent Payment Minimum Values Details")
    @Test(dependsOnMethods = {"initiateSepaRecurrentPaymentMinValuesTest"})
    public void getSepaRecurrentPaymentMinValuesDetailsTest() {

        uuid = uuid();
        Map json = convertFileToHashMap("src/main/resources/recurrent-payment/sepa-credit-transfer/paymentInitiateSepaMinValues.json");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD")).
                body("debtorAccount", equalTo(json.get("debtorAccount"))).
                body("creditorAccount", equalTo(json.get("creditorAccount"))).
                body("instructedAmount.currency", equalTo(((Map)json.get("instructedAmount")).get("currency"))).
                body("instructedAmount.amount", equalTo((Float.parseFloat((String)(((Map)json.get("instructedAmount")).get("amount")))))).
                body("creditorName", equalTo(json.get("creditorName")));
    }

    @Features("7A. Recurrent Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"2. Get Recurrent Payment Details"})
    @TestCaseId("Recurrent Payment Initiation Service - Get Sepa Recurrent Payment Minimum Values Details Schema Conform Test")
    @Test(dependsOnMethods = {"initiateSepaRecurrentPaymentMinValuesTest"})
    public void getSepaRecurrentPaymentMinValuesDetailsSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/recurrent-payment/sepa-credit-transfer/SepaPaymentDetails.json")));
    }


    /********************************
     *
     *    GET TRANSACTION STATUS
     *
     *******************************/




    @Features("7A. Recurrent Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"3. Recurrent Payment Status Request"})
    @TestCaseId("Recurrent Payment Status Request - Get the Status of Sepa Recurrent Payment with Maximum Values by Id")
    @Test(dependsOnMethods = {"initiateSepaRecurrentPaymentMaxValuesTest"})
    public void getSepaRecurrentPaymentMaxValuesStatusTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD"));
    }

    @Features("7A. Recurrent Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"3. Recurrent Payment Status Request"})
    @TestCaseId("Recurrent Payment Status Request - Get the Status of Sepa Recurrent Payment with Maximum Values by Id Schema Conform Test")
    @Test(dependsOnMethods = {"initiateSepaRecurrentPaymentMaxValuesTest"})
    public void getSepaPaymentMaxValuesStatusSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/recurrent-payment/sepa-credit-transfer/SepaPaymentStatus.json")));
    }

    @Features("7A. Recurrent Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"3. Recurrent Payment Status Request"})
    @TestCaseId("Recurrent Payment Status Request - Get the Status of Sepa Recurrent Payment with Minimum Values by Id")
    @Test(dependsOnMethods = {"initiateSepaRecurrentPaymentMinValuesTest"})
    public void getSepaRecurrentPaymentMinValuesStatusTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("transactionStatus", equalTo("RCVD"));
    }

    @Features("7A. Recurrent Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"3. Recurrent Payment Status Request"})
    @TestCaseId("Recurrent Payment Status Request - Get the Status of Sepa Recurrent Payment with Minimum Values by Id Schema Conform Test")
    @Test(dependsOnMethods = {"initiateSepaRecurrentPaymentMinValuesTest"})
    public void getSepaRecurrentPaymentMinValuesStatusSchemaConformTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body(matchesJsonSchema(new File("src/main/resources/schemas/recurrent-payment/sepa-credit-transfer/SepaPaymentStatus.json")));
    }


    /*************************
     *
     *    DELETE PAYMENT
     *
     *************************/



    @Features("7A. Recurrent Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"4. Delete Recurrent Payment"})
    @TestCaseId("Delete Recurrent Payment - Delete Sepa Recurrent Payment with Maximum Values While SCA not finished")
    @Test(alwaysRun = true, dependsOnMethods = {"initiateSepaRecurrentPaymentMaxValuesTest", "getSepaRecurrentPaymentMaxValuesDetailsTest","getSepaRecurrentPaymentMaxValuesStatusTest"})
    public void deleteSepaRecurrentPaymentWithMaxValuesTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", paymentIdMax).
                when().delete(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid));

        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                body("transactionStatus", equalTo("CANC"));

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMax).
                when().get(product + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+ paymentIdMax +" doesn't exist.")));
    }

    @Features("7A. Recurrent Payment Initiation Service - Sepa Credit Transfer")
    @Stories({"4. Delete Recurrent Payment"})
    @TestCaseId("Delete Recurrent Payment - Delete Sepa Recurrent Payment with Minimum Values While SCA not finished")
    @Test(alwaysRun = true, dependsOnMethods = {"initiateSepaRecurrentPaymentMinValuesTest", "getSepaRecurrentPaymentMinValuesDetailsTest","getSepaRecurrentPaymentMinValuesStatusTest"})
    public void deleteSepaRecurrentPaymentWithMinValuesTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", paymentIdMin).
                when().delete(product + "/{paymentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid));

        given().header("X-Request-ID", uuid).header("PSU-ID", PSU_ID).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}/status").
                then().statusCode(200).
                body("transactionStatus", equalTo("CANC"));

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID)
                .header("date", date()).pathParam("paymentId", paymentIdMin).
                when().get(product + "/{paymentId}").
                then().statusCode(403).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Payment "+ paymentIdMin +" doesn't exist.")));
    }

}
