package core.AccountInformationService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.*;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.util.*;

import static core.helpers.Actions.getConsent;
import static core.helpers.Actions.getValidConsent;
import static core.helpers.Actions.uuid;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.*;



/**
 * Created by Sergey.Syomin on 10/10/2018.
 */

@Listeners({TestExecutor.class})
public class AccountInformationServiceTest extends TestBase {

    private static String PSUId = "aspsp2";
    private static String ACCOUNT_ID = "77777-999999999";
    private static String ACCOUNT_IBAN = "DE89370400440532013000";
    private static String consentIdAccountDetails;
    private static String consentIdBalances;
    private static String consentIdTransactions;
    private static String consentIdBalancesTransactions;
    private static String consentIdAvailableAccounts;
    private static String consentIdGlobal;
    private static String accountId;
    private static String transactionIdDetailed;
    private static String transactionIdGlobal;
    private static String ibanDetailed;
    private static String currencyDetailed;
    private static String ibanGlobal;
    private static String currencyGlobal;

    @BeforeClass
    public static void setup() {

        consentIdAccountDetails = getValidConsent(PSUId, "src/main/resources/consent/detailedConsent/createConsentAccountDetailsAccessOnly.json","src/main/resources/authorization/passwordInit2.json");
        consentIdBalances = getValidConsent(PSUId, "src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalances.json","src/main/resources/authorization/passwordInit2.json");
        consentIdTransactions = getValidConsent(PSUId, "src/main/resources/consent/detailedConsent/createConsentAccountDetailsTransactions.json","src/main/resources/authorization/passwordInit2.json");
        consentIdBalancesTransactions = getValidConsent(PSUId, "src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalancesTransactions.json","src/main/resources/authorization/passwordInit2.json");
        consentIdAvailableAccounts = getValidConsent(PSUId, "src/main/resources/consent/availableAccounts/createAvailableAccountsConsent.json","src/main/resources/authorization/passwordInit2.json");
        consentIdGlobal = getValidConsent(PSUId, "src/main/resources/consent/globalConsent/createGlobalConsent.json","src/main/resources/authorization/passwordInit2.json");
        System.out.println("\n\nconsentIdAccountDetails\n" + consentIdAccountDetails + "\n\nnnconsentIdBalances\n" + consentIdBalances + "\n\nconsentIdTransactions\n" + consentIdTransactions + "\n\nconsentIdBalancesTransactions\n" + consentIdBalancesTransactions + "\n\nconsentIdAvailableAccounts\n" + consentIdAvailableAccounts + "\n\nconsentIdGlobal\n" + consentIdGlobal + "\n\n");
    }

    @BeforeMethod
    public void setPath(){
        RestAssured.basePath = "/v1/accounts";
    }





    /************************************************************************
     *
     *    Account Information Service + AVAILABLE ACCOUNTS CONSENT Tests
     *
     ************************************************************************/






    @Features("1A. Account Information Service - Available Accounts Consent")
    @Stories({"1. Read Accounts List - Positive Tests"})
    @TestCaseId("Read Accounts List - Get Accounts WIth Available Accounts Consent")
    @Test()
    public void getAccountsWithAvailableAccountsConsentTest() {

        ArrayList<String> accounts = given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdAvailableAccounts).
                when().get().
                then().statusCode(200).
                extract().body().path("accounts.resourceId");

        accountId = accounts.get(new Random().nextInt(accounts.size()));

        System.out.println("Account for Account Details and get Balances tests chosen: " + accountId);
    }





    /************************************************************************
     *
     *    Account Information Service + DETAILED CONSENT Tests
     *
     ************************************************************************/





    // GET ACCOUNT LIST + DETAILED CONSENT



    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"1. Read Accounts List - Positive Tests"})
    @TestCaseId("Read Accounts List - Get Accounts WIth Account Details Detailed Consent")
    @Test()
    public void getAccountsWithAccountDetailsDetailedConsentTest() {
        given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdAccountDetails).
                when().get().
                then().statusCode(200);
    }

    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"1. Read Accounts List - Positive Tests"})
    @TestCaseId("Read Accounts List - Get Accounts WIth Account Details Detailed Consent Schema Conform Test")
    @Test()
    public void getAccountsSchemaConformTest() {
        given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdAccountDetails).
                when().get().then().
                body(matchesJsonSchema(new File("src/main/resources/schemas/account/AllAccounts.json")));
    }


    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"1. Read Accounts List - Positive Tests"})
    @TestCaseId("Read Accounts List - Get Accounts WIth Account Details + Balances Detailed Consent")
    @Test()
    public void getAccountsWithBalancesDetailedConsentTest() {
        given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdBalances).
                when().get().
                then().statusCode(200);
    }

    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"1. Read Accounts List - Positive Tests"})
    @TestCaseId("Read Accounts List - Get Accounts WIth Account Details + Transactions Detailed Consent")
    @Test()
    public void getAccountsWithTransactionsDetailedConsentTest() {
        given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdTransactions).
                when().get().
                then().statusCode(200);
    }

    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"1. Read Accounts List - Positive Tests"})
    @TestCaseId("Read Accounts List - Get Accounts WIth Account Details + Balances + Transactions Detailed Consent")
    @Test()
    public void getAccountsWithBalancesTransactionsDetailedConsentTest() {
        given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdBalancesTransactions).
                when().get().
                then().statusCode(200);
    }



    // GET ACCOUNT DETAILS + DETAILED CONSENT



    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"2. Read Account Details - Positive Tests"})
    @TestCaseId("Read Account Details - Get Account Details with Account Details Detailed Consent")
    @Test(alwaysRun = true, dependsOnMethods = "getAccountsWithAvailableAccountsConsentTest")
    public void getAccountDetailsWithAccountDetailsDetailedConsentTest() {

        Response response = given().pathParam("account-id", accountId)
                .header("X-Request-ID", uuid()).header("Consent-ID", consentIdAccountDetails).
                        when().get("{account-id}").
                        then().statusCode(200)
                .body("resourceId", equalTo(accountId))
                .extract().response();

        ibanDetailed = response.then().extract().path("iban");
        currencyDetailed = response.then().extract().path("currency");

        System.out.println(response.body().path("$").toString());
    }

    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"2. Read Account Details - Positive Tests"})
    @TestCaseId("Read Account Details - Get Account Details with Account Details + Balances Detailed Consent")
    @Test(alwaysRun = true, dependsOnMethods = "getAccountsWithAvailableAccountsConsentTest")
    public void getAccountDetailsWithBalancesDetailedConsentTest() {

        given().pathParam("account-id", accountId).
                header("X-Request-ID", uuid()).header("Consent-ID", consentIdBalances).
                when().get("{account-id}").
                then().statusCode(200).
                body("resourceId", equalTo(accountId));
    }

    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"2. Read Account Details - Positive Tests"})
    @TestCaseId("Read Account Details - Get Account Details with Account Details + Transactions Detailed Consent")
    @Test(alwaysRun = true, dependsOnMethods = "getAccountsWithAvailableAccountsConsentTest")
    public void getAccountDetailsWithTransactionsDetailedConsentTest() {

        given().pathParam("account-id", accountId).
                header("X-Request-ID", uuid()).header("Consent-ID", consentIdTransactions).
                when().get("{account-id}").
                then().statusCode(200).
                body("resourceId", equalTo(accountId));
    }

    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"2. Read Account Details - Positive Tests"})
    @TestCaseId("Read Account Details - Get Account Details with Account Details + Balances + Transactions Detailed Consent")
    @Test(alwaysRun = true, dependsOnMethods = "getAccountsWithAvailableAccountsConsentTest")
    public void getAccountDetailsWithBalancesTransactionsDetailedConsentTest() {

        given().pathParam("account-id", accountId).
                header("X-Request-ID", uuid()).header("Consent-ID", consentIdBalancesTransactions).
                when().get("{account-id}").
                then().statusCode(200).
                body("resourceId", equalTo(accountId));
    }

    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"2. Read Account Details - Positive Tests"})
    @TestCaseId("Read Account Details - Get Account Details With Detailed Consent Schema Conform")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest"})
    public void getAccountDetailsWithDetailedConsentSchemaConformTest() {

        given().header("X-Request-ID", uuid()).header("consent-ID", consentIdBalancesTransactions).pathParam("account-id", accountId).
                when().get("{account-id}").then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/account/AccountDetails.json")));
    }




    // GET ACCOUNT BALANCES + DETAILED CONSENT





    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"3. Read Account Balances - Positive Tests"})
    @TestCaseId("Read Account Balances - Get Account Balances With Account Details Balances Detailed Consent")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest", "getAccountDetailsWithAccountDetailsDetailedConsentTest"})
    public void getAccountBalancesWithBalancesDetailedConsentTest() {

        given().pathParam("account-id", accountId).header("X-Request-ID", uuid()).
                header("Consent-ID", consentIdBalances).
                when().get("{account-id}/balances").
                then().statusCode(200)
                .body("account.iban", equalTo(ibanDetailed))
                //.body("balances.balanceAmount.currency",equalTo(currencyDetailed))
                .extract().response();
    }

    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"3. Read Account Balances - Positive Tests"})
    @TestCaseId("Read Account Balances - Get Account Balances With Account Details Balances + Transactions Detailed Consent")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest", "getAccountDetailsWithAccountDetailsDetailedConsentTest"})
    public void getAccountBalancesWithBalancesTransactionsDetailedConsentTest() {

        given().pathParam("account-id", accountId).header("X-Request-ID", uuid()).header("Consent-ID", consentIdBalancesTransactions).
                when().get("{account-id}/balances").
                then().statusCode(200)
                .body("account.iban", equalTo(ibanDetailed))
                //.body("balances.balanceAmount.currency",equalTo(currencyDetailed))
                .extract().response();
    }

    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"3. Read Account Balances - Positive Tests"})
    @TestCaseId("Read Account Balances - Get Account Balances Schema Conform")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest", "getAccountDetailsWithAccountDetailsDetailedConsentTest"})
    public void getAccountBalancesWithDetailedConsentSchemaConformTest() {

        given().header("X-Request-ID", uuid()).header("consent-ID", consentIdBalancesTransactions).pathParam("account-id", accountId).
                when().get("{account-id}/balances").then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/account/AccountBalances.json")));
    }






    // GET TRANSACTIONS LIST + DETAILED CONSENT








    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"4. Read Account Transactions List - Positive Tests"})
    @TestCaseId("Read Account Transactions List - Get Account Transactions List With Account Details + Transactions Detailed Consent")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest", "getAccountDetailsWithAccountDetailsDetailedConsentTest"})
    public void getAccountTransactionsListWithTransactionsDetailedConsentTest() {

        Response response = given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", consentIdTransactions).
                queryParam("bookingStatus","both").
                //queryParam("bookingStatus","pending").
                when().get("{account-id}/transactions/").
                then().statusCode(200)
                //.body("account", equalTo(accountId))
                .body("account.iban", equalTo(ACCOUNT_IBAN))
                .extract().response();

        ArrayList<String> transactionsPending = response.then().extract().body().path("transactions.pending.transactionId");
        ArrayList<String> transactionsBooked = response.then().extract().body().path("transactions.booked.transactionId");
        ArrayList<String> transactionsBoth = transactionsPending;
        transactionsBoth.addAll(transactionsBooked);

        transactionIdDetailed = (transactionsBoth).get(new Random().nextInt(transactionsBoth.size()));

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).header("Consent-ID", consentIdTransactions).
                queryParam("bookingStatus","pending").
                when().get("{account-id}/transactions/").
                then().statusCode(200)
                //.body("account", equalTo(accountId))
                .body("account.iban", equalTo(ACCOUNT_IBAN));

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).header("Consent-ID", consentIdTransactions).
                queryParam("bookingStatus","booked").
                when().get("{account-id}/transactions/").
                then().statusCode(200)
                //.body("account", equalTo(accountId))
                .body("account.iban", equalTo(ACCOUNT_IBAN));
    }

    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"4. Read Account Transactions List - Positive Tests"})
    @TestCaseId("Read Account Transactions List - Get Account Transactions List Schema Conform")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest", "getAccountDetailsWithAccountDetailsDetailedConsentTest"})
    public void getAccountTransactionListWithBalancesTransactionsDetailedConsentSchemaConformTest() {

        given().header("X-Request-ID", uuid()).header("consent-ID", consentIdBalancesTransactions)
                .pathParam("account-id", ACCOUNT_ID).queryParam("bookingStatus","both").
                when().get("{account-id}/transactions/").then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/account/AccountTransactions.json")));
    }

    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"4. Read Account Transactions List - Positive Tests"})
    @TestCaseId("Read Account Transactions List - Get Account Transactions List With Account Details + Balances + Transactions Detailed Consent")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest", "getAccountDetailsWithAccountDetailsDetailedConsentTest"})
    public void getAccountTransactionsListWithBalancesTransactionsDetailedConsentTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).header("Consent-ID", consentIdBalancesTransactions).
                //queryParam("bookingStatus","both").
                queryParam("bookingStatus","pending").
                when().get("{account-id}/transactions/").
                then().statusCode(200)
                //.body("account", equalTo(accountId))
                .body("account.iban", equalTo(ACCOUNT_IBAN))
                .extract().response();

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).header("Consent-ID", consentIdBalancesTransactions).
                queryParam("bookingStatus","both").
                when().get("{account-id}/transactions/").
                then().statusCode(200)
                //.body("account", equalTo(accountId))
                .body("account.iban", equalTo(ACCOUNT_IBAN));

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).header("Consent-ID", consentIdBalancesTransactions).
                queryParam("bookingStatus","booked").
                when().get("{account-id}/transactions/").
                then().statusCode(200)
                //.body("account", equalTo(accountId))
                .body("account.iban", equalTo(ACCOUNT_IBAN));
    }




    // GET TRANSACTION DETAILS + DETAILED CONSENT





    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"5. Read Account Transaction Details - Positive Tests"})
    @TestCaseId("Read Account Transaction Details - Get Account Transaction Details With Account Details + Transactions Detailed Consent")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest", "getAccountDetailsWithAccountDetailsDetailedConsentTest", "getAccountTransactionsListWithTransactionsDetailedConsentTest"})
    public void getAccountTransactionDetailsWithTransactionsDetailedConsentTest() {

        given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdTransactions)
                .pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", transactionIdDetailed)
                .when().get("{account-id}/transactions/{transactionId}")
                .then().statusCode(200)
                .body("transactionId", equalTo(transactionIdDetailed))
                .extract().response();
    }

    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"5. Read Account Transaction Details - Positive Tests"})
    @TestCaseId("Read Account Transaction Details - Get Account Transaction Details With Transactions Detailed Consent Schema Conform")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest", "getAccountDetailsWithAccountDetailsDetailedConsentTest", "getAccountTransactionsListWithTransactionsDetailedConsentTest"})
    public void getAccountTransactionDetailsWithTransactionsDetailedConsentSchemaConformTest() {

        given().header("X-Request-ID", uuid()).header("consent-ID", consentIdTransactions)
                .pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", transactionIdDetailed).
                when().get("{account-id}/transactions/{transactionId}").then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/account/AccountTransactionDetails.json")));
    }

    @Features("1B. Account Information Service - Detailed Consent")
    @Stories({"5. Read Account Transaction Details - Positive Tests"})
    @TestCaseId("Read Account Transaction Details - Get Account Transaction Details With Account Details + Balances + Transactions Detailed Consent")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest", "getAccountDetailsWithAccountDetailsDetailedConsentTest", "getAccountTransactionsListWithTransactionsDetailedConsentTest"})
    public void getAccountTransactionDetailsWithBalancesTransactionsDetailedConsentTest() {

        given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdBalancesTransactions)
                .pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", transactionIdDetailed)
                .when().get("{account-id}/transactions/{transactionId}")
                .then().statusCode(200)
                .body("transactionId", equalTo(transactionIdDetailed))
                .extract().response();
    }







    /************************************************************************
     *
     *    Account Information Service + GLOBAL CONSENT Tests
     *
     ************************************************************************/



    // GET ACCOUNT LIST + GLOBAL CONSENT



    @Features("1C. Account Information Service - Global Consent")
    @Stories({"1. Read Accounts List - Positive Tests"})
    @TestCaseId("Read Accounts List - Get Accounts WIth Global Consent")
    @Test()
    public void getAccountsWithGlobalConsentTest() {
        given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdGlobal).
                when().get().
                then().statusCode(200);
    }

    @Features("1C. Account Information Service - Global Consent")
    @Stories({"1. Read Accounts List - Positive Tests"})
    @TestCaseId("Read Accounts List - Get Accounts WIth Global Consent Schema Conform Test")
    @Test()
    public void getAccountsWIthGlobalConsentSchemaConformTest() {
        given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdGlobal).
                when().get().then().
                body(matchesJsonSchema(new File("src/main/resources/schemas/account/AllAccounts.json")));
    }




    // GET ACCOUNT DETAILS + GLOBAL CONSENT



    @Features("1C. Account Information Service - Global Consent")
    @Stories({"2. Read Account Details - Positive Tests"})
    @TestCaseId("Read Account Details - Get Account Details with Global Consent")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest"})
    public void getAccountDetailsWithGlobalConsentTest() {

        Response response = given().pathParam("account-id", accountId)
                .header("X-Request-ID", uuid()).header("Consent-ID", consentIdGlobal).
                        when().get("{account-id}").
                        then().statusCode(200)
                .body("resourceId", equalTo(accountId))
                .extract().response();

        ibanGlobal = response.then().extract().path("iban");
        currencyGlobal = response.then().extract().path("currency");

        System.out.println(response.body().path("$").toString());
    }

    @Features("1C. Account Information Service - Global Consent")
    @Stories({"2. Read Account Details - Positive Tests"})
    @TestCaseId("Read Account Details - Get Account Details With Global Consent Schema Conform")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest"})
    public void getAccountDetailsWithGlobalConsentSchemaConformTest() {

        given().pathParam("account-id", accountId).
                header("X-Request-ID", uuid()).header("Consent-ID", consentIdGlobal).
                when().get("{account-id}").then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/account/AccountDetails.json")));
    }




    // GET ACCOUNT BALANCES + GLOBAL CONSENT





    @Features("1C. Account Information Service - Global Consent")
    @Stories({"3. Read Account Balances - Positive Tests"})
    @TestCaseId("Read Account Balances - Get Account Balances With Global Consent")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest", "getAccountDetailsWithGlobalConsentTest"})
    public void getAccountBalancesWithGlobalConsentTest() {

        given().pathParam("account-id", accountId).header("X-Request-ID", uuid()).header("Consent-ID", consentIdGlobal).
                when().get("{account-id}/balances").
                then().statusCode(200)
                .body("account.iban", equalTo(ibanGlobal))
                .extract().response();//.body("balances.balanceAmount.currency",equalTo(currencyDetailed))
    }

    @Features("1C. Account Information Service - Global Consent")
    @Stories({"3. Read Account Balances - Positive Tests"})
    @TestCaseId("Read Account Balances - Get Account Balances Schema Conform")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest", "getAccountDetailsWithGlobalConsentTest"})
    public void getAccountBalancesWithGlobalSchemaConformTest() {

        given().header("X-Request-ID", uuid()).header("consent-ID", consentIdGlobal).pathParam("account-id", accountId).
                when().get("{account-id}/balances").then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/account/AccountBalances.json")));
    }






    // GET TRANSACTIONS LIST + GLOBAL CONSENT








    @Features("1C. Account Information Service - Global Consent")
    @Stories({"4. Read Account Transactions List - Positive Tests"})
    @TestCaseId("Read Account Transactions List - Get Account Transactions List With Global Consent")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest", "getAccountDetailsWithGlobalConsentTest"})
    public void getAccountTransactionsListWithGlobalConsentTest() {

        Response response = given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).header("Consent-ID", consentIdGlobal).
                queryParam("bookingStatus","both").
                when().get("{account-id}/transactions/").
                then().statusCode(200)
                //.body("account", equalTo(accountId))
                .body("account.iban", equalTo(ACCOUNT_IBAN))
                .extract().response();

        ArrayList<String> transactionsPending = response.then().extract().body().path("transactions.pending.transactionId");
        ArrayList<String> transactionsBooked = response.then().extract().body().path("transactions.booked.transactionId");
        ArrayList<String> transactionsBoth = transactionsPending;
        transactionsBoth.addAll(transactionsBooked);

        transactionIdGlobal = (transactionsBoth).get(new Random().nextInt(transactionsBoth.size()));

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).header("Consent-ID", consentIdTransactions).
                queryParam("bookingStatus","pending").
                when().get("{account-id}/transactions/").
                then().statusCode(200)
                //.body("account", equalTo(accountId))
                .body("account.iban", equalTo(ACCOUNT_IBAN));

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).header("Consent-ID", consentIdTransactions).
                queryParam("bookingStatus","booked").
                when().get("{account-id}/transactions/").
                then().statusCode(200)
                //.body("account", equalTo(accountId))
                .body("account.iban", equalTo(ACCOUNT_IBAN));
    }

    @Features("1C. Account Information Service - Global Consent")
    @Stories({"4. Read Account Transactions List - Positive Tests"})
    @TestCaseId("Read Account Transactions List - Get Account Transactions List With Global Consent Schema Conform")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest", "getAccountDetailsWithGlobalConsentTest"})
    public void getAccountTransactionListWithGlobalConsentSchemaConformTest() {

        given().header("X-Request-ID", uuid()).header("consent-ID", consentIdGlobal)
                .pathParam("account-id", ACCOUNT_ID).queryParam("bookingStatus","both").
                when().get("{account-id}/transactions/").then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/account/AccountTransactions.json")));
    }






    // GET TRANSACTION DETAILS + GLOBAL CONSENT





    @Features("1C. Account Information Service - Global Consent")
    @Stories({"5. Read Account Transaction Details - Positive Tests"})
    @TestCaseId("Read Account Transaction Details - Get Account Transaction Details With Global Consent")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest", "getAccountDetailsWithGlobalConsentTest", "getAccountTransactionsListWithGlobalConsentTest"})
    public void getAccountTransactionDetailsWithGlobalConsentTest() {

        given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdGlobal)
                .pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", transactionIdGlobal)
                .when().get("{account-id}/transactions/{transactionId}")
                .then().statusCode(200)
                .body("transactionId", equalTo(transactionIdGlobal))
                .extract().response();
    }

    @Features("1C. Account Information Service - Global Consent")
    @Stories({"5. Read Account Transaction Details - Positive Tests"})
    @TestCaseId("Read Account Transaction Details - Get Account Transaction Details With Global Consent Schema Conform")
    @Test(alwaysRun = true, dependsOnMethods = {"getAccountsWithAvailableAccountsConsentTest", "getAccountDetailsWithGlobalConsentTest", "getAccountTransactionsListWithGlobalConsentTest"})
    public void getAccountTransactionDetailsWithGlobalConsentSchemaConformTest() {

        given().header("X-Request-ID", uuid()).header("consent-ID", consentIdGlobal)
                .pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", transactionIdGlobal).
                when().get("{account-id}/transactions/{transactionId}").then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/account/AccountTransactionDetails.json")));
    }
}