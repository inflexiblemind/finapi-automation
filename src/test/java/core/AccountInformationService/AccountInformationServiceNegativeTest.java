package core.AccountInformationService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Random;

import static core.helpers.Actions.*;
import static core.helpers.Actions.convertFileToHashMap;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;


/**
 * Created by Sergey.Syomin on 10/10/2018.
 */

@Listeners({TestExecutor.class})
public class AccountInformationServiceNegativeTest extends TestBase {

    private static String PSUId_2 = "aspsp2";
    private static String PSUId_3 = "aspsp3";
    private static String ACCOUNT_ID = "77777-999999999";
    private static String ACCOUNT_IBAN = "DE89370400440532013000";
    private static String consentIdAccountDetails_2;
    private static String consentIdBalances_2;
    private static String consentIdTransactions_2;
    private static String consentIdDetailed_2;
    private static String consentIdAvailableAccounts_2;
    private static String consentIdGlobal_2;
    private static String consentIdDetailed_3;
    private static String consentIdAvailableAccounts_3;
    private static String consentIdGlobal_3;
    private static String consentIdAccountDetailsExpired = "9195f9bf-df7c-47c0-9ec7-3c99566556bc"; // Expired 2019-01-07
    private static String consentIdAccountDetailsBalancesExpired = "333a4e1a-f13a-41e9-9234-a585b9a76858"; // Expired 2019-01-07
    private static String consentIdAccountDetailsTransactionsExpired = "ebf84ebc-5421-4445-81b5-0517c9665ee4"; // Expired 2019-01-07
    private static String consentIdAccountDetailsBalancesTransactionsExpired = "0d77272b-9145-4bf8-8f85-c5f6f7609c26"; // Expired 2019-01-07
    private static String consentIdAvailableAccountsExpired = "e1d93230-8043-4e57-bf3e-996037ec2dc9"; // Expired 2019-01-07
    private static String consentIdGlobalExpired = "cd94e6a0-0257-4436-ab43-ec5fcdbc07c9"; // Expired 2019-01-07
    private String uuid;
    private static String accountId;
    private static String transactionId = "0004";
    private static String notAuthorizedConsent;
    private static String deletedConsent;//used like deleted consent
    private static String nonExistenceConsent = uuid(); //used like nonExistence consent
    private static String expiredConsent = "7dd0dbfc-9999-4be7-a39b-e884bd188a35";
    private static String rejectedConsent = "7dd0dbfc-1111-4be7-a39b-e884bd188a35";

    @BeforeClass
    public static void setup() {

        consentIdAccountDetails_2 = getValidConsent(PSUId_2, "src/main/resources/consent/detailedConsent/createConsentAccountDetailsAccessOnly.json","src/main/resources/authorization/passwordInit2.json");
        consentIdBalances_2 = getValidConsent(PSUId_2, "src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalances.json","src/main/resources/authorization/passwordInit2.json");
        consentIdTransactions_2 = getValidConsent(PSUId_2, "src/main/resources/consent/detailedConsent/createConsentAccountDetailsTransactions.json","src/main/resources/authorization/passwordInit2.json");
        consentIdDetailed_2 = getValidAccountDetailsBalancesTransactionsConsent();
        consentIdAvailableAccounts_2 = getValidAvailableAccountsConsent();
        consentIdGlobal_2 = getValidConsent(PSUId_2, "src/main/resources/consent/globalConsent/createGlobalConsent.json","src/main/resources/authorization/passwordInit2.json");

        consentIdDetailed_3 = getValidConsent(PSUId_3, "src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalancesTransactions.json","src/main/resources/authorization/passwordInit3.json");
        consentIdAvailableAccounts_3 = getValidConsent(PSUId_3, "src/main/resources/consent/availableAccounts/createAvailableAccountsConsent.json","src/main/resources/authorization/passwordInit3.json");
        consentIdGlobal_3 = getValidConsent(PSUId_3, "src/main/resources/consent/globalConsent/createGlobalConsent.json","src/main/resources/authorization/passwordInit3.json");

        notAuthorizedConsent = getConsent(PSUId_2, "src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalancesTransactions.json");
        deletedConsent = getDeletedConsent(PSUId_2, getConsent(PSUId_2, "src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalancesTransactions.json"));



        System.out.println("\n\nconsentIdAccountDetails_2\n" + consentIdAccountDetails_2 + "\n\nnnconsentIdBalances\n" + consentIdBalances_2 + "\n\nconsentIdTransactions_2\n" + consentIdTransactions_2 + "\n\nconsentIdBalancesTransactions\n" + consentIdDetailed_2 + "\n\nconsentIdAvailableAccounts_2\n" + consentIdAvailableAccounts_2 + "\n\nconsentIdGlobal_2\n" + consentIdGlobal_2 + "\n\nconsentIdBalancesTransactions\n" + consentIdDetailed_3 + "\n\nconsentIdAvailableAccounts_3\n" + consentIdAvailableAccounts_3 + "\n\nconsentIdGlobal_3\n" + consentIdGlobal_3 + "\n\n");
    }

    @BeforeMethod
    public void setPath(){
        RestAssured.basePath = "/v1/accounts";
    }



    /************************************************************************
     *
     *    Account Information Service + AVAILABLE ACCOUNTS CONSENT Tests
     *
     ************************************************************************/
//From Access to account page

    @Features("1D. Account Information Service - Negative")
    @Stories({"1. Read Accounts List"})
    @TestCaseId("Read Accounts List - Read Account Details with non-existence AccountID Negative Test")
    @Test
    public void readAccountDetailsWithNonExistenceAccountIDNegativeTest() {

        uuid = uuid();

        given().pathParam("account-id", uuid).header("X-Request-ID", uuid()).
                header("Consent-ID", consentIdBalances_2).
                when().get("{account-id}").
                then().statusCode(404).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Account "+uuid+" is not found.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"5. Read Account Transaction Details"})
    @TestCaseId("Read Account Transaction Details - Get Account non-existence Transaction Details With Account Details + Transactions Detailed Consent Negative Test")
    @Test
    public void getAccountNonExistenceTransactionDetailsWithTransactionsDetailedConsentNegativeTest() {

        uuid=uuid();

        given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdDetailed_2).
                pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", uuid).
                when().get("{account-id}/transactions/{transactionId}").
                then().statusCode(404).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Transaction "+uuid+" is not found.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"4. Read Account Transactions List"})
    @TestCaseId("Read Account Transactions List - Get Account Transactions Without booking status Negative Test")
    @Test
    public void getAccountTransactionsWithoutBookingStatusNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", consentIdDetailed_2).
                when().get("{account-id}/transactions/").
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text",equalTo(Arrays.asList("Format of certain request fields are not matching the XS2A requirements.")));
    }


//From Access to account + consent page
//expired consent

    @Features("1D. Account Information Service - Negative")
    @Stories({"2. Read Account Details"})
    @TestCaseId("Read Account Details - Get Account Details with Expired Consent Negative Test")
    @Test
    public void getAccountDetailsWithExpiredConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).
                header("X-Request-ID", uuid()).header("Consent-ID", expiredConsent).
                when().get("{account-id}").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_EXPIRED"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+ expiredConsent +" is expired and must be renewed")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"3. Read Account Balances"})
    @TestCaseId("Read Account Balances - Get Account Balances With Expired Consent Negative Test")
    @Test
    public void getAccountBalancesWithExpiredConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", expiredConsent).
                when().get("{account-id}/balances").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_EXPIRED"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+ expiredConsent +" is expired and must be renewed")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"4. Read Account Transactions List"})
    @TestCaseId("Read Account Transactions List - Get Account Transactions List With Expired Consent Negative Test")
    @Test
    public void getAccountTransactionsListWithExpiredConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", expiredConsent).
                queryParam("bookingStatus", "both").
                when().get("{account-id}/transactions/").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_EXPIRED"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+ expiredConsent +" is expired and must be renewed")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"5. Read Account Transaction Details"})
    @TestCaseId("Read Account Transaction Details - Get Account Transaction Details With Expired Consent Negative Test")
    @Test
    public void getAccountTransactionDetailsWithExpiredConsentNegativeTest() {

        given().header("X-Request-ID", uuid()).header("Consent-ID", expiredConsent)
                .pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", uuid())
                .when().get("{account-id}/transactions/{transactionId}").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_EXPIRED"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+ expiredConsent +" is expired and must be renewed")));
    }


    //don't autorized consent

    @Features("1D. Account Information Service - Negative")
    @Stories({"2. Read Account Details"})
    @TestCaseId("Read Account Details - Get Account Details with Not Authorised Consent Negative Test")
    @Test
    public void getAccountDetailsWithNotAuthorisedConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).
                header("X-Request-ID", uuid()).header("Consent-ID", notAuthorizedConsent).
                when().get("{account-id}").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+ notAuthorizedConsent +" is not authorised.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"3. Read Account Balances"})
    @TestCaseId("Read Account Balances - Get Account Balances With Not Authorised Consent Negative Test")
    @Test
    public void getAccountBalancesWithNotAuthorisedConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", notAuthorizedConsent).
                when().get("{account-id}/balances").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+ notAuthorizedConsent +" is not authorised.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"4. Read Account Transactions List"})
    @TestCaseId("Read Account Transactions List - Get Account Transactions List With Not Authorised Consent Negative Test")
    @Test
    public void getAccountTransactionsListWithNotAuthorisedConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", notAuthorizedConsent).
                queryParam("bookingStatus", "both").
                when().get("{account-id}/transactions/").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+ notAuthorizedConsent +" is not authorised.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"5. Read Account Transaction Details"})
    @TestCaseId("Read Account Transaction Details - Get Account Transaction Details With Not Authorised Consent Negative Test")
    @Test
    public void getAccountTransactionDetailsWithNotAuthorisedConsentNegativeTest() {

        given().header("X-Request-ID", uuid()).header("Consent-ID", notAuthorizedConsent)
                .pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", uuid())
                .when().get("{account-id}/transactions/{transactionId}").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+ notAuthorizedConsent +" is not authorised.")));
    }

//deleted consent

    @Features("1D. Account Information Service - Negative")
    @Stories({"2. Read Account Details"})
    @TestCaseId("Read Account Details - Get Account Details with Deleted Consent Negative Test")
    @Test
    public void getAccountDetailsWithDeletedConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).
                header("X-Request-ID", uuid()).header("Consent-ID", deletedConsent).
                when().get("{account-id}").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+deletedConsent+" is deleted")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"3. Read Account Balances"})
    @TestCaseId("Read Account Balances - Get Account Balances With Deleted Consent Negative Test")
    @Test
    public void getAccountBalancesWithDeletedConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", deletedConsent).
                when().get("{account-id}/balances").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+deletedConsent+" is deleted")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"4. Read Account Transactions List"})
    @TestCaseId("Read Account Transactions List - Get Account Transactions List With Deleted Consent Negative Test")
    @Test
    public void getAccountTransactionsListWithDeletedConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", deletedConsent).
                queryParam("bookingStatus", "both").
                when().get("{account-id}/transactions/").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+deletedConsent+" is deleted")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"5. Read Account Transaction Details"})
    @TestCaseId("Read Account Transaction Details - Get Account Transaction Details With Deleted Consent Negative Test")
    @Test
    public void getAccountTransactionDetailsWithDeletedConsentNegativeTest() {

        given().header("X-Request-ID", uuid()).header("Consent-ID", deletedConsent).
                pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", uuid()).
                when().get("{account-id}/transactions/{transactionId}").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+deletedConsent+" is deleted")));
    }




//rejected consent

    @Features("1D. Account Information Service - Negative")
    @Stories({"2. Read Account Details"})
    @TestCaseId("Read Account Details - Get Account Details with Rejected Consent Negative Test")
    @Test(alwaysRun = false)
    public void getAccountDetailsWithRejectedConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).
                header("X-Request-ID", uuid()).header("Consent-ID", rejectedConsent).
                when().get("{account-id}").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+ rejectedConsent +" is not authorised.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"3. Read Account Balances"})
    @TestCaseId("Read Account Balances - Get Account Balances With Rejected Consent Negative Test")
    @Test(alwaysRun = false)
    public void getAccountBalancesWithRejectedConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", rejectedConsent).
                when().get("{account-id}/balances").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+ rejectedConsent +" is not authorised.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"4. Read Account Transactions List"})
    @TestCaseId("Read Account Transactions List - Get Account Transactions List With Rejected Consent Negative Test")
    @Test(alwaysRun = false)
    public void getAccountTransactionsListWithRejectedConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", rejectedConsent).
                queryParam("bookingStatus", "both").
                when().get("{account-id}/transactions/").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+ rejectedConsent +" is not authorised.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"5. Read Account Transaction Details"})
    @TestCaseId("Read Account Transaction Details - Get Account Transaction Details With Rejected Consent Negative Test")
    @Test(alwaysRun = false)
    public void getAccountTransactionDetailsWithRejectedConsentNegativeTest() {

        given().header("X-Request-ID", uuid()).header("Consent-ID", rejectedConsent).
                pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", uuid()).
                when().get("{account-id}/transactions/{transactionId}").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+ rejectedConsent +" is not authorised.")));
    }




//non-existence consent


    @Features("1D. Account Information Service - Negative")
    @Stories({"2. Read Account Details"})
    @TestCaseId("Read Account Details - Get Account Details with Non-Existence Consent Negative Test")
    @Test
    public void getAccountDetailsWithNonExistenceConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).
                header("X-Request-ID", uuid()).header("Consent-ID", nonExistenceConsent).
                when().get("{account-id}").
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+nonExistenceConsent+" doesn't exist.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"3. Read Account Balances"})
    @TestCaseId("Read Account Balances - Get Account Balances With Non-Existence Consent Negative Test")
    @Test
    public void getAccountBalancesWithNonExistenceConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", nonExistenceConsent).
                when().get("{account-id}/balances").
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+nonExistenceConsent+" doesn't exist.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"4. Read Account Transactions List"})
    @TestCaseId("Read Account Transactions List - Get Account Transactions List With Non-Existence Consent Negative Test")
    @Test
    public void getAccountTransactionsListWithNonExistenceConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", nonExistenceConsent).
                queryParam("bookingStatus", "both").
                when().get("{account-id}/transactions/").
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+nonExistenceConsent+" doesn't exist.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"5. Read Account Transaction Details"})
    @TestCaseId("Read Account Transaction Details - Get Account Transaction Details With Non-Existence Consent Negative Test")
    @Test
    public void getAccountTransactionDetailsWithNonExistenceConsentNegativeTest() {

        given().header("X-Request-ID", uuid()).header("Consent-ID", nonExistenceConsent).
                pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", uuid()).
                when().get("{account-id}/transactions/{transactionId}").
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+nonExistenceConsent+" doesn't exist.")));
    }


//execute n+1 request


    @Features("1D. Account Information Service - Negative")
    @Stories({"2. Read Account Details"})
    @TestCaseId("Read Account Details - Get Account Details with exceeding requests per day Negative Test")
    @Test(dependsOnMethods = {"getAccountNonExistenceTransactionDetailsWithTransactionsDetailedConsentNegativeTest","getAccountTransactionsWithoutBookingStatusNegativeTest"})
    public void getAccountDetailsWithExceedingRequestsPerDayConsentNegativeTest() {

        String consentIDAccountDetailsBalancesTransactions = getValidAccountDetailsBalancesTransactionsConsent();

        Map json = convertFileToHashMap("src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalancesTransactions.json");
        int quantityRequests = Integer.valueOf((String)json.get("frequencyPerDay"));
        int i = 0;
        do {
            given().pathParam("account-id", ACCOUNT_ID).
                    header("X-Request-ID", uuid()).header("Consent-ID", consentIDAccountDetailsBalancesTransactions).
                    when().get("{account-id}").
                    then().statusCode(200);
            i++;
        }while (i<quantityRequests);
            given().pathParam("account-id", ACCOUNT_ID).
                    header("X-Request-ID", uuid()).header("Consent-ID", consentIDAccountDetailsBalancesTransactions).
                    when().get("{account-id}").
                    then().statusCode(429).
                    body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                    body("tppMessages.code",equalTo(Arrays.asList("ACCESS_EXCEEDED"))).
                    body("tppMessages.text",equalTo(Arrays.asList("Access on accounts has been exceeding the daily multiplicity for consent " + consentIDAccountDetailsBalancesTransactions + ".")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"3. Read Account Balances"})
    @TestCaseId("Read Account Balances - Get Account Balances With exceeding requests per day Negative Test")
    @Test(dependsOnMethods = {"getAccountNonExistenceTransactionDetailsWithTransactionsDetailedConsentNegativeTest","getAccountTransactionsWithoutBookingStatusNegativeTest"})
    public void getAccountBalancesWithExceedingRequestsPerDayConsentNegativeTest() {

        String consentIDAccountDetailsBalancesTransactions = getValidAccountDetailsBalancesTransactionsConsent();

        Map json = convertFileToHashMap("src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalancesTransactions.json");
        int quantityRequests = Integer.valueOf((String)json.get("frequencyPerDay"));
        int i = 0;
        do {
            given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                    header("Consent-ID", consentIDAccountDetailsBalancesTransactions).
                    when().get("{account-id}/balances").
                    then().statusCode(200);
            i++;
        }while (i<quantityRequests);
            given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                    header("Consent-ID", consentIDAccountDetailsBalancesTransactions).
                    when().get("{account-id}/balances").
                    then().statusCode(429).
                    body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                    body("tppMessages.code",equalTo(Arrays.asList("ACCESS_EXCEEDED"))).
                    body("tppMessages.text",equalTo(Arrays.asList("Access on accounts has been exceeding the daily multiplicity for consent "+consentIDAccountDetailsBalancesTransactions+".")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"4. Read Account Transactions List"})
    @TestCaseId("Read Account Transactions List - Get Account Transactions List With exceeding requests per day Negative Test")
    @Test(dependsOnMethods = {"getAccountNonExistenceTransactionDetailsWithTransactionsDetailedConsentNegativeTest","getAccountTransactionsWithoutBookingStatusNegativeTest"})
    public void getAccountTransactionsListWithExceedingRequestsPerDayConsentNegativeTest() {

        String consentIDAccountDetailsBalancesTransactions = getValidAccountDetailsBalancesTransactionsConsent();

        Map json = convertFileToHashMap("src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalancesTransactions.json");
        int quantityRequests = Integer.valueOf((String)json.get("frequencyPerDay"));
        int i = 0;
        do {
            given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                    header("Consent-ID", consentIDAccountDetailsBalancesTransactions).
                    queryParam("bookingStatus", "both").
                    when().get("{account-id}/transactions/").
                    then().statusCode(200);
            i++;
        }while (i<quantityRequests);
            given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                    header("Consent-ID", consentIDAccountDetailsBalancesTransactions).
                    queryParam("bookingStatus", "both").
                    when().get("{account-id}/transactions/").
                    then().statusCode(429).
                    body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                    body("tppMessages.code",equalTo(Arrays.asList("ACCESS_EXCEEDED"))).
                    body("tppMessages.text",equalTo(Arrays.asList("Access on accounts has been exceeding the daily multiplicity for consent "+consentIDAccountDetailsBalancesTransactions+".")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"5. Read Account Transaction Details"})
    @TestCaseId("Read Account Transaction Details - Get Account Transaction Details With exceeding requests per day Negative Test")
    @Test()
    public void getAccountTransactionDetailsWithExceedingRequestsPerDayNegativeTest() {

        String consentIDAccountDetailsBalancesTransactions = getValidAccountDetailsBalancesTransactionsConsent();

        Map json = convertFileToHashMap("src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalancesTransactions.json");
        int quantityRequests = Integer.valueOf((String)json.get("frequencyPerDay"));
        int i = 0;
        do {
            given().header("X-Request-ID", uuid()).header("Consent-ID", consentIDAccountDetailsBalancesTransactions).
                    pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", "0004").
                    when().get("{account-id}/transactions/{transactionId}").
                    then().statusCode(200);
            i++;
        }while (i<quantityRequests);
            given().header("X-Request-ID", uuid()).header("Consent-ID", consentIDAccountDetailsBalancesTransactions).
                    pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", "0004").
                    when().get("{account-id}/transactions/{transactionId}").
                    then().statusCode(429).
                    body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                    body("tppMessages.code",equalTo(Arrays.asList("ACCESS_EXCEEDED"))).
                    body("tppMessages.text",equalTo(Arrays.asList("Access on accounts has been exceeding the daily multiplicity for consent "+consentIDAccountDetailsBalancesTransactions+".")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"1. Read Accounts List"})
    @TestCaseId("Read Accounts List - Get Accounts List With exceeding Available Accounts Consent requests per day Negative Test")
    @Test()
    public void getAvailableAccountsExceedingRequestsNegativeTest() {

        String consentIdAvailableAccounts = getValidAvailableAccountsConsent();

        Map json = convertFileToHashMap("src/main/resources/consent/availableAccounts/createAvailableAccountsConsent.json");
        int quantityRequests = Integer.valueOf((String)json.get("frequencyPerDay"));
        int i = 0;
        do {
            given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdAvailableAccounts).
                    when().get().
                    then().statusCode(200);
            i++;
        }while (i<quantityRequests);
        given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdAvailableAccounts).
                when().get().
                then().statusCode(429).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("ACCESS_EXCEEDED"))).
                body("tppMessages.text",equalTo(Arrays.asList("Access on accounts has been exceeding the daily multiplicity for consent " + consentIdAvailableAccounts + ".")));
    }

//without consent


    @Features("1D. Account Information Service - Negative")
    @Stories({"2. Read Account Details"})
    @TestCaseId("Read Account Details - Get Account Details With non-existent Consent Negative Test")
    @Test
    public void getAccountDetailsWithNonExistentConsentNegativeTest() {

        uuid=uuid();
        given().pathParam("account-id", ACCOUNT_ID).
                header("X-Request-ID", uuid()).header("Consent-ID", uuid).
                when().get("{account-id}").
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+uuid+" doesn't exist.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"3. Read Account Balances"})
    @TestCaseId("Read Account Balances - Get Account Balances Without Consent Negative Test")
    @Test
    public void getAccountBalancesWithoutConsentNegativeTest() {
        uuid=uuid();
        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", uuid).
                when().get("{account-id}/balances").
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+uuid+" doesn't exist.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"4. Read Account Transactions List"})
    @TestCaseId("Read Account Transactions List - Get Account Transactions List Without Consent Negative Test")
    @Test
    public void getAccountTransactionsListWithoutConsentNegativeTest() {
        uuid=uuid();
        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", uuid).
                queryParam("bookingStatus", "both").
                when().get("{account-id}/transactions/").
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+uuid+" doesn't exist.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"5. Read Account Transaction Details"})
    @TestCaseId("Read Account Transaction Details - Get Account Transaction Details Without Consent Negative Test")
    @Test
    public void getAccountTransactionDetailsWithoutConsentNegativeTest() {
        uuid=uuid();
        given().header("X-Request-ID", uuid()).header("Consent-ID", uuid).
                pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", uuid()).
                when().get("{account-id}/transactions/{transactionId}").
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+uuid+" doesn't exist.")));
    }


//get Account balances with consent account details access only

    @Features("1D. Account Information Service - Negative")
    @Stories({"3. Read Account Balances"})
    @TestCaseId("Read Account Balances - Get Account Balances With Consent Account Details Access Only Negative Test")
    @Test
    public void getAccountBalancesWithConsentAccountDetailsAccessOnlyNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", consentIdAccountDetails_2).
                when().get("{account-id}/balances").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Given consent is not valid for requested resource.")));
    }


//Get Account Balances With Consent Account Transactions Access Only

    @Features("1D. Account Information Service - Negative")
    @Stories({"3. Read Account Balances"})
    @TestCaseId("Read Account Balances - Get Account Balances With Consent Account Transactions Access Only Negative Test")
    @Test
    public void getAccountBalancesWithConsentAccountTransactionsAccessOnlyNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", consentIdTransactions_2).
                when().get("{account-id}/balances").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Given consent is not valid for requested resource.")));
    }


//Get Account Transactions List With Consent Account Details Access Only


    @Features("1D. Account Information Service - Negative")
    @Stories({"4. Read Account Transactions List"})
    @TestCaseId("Read Account Transactions List - Get Account Transactions List With Consent Account Details Access Only Negative Test")
    @Test
    public void getAccountTransactionsListWithConsentAccountDetailsAccessOnlyNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", consentIdAccountDetails_2).
                queryParam("bookingStatus", "both").
                when().get("{account-id}/transactions/").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Given consent is not valid for requested resource")));
    }



//Get Account Transactions List With Consent Account Balances Access Only

    @Features("1D. Account Information Service - Negative")
    @Stories({"4. Read Account Transactions List"})
    @TestCaseId("Read Account Transactions List - Get Account Transactions List With Consent Account Balances Access Only Negative Test")
    @Test
    public void getAccountTransactionsListWithConsentAccountBalancesAccessOnlyNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", consentIdBalances_2).
                queryParam("bookingStatus", "both").
                when().get("{account-id}/transactions/").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Given consent is not valid for requested resource")));
    }

//Get Account Transaction Details With Consent Account Details Access Only

    @Features("1D. Account Information Service - Negative")
    @Stories({"4. Read Account Transaction Details"})
    @TestCaseId("Read Account Transaction Details - Get Account Transaction Details With Consent Account Details Access Only Negative Test")
    @Test
    public void getAccountTransactionDetailsWithConsentAccountDetailsAccessOnlyNegativeTest() {

        given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdAccountDetails_2)
                .pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", uuid())
                .when().get("{account-id}/transactions/{transactionId}").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Given consent is not valid for requested resource")));
    }



//Get Account Transaction Details With Consent Account Balances Access Only

    @Features("1D. Account Information Service - Negative")
    @Stories({"4. Read Account Transaction Details"})
    @TestCaseId("Read Account Transaction Details - Get Account Transaction Details With Consent Account Balances Access Only Negative Test")
    @Test
    public void getAccountTransactionDetailsWithConsentAccountBalancesAccessOnlyNegativeTest() {

        given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdBalances_2)
                .pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", uuid())
                .when().get("{account-id}/transactions/{transactionId}").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Given consent is not valid for requested resource")));
    }


    //AVAILABLE ACCOUNTS CONSENT - consentIdAvailableAccounts_3

    @Features("1A. Account Information Service - Available Accounts Consent")
    @Stories({"1. Read Accounts List - Positive Tests"})
    @TestCaseId("Read Accounts List - Get Accounts WIth Available Accounts Consent")
    @Test()
    public void getAccountsWithAvailableAccountsConsentTest() {

        ArrayList<String> accounts = given().header("X-Request-ID", uuid()).
                header("Consent-ID", consentIdAvailableAccounts_3).
                when().get().
                then().statusCode(200).
                extract().body().path("accounts.resourceId");

        accountId = accounts.get(new Random().nextInt(accounts.size()));

        System.out.println("Account for Account Details and get Balances tests chosen: " + accountId);
    }

    //DETAILED CONSENT - consentIdDetailed_3

    @Features("1D. Account Information Service - Negative")
    @Stories({"1. Read Accounts List"})
    @TestCaseId("Read Accounts List - Read Account Details when AccountID not belong PSU from Detailed Consent Negative Test")
    @Test
    public void readAccountDetailsWhenAccountIDNotBelongPSUDetailedConsentNegativeTest() {

        //given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
        given().pathParam("account-id", "11111-999999999").header("X-Request-ID", uuid()).
                header("Consent-ID", consentIdDetailed_3).
                when().get("{account-id}").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Given consent is not valid for requested resource.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"3. Read Account Balances"})
    @TestCaseId("Read Account Balances - Get Account Balances when AccountID not belong PSU from Detailed Consent Negative Test")
    @Test
    public void getAccountBalancesWhenAccountIDNotBelongPSUDetailedConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", consentIdDetailed_3).
                when().get("{account-id}/balances").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Given consent is not valid for requested resource.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"4. Read Account Transactions List"})
    @TestCaseId("Read Account Transactions List - Get Account Transactions List when AccountID not belong PSU from Detailed Consent Negative Test")
    @Test
    public void getAccountTransactionsListWhenAccountIDNotBelongPSUDetailedConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", consentIdDetailed_3).
                queryParam("bookingStatus", "both").
                when().get("{account-id}/transactions/").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Given consent is not valid for requested resource.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"4. Read Account Transaction Details"})
    @TestCaseId("Read Account Transaction Details - Get Account Transaction Details when AccountID not belong PSU from Detailed Consent Negative Test")
    @Test
    public void getAccountTransactionDetailsWhenAccountIDNotBelongPSUDetailedConsentNegativeTest() {

        given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdDetailed_3)
                .pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", transactionId)
                .when().get("{account-id}/transactions/{transactionId}").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Given consent is not valid for requested resource.")));
    }


    //GLOBAL CONSENT - consentIdGlobal_3

    @Features("1D. Account Information Service - Negative")
    @Stories({"1. Read Accounts List"})
    @TestCaseId("Read Accounts List - Read Account Details when AccountID not belong PSU from Global Consent Negative Test")
    @Test
    public void readAccountDetailsWhenAccountIDNotBelongPSUGlobalConsentNegativeTest() {

        //given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
        given().pathParam("account-id", "11111-999999999").header("X-Request-ID", uuid()).
                header("Consent-ID", consentIdGlobal_3).
                when().get("{account-id}").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Given consent is not valid for requested resource.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"3. Read Account Balances"})
    @TestCaseId("Read Account Balances - Get Account Balances when AccountID not belong PSU from Global Consent Negative Test")
    @Test
    public void getAccountBalancesWhenAccountIDNotBelongPSUGlobalConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", consentIdGlobal_3).
                when().get("{account-id}/balances").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Given consent is not valid for requested resource.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"4. Read Account Transactions List"})
    @TestCaseId("Read Account Transactions List - Get Account Transactions List when AccountID not belong PSU from Global Consent Negative Test")
    @Test
    public void getAccountTransactionsListWhenAccountIDNotBelongPSUGlobalConsentNegativeTest() {

        given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", consentIdGlobal_3).
                queryParam("bookingStatus", "both").
                when().get("{account-id}/transactions/").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Given consent is not valid for requested resource.")));
    }

    @Features("1D. Account Information Service - Negative")
    @Stories({"4. Read Account Transaction Details"})
    @TestCaseId("Read Account Transaction Details - Get Account Transaction Details when AccountID not belong PSU from Global Consent Negative Test")
    @Test
    public void getAccountTransactionDetailsWhenAccountIDNotBelongPSUGlobalConsentNegativeTest() {

        given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdGlobal_3)
                .pathParam("account-id", ACCOUNT_ID).pathParam("transactionId", transactionId)
                .when().get("{account-id}/transactions/{transactionId}").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Given consent is not valid for requested resource.")));
    }

    private static String getValidAvailableAccountsConsent() {
        return getValidConsent(PSUId_2, "src/main/resources/consent/availableAccounts/createAvailableAccountsConsent.json","src/main/resources/authorization/passwordInit2.json");
    }

    private static String getValidAccountDetailsBalancesTransactionsConsent() {
        return getValidConsent(PSUId_2, "src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalancesTransactions.json","src/main/resources/authorization/passwordInit2.json");
    }
}