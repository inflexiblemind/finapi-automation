package core.AuthorizationsService;

import core.base.TestBase;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;

import static core.helpers.Actions.date;
import static core.helpers.Actions.uuid;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

public class NegativePaymentAuthorizationTest extends TestBase{

    private String sepa = "sepa-credit-transfers";
    private String authorisationURL;
    private String uuid;
    private String PSU_ID = "aspsp2";
    private String providePSUCredentialsURL;
    private String chooseSCAMethodURL;
    private String authoriseTransactionURL;
    private HashMap<String, String> URLs = new HashMap<>();
    private Response response;
    private String basePath;

    @BeforeClass
    public void setup() {
        basePath = "/v1/payments";
        RestAssured.basePath = basePath;
    }

    @BeforeMethod
    public void setPath(){
        setup();
    }


     @Features("4B. Payment Authorization Service")
     @Stories({"2. Negative scenario"})
     @TestCaseId("Payment Initiation Service - Initiate Payment with incorrect PSU-ID")
     @Test()
     public void initiatePaymentNegative() {
         uuid = uuid();
         response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID+"failed").header("date", date()).contentType("application/json").
                 body(new File("src/main/resources/payment/paymentInitiate.json")).
                 when().post(sepa).
                 then().statusCode(401).
                 body("tppMessages.category", equalTo(Arrays.asList("ERROR"))).
                 body("tppMessages.text", equalTo(Arrays.asList("PSU-ID validation failed"))).
                 body("tppMessages.code", equalTo(Arrays.asList("PSU_CREDENTIALS_INVALID"))).
                 extract().response();
     }

    @Features("4B. Payment Authorization Service")
    @Stories({"3. Preparation for negative scenario"})
    @TestCaseId("Payment Initiation Service - Initiate Payment for incorrect password negative test")
    @Test(dependsOnMethods = "initiatePaymentNegative")
    public void initiatePaymentTestForNegativePasswordValidation() {
        uuid = uuid();
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/paymentInitiate.json")).
                when().post(sepa).
                then().statusCode(201).
                extract().path("_links");
        authorisationURL=URLs.get("startAuthorisation").split("/payments")[1];
    }


     @Features("4B. Payment Authorization Service")
     @Stories({"3. Preparation for negative scenario"})
     @TestCaseId("Payment Initiation Service - Start authentication")
     @Test(dependsOnMethods = "initiatePaymentTestForNegativePasswordValidation")
     public void startAuthenticationForIncorrectPasswordNegative() {
         URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                 post(authorisationURL).
                 then().statusCode(200).
                 extract().path("_links");
         providePSUCredentialsURL = URLs.get("updatePsuAuthentication").split("/payments")[1];
     }


     @Features("4B. Payment Authorization Service")
     @Stories({"2. Negative scenario"})
     @TestCaseId("Payment Initiation Service - Provide invalid PSU credentials")
     @Test(dependsOnMethods = "startAuthenticationForIncorrectPasswordNegative")
     public void sendPasswordNegative() {
         response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                 body(new File("src/main/resources/authorization/invalidPasswordInit.json")).
                 put(providePSUCredentialsURL).
                 then().statusCode(401).
                 header("Aspsp-Sca-Approach", equalTo("EMBEDDED")).
                 body("psuMessage", equalTo("Invalid password. Please, retry")).
                 body("scaStatus", equalTo("failed")).
                 extract().response();
         providePSUCredentialsURL = URLs.get("updatePsuAuthentication").split("/payments")[1];
     }

    @Features("4B. Payment Authorization Service")
    @Stories({"3. Preparation for negative scenario"})
    @TestCaseId("Payment Initiation Service - Provide PSU credentials for invalid SCA method test")
    @Test(dependsOnMethods = "sendPasswordNegative")
    public void sendPasswordForNegative() {
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/authorization/passwordInit2.json")).
                put(providePSUCredentialsURL).
                then().statusCode(200).
                extract().path("_links");
        chooseSCAMethodURL = URLs.get("selectAuthenticationMethod").split("/payments")[1];
    }

     @Features("4B. Payment Authorization Service")
     @Stories({"2. Negative scenario"})
     @TestCaseId("Payment Initiation Service - Provide invalid SCA method")
     @Test(dependsOnMethods = "sendPasswordForNegative")
     public void provideSCAMethodNegative() {
         response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                 body(new File("src/main/resources/authorization/invalidAuthenticationMethodInit.json")).
                 put(providePSUCredentialsURL).
                 then().statusCode(400).
                 body("scaStatus", equalTo("failed")).
                 body("psuMessage",equalTo("Selected SCA method is not supported. Please, restart authorisation.")).
                 extract().response();
         URLs = response.path("_links");
         authorisationURL = URLs.get("startAuthorisation").split("/payments")[1];
     }

    @Features("4B. Payment Authorization Service")
    @Stories({"3. Preparation for negative scenario"})
    @TestCaseId("Payment Initiation Service - Start authentication")
    @Test(dependsOnMethods = "provideSCAMethodNegative")
    public void startAuthenticationForIncorrectChallengeDataNegative() {
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                post(authorisationURL).
                then().statusCode(200).
                extract().path("_links");
        providePSUCredentialsURL = URLs.get("updatePsuAuthentication").split("/payments")[1];
    }

    @Features("4B. Payment Authorization Service")
    @Stories({"3. Preparation for negative scenario"})
    @TestCaseId("Payment Initiation Service - Provide PSU credentials for invalid challenge test")
    @Test(dependsOnMethods = "startAuthenticationForIncorrectChallengeDataNegative")
    public void sendPasswordForNegativeAuthorization() {
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/authorization/passwordInit2.json")).
                put(providePSUCredentialsURL).
                then().statusCode(200).
                extract().path("_links");
        chooseSCAMethodURL = URLs.get("selectAuthenticationMethod").split("/payments")[1];
    }

    @Features("4B. Payment Authorization Service")
    @Stories({"3. Preparation for negative scenario"})
    @TestCaseId("Payment Initiation Service - Provide SCA method for invalid challenge test")
    @Test(dependsOnMethods = "sendPasswordForNegativeAuthorization")
    public void provideSCAMethodForNegativeAuthorization() {
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/authorization/authenticationMethodInit.json")).
                put(chooseSCAMethodURL).
                then().statusCode(200).
                extract().path("_links");
        authoriseTransactionURL = URLs.get("authoriseTransaction").split("/payments")[1];
    }

     @Features("4B. Payment Authorization Service")
     @Stories({"2. Negative scenario"})
     @TestCaseId("Payment Initiation Service - Provide invalid challenge")
     @Test(dependsOnMethods = "provideSCAMethodForNegativeAuthorization")
     public void provideChallengeNegative() {
         response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).contentType("application/json").
                 body("{  \"scaAuthenticationData\": \"fail\" } ").
                 put(authoriseTransactionURL).
                 then().statusCode(401).
//                 statusLine("UNAUTHORIZED").
                 body("psuMessage", equalTo("PSU authorisation failed. Please, restart authorisation.")).
                 body("scaStatus", equalTo("failed")).
                 extract().response();
         URLs = response.path("_links");
         authorisationURL = URLs.get("startAuthorisation");
     }

}
