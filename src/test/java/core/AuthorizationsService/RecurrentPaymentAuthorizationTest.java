package core.AuthorizationsService;

import core.base.TestBase;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import static core.helpers.Actions.date;
import static core.helpers.Actions.uuid;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

public class RecurrentPaymentAuthorizationTest extends TestBase{

    private String sepa = "sepa-credit-transfers";
    private String authorisationURL;
    private String uuid;
    private String PSU_ID = "aspsp2";
    private String providePSUCredentialsURL;
    private String chooseSCAMethodURL;
    private String authoriseTransactionURL;
    private HashMap<String, String> URLs = new HashMap<>();
    private Response response;
    private String paymentId;
    private String basePath;
    private String authorizationId;

    @BeforeClass
    public void setup() {
        basePath = "/v1/periodic-payments";
        RestAssured.basePath = basePath;
    }

    @BeforeMethod
    public void setPath(){
        setup();
    }

    @Features("4C. Recurrent Payment Authorization Service")
    @Stories({"1. Positive scenario"})
    @TestCaseId("Recurrent Payment Initiation Service - Initiate Payment for PSU")
    @Test()
    public void initiatePaymentTest() {
        uuid = uuid();
        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/sepa-credit-transfer/paymentInitiateSepaRecurrent.json")).
                when().post(sepa);
        paymentId = response.then().statusCode(201).extract().path("paymentId");
        authorisationURL = response.then().statusCode(201).
                header("Aspsp-Sca-Approach", equalTo("EMBEDDED")).
                body("transactionStatus", equalTo("RCVD")).
                body("paymentId", notNullValue()).
                body("_links", notNullValue()).
                body("_links.startAuthorisation", equalTo(basePath + "/" + sepa + "/" + paymentId + "/authorisations")).
                extract().path("_links.startAuthorisation");
        authorisationURL = authorisationURL.split("/periodic-payments")[1];
    }


    @Features("4C. Recurrent Payment Authorization Service")
    @Stories({"1. Positive scenario"})
    @TestCaseId("Recurrent Payment Initiation Service - Start authentication")
    @Test(dependsOnMethods = "initiatePaymentTest")
    public void startAuthentication() {
        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                post(authorisationURL);
//        authorizationId = response.then().statusCode(200).extract().path("authorisationId");
        providePSUCredentialsURL = response.then().statusCode(200).
                header("Aspsp-Sca-Approach", equalTo("EMBEDDED")).
                body("scaStatus", equalTo("received")).
//                body("authorisationId", notNullValue()).
//                body("_links.updatePsuAuthentication", equalTo(basePath + "/" + sepa + "/" + paymentId + "/authorisations"+"/" + authorizationId)).
        extract().path("_links.updatePsuAuthentication");
        providePSUCredentialsURL = providePSUCredentialsURL.split("/periodic-payments")[1];
        authorizationId = providePSUCredentialsURL.split("authorisations/")[1];

    }


    @Features("4C. Recurrent Payment Authorization Service")
    @Stories({"1. Positive scenario"})
    @TestCaseId("Recurrent Payment Initiation Service - Provide PSU credentials")
    @Test(dependsOnMethods = "startAuthentication")
    public void sendPassword() {
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/authorization/passwordInit2.json")).
                put(providePSUCredentialsURL).
//                put(providePSUCredentialsURL).
        then().statusCode(200).
                        header("Aspsp-Sca-Approach", equalTo("EMBEDDED")).
                        body("scaStatus", equalTo("psuAuthenticated")).
                        body("scaMethods ", notNullValue()).
                        body("_links.selectAuthenticationMethod", equalTo(basePath + "/" + sepa + "/" + paymentId + "/authorisations"+"/" + authorizationId)).
                        extract().path("_links");
        chooseSCAMethodURL = URLs.get("selectAuthenticationMethod").split("/periodic-payments")[1];
    }

    @Features("4C. Recurrent Payment Authorization Service")
    @Stories({"1. Positive scenario"})
    @TestCaseId("Recurrent Payment Initiation Service - Get authorization ID")
    @Test(dependsOnMethods = "sendPassword")
    public void getAuthorizationID() {
        Response response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                get(  "/" +sepa+"/"+ paymentId + "/authorisations"+"/").
                then().statusCode(200).extract().response();
        response.path("authorisationIds").equals(authorizationId);
    }

    @Features("4C. Recurrent Payment Authorization Service")
    @Stories({"1. Positive scenario"})
    @TestCaseId("Recurrent Payment Initiation Service - Get SCA status")
    @Test(dependsOnMethods = "getAuthorizationID")
    public void getScaStatus() {
        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                get("/" + sepa + "/" + paymentId + "/authorisations"+"/" + authorizationId).
                then().statusCode(200).
                body("scaStatus", equalTo("psuAuthenticated"));
    }

    @Features("4C. Recurrent Payment Authorization Service")
    @Stories({"1. Positive scenario"})
    @TestCaseId("Recurrent Payment Initiation Service - Provide SCA method")
    @Test(dependsOnMethods = "getScaStatus")
    public void provideSCAMethod() {
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/authorization/authenticationMethodInit.json")).
                put(chooseSCAMethodURL).
                then().statusCode(200).
                header("Aspsp-Sca-Approach", equalTo("EMBEDDED")).
                body("scaStatus", equalTo("scaMethodSelected")).
                body("chosenScaMethod.authenticationMethodId", equalTo("chip")).
                body("_links.authoriseTransaction", equalTo(basePath + "/" + sepa + "/" + paymentId + "/authorisations"+"/" + authorizationId)).
                extract().path("_links");
        authoriseTransactionURL = URLs.get("authoriseTransaction").split("/periodic-payments")[1];
    }

    @Features("4C. Recurrent Payment Authorization Service")
    @Stories({"1. Positive scenario"})
    @TestCaseId("Recurrent Payment Initiation Service - Provide challenge")
    @Test(dependsOnMethods = "provideSCAMethod")
    public void provideChallenge() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat date = new SimpleDateFormat("ddmm");
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).contentType("application/json").
                body("{  \"scaAuthenticationData\": \""+date.format(cal.getTime())+"\" } ").
                put(authoriseTransactionURL).
                then().statusCode(200).
                header("Aspsp-Sca-Approach", equalTo("EMBEDDED")).
                body("scaStatus", equalTo("finalised")).
                body("_links.scaStatus", equalTo(basePath + "/" + sepa + "/" + paymentId + "/authorisations"+"/" + authorizationId)).
                extract().path("_links");

    }

}
