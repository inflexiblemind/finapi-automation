package core.AuthorizationsService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.util.Arrays;
import java.util.HashMap;

import static core.helpers.Actions.date;
import static core.helpers.Actions.uuid;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@Listeners({TestExecutor.class})
public class NegativeConsentAuthroizationTest extends TestBase{

    private String consent = "consents";
    private String authorisationURL;
    private String uuid;
    private String PSU_ID = "aspsp2";
    private String providePSUCredentialsURL;
    private String chooseSCAMethodURL;
    private String authoriseTransactionURL;
    private HashMap<String, String> URLs = new HashMap<>();
    private String basePath;
    private Response response;

    @BeforeClass
    public void setup() {
        basePath = "/v1";
        RestAssured.basePath = basePath;
    }

    @BeforeMethod
    public void setPath(){
        setup();
    }

    @Features("4A. Consent Authorization Service")
    @Stories({"2. Negative scenario"})
    @TestCaseId("Consent Initiation Service - Initiate Consent with incorrect PSU-ID")
    @Test()
    public void initiatePaymentNegative() {
        uuid = uuid();
        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID+"failed").header("date", date()).contentType("application/json").
                body(new File("src/main/resources/authorization/createConsent.json")).
                when().post(consent).
                then().statusCode(401).
                body("tppMessages.category", equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("PSU-ID validation failed"))).
                body("tppMessages.code", equalTo(Arrays.asList("PSU_CREDENTIALS_INVALID"))).
                extract().response();

    }

    @Features("4A. Consent Authorization Service")
    @Stories({"3. Preparation for negative scenario"})
    @TestCaseId("Consent Initiation Service - Initiate Consent for incorrect password negative test")
    @Test(dependsOnMethods = "initiatePaymentNegative")
    public void initiatePaymentTestForNegativePasswordValidation() {
        uuid = uuid();
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/authorization/createConsent.json")).
                when().post(consent).
                then().statusCode(201).
                extract().path("_links");
        authorisationURL=URLs.get("startAuthorisation").split("/v1")[1];
    }


    @Features("4A. Consent Authorization Service")
    @Stories({"3. Preparation for negative scenario"})
    @TestCaseId("Consent Initiation Service - Start authentication")
    @Test(dependsOnMethods = "initiatePaymentTestForNegativePasswordValidation")
    public void startAuthenticationForIncorrectPasswordNegative() {
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                post(authorisationURL).
                then().statusCode(200).
                extract().path("_links");
        providePSUCredentialsURL = URLs.get("updatePsuAuthentication").split("/v1")[1];
    }


    @Features("4A. Consent Authorization Service")
    @Stories({"2. Negative scenario"})
    @TestCaseId("Consent Initiation Service - Provide invalid PSU credentials")
    @Test(dependsOnMethods = "startAuthenticationForIncorrectPasswordNegative")
    public void sendPasswordNegative() {
         response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                 body(new File("src/main/resources/authorization/invalidPasswordInit.json")).
                 put(providePSUCredentialsURL).
                 then().statusCode(401).
                 header("Aspsp-Sca-Approach", equalTo("EMBEDDED")).
                 body("psuMessage", equalTo("Invalid password. Please, retry")).
                 body("scaStatus", equalTo("failed")).
                 extract().response();
        URLs = response.path("_links");
        providePSUCredentialsURL = URLs.get("updatePsuAuthentication").split("/v1")[1];
    }

    @Features("4A. Consent Authorization Service")
    @Stories({"3. Preparation for negative scenario"})
    @TestCaseId("Consent Initiation Service - Start authentication")
    @Test(dependsOnMethods = "sendPasswordNegative")
    public void startAuthenticationForIncorrectScaMethodNegative() {
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                post(authorisationURL).
                then().statusCode(200).
                extract().path("_links");
        providePSUCredentialsURL = URLs.get("updatePsuAuthentication").split("/v1")[1];
    }

    @Features("4A. Consent Authorization Service")
    @Stories({"3. Preparation for negative scenario"})
    @TestCaseId("Consent Initiation Service - Provide PSU credentials for invalid SCA method test")
    @Test(dependsOnMethods = "startAuthenticationForIncorrectScaMethodNegative")
    public void sendPasswordForNegative() {
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/authorization/passwordInit2.json")).
                put(providePSUCredentialsURL).
                then().statusCode(200).
                extract().path("_links");
        chooseSCAMethodURL = URLs.get("selectAuthenticationMethod").split("/v1")[1];
    }

    @Features("4A. Consent Authorization Service")
    @Stories({"2. Negative scenario"})
    @TestCaseId("Consent Initiation Service - Provide invalid SCA method")
    @Test(dependsOnMethods = "sendPasswordForNegative")
    public void provideSCAMethodNegative() {
         response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                 body(new File("src/main/resources/authorization/invalidAuthenticationMethodInit.json")).
                 put(providePSUCredentialsURL).
                 then().statusCode(400).
                 body("psuMessage", equalTo("Selected SCA method is not supported. Please, restart authorisation.")).
                 body("scaStatus", equalTo("failed")).
                 extract().response();
        URLs = response.path("_links");
        authorisationURL = URLs.get("startAuthorisation").split("/v1")[1];

    }

    @Features("4A. Consent Authorization Service")
    @Stories({"3. Preparation for negative scenario"})
    @TestCaseId("Consent Initiation Service - Start authentication")
    @Test(dependsOnMethods = "provideSCAMethodNegative")
    public void startAuthenticationForIncorrectChallengeDataNegative() {
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                post(authorisationURL).
                then().statusCode(200).
                extract().path("_links");
        providePSUCredentialsURL = URLs.get("updatePsuAuthentication").split("/v1")[1];
    }

    @Features("4A. Consent Authorization Service")
    @Stories({"3. Preparation for negative scenario"})
    @TestCaseId("Consent Initiation Service - Provide PSU credentials for invalid challenge test")
    @Test(dependsOnMethods = "startAuthenticationForIncorrectChallengeDataNegative")
    public void sendPasswordForNegativeAuthorization() {
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/authorization/passwordInit2.json")).
                put(providePSUCredentialsURL).
                then().statusCode(200).
                extract().path("_links");
        chooseSCAMethodURL = URLs.get("selectAuthenticationMethod").split("/v1")[1];
    }

    @Features("4A. Consent Authorization Service")
    @Stories({"3. Preparation for negative scenario"})
    @TestCaseId("Consent Initiation Service - Provide SCA method for invalid challenge test")
    @Test(dependsOnMethods = "sendPasswordForNegativeAuthorization")
    public void provideSCAMethodForNegativeAuthorization() {
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/authorization/authenticationMethodInit.json")).
                put(chooseSCAMethodURL).
                then().statusCode(200).
                extract().path("_links");
        authoriseTransactionURL = URLs.get("authoriseTransaction").split("/v1")[1];
    }

    @Features("4A. Consent Authorization Service")
    @Stories({"2. Negative scenario"})
    @TestCaseId("Consent Initiation Service - Provide invalid challenge")
    @Test(dependsOnMethods = "provideSCAMethodForNegativeAuthorization")
    public void provideChallengeNegative() {
         response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).contentType("application/json").
                 body("{  \"scaAuthenticationData\": \"fail\" } ").
                 put(authoriseTransactionURL).
                 then().statusCode(401).
                 body("psuMessage", equalTo("PSU authorisation failed. Please, restart authorisation.")).
                 body("scaStatus", equalTo("failed")).
                 extract().response();
        URLs = response.path("_links");
        authorisationURL = URLs.get("startAuthorisation").split("/v1")[1];

    }

}
