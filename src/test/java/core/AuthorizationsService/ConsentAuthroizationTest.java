package core.AuthorizationsService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;

import static core.helpers.Actions.date;
import static core.helpers.Actions.uuid;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

@Listeners({TestExecutor.class})
public class ConsentAuthroizationTest extends TestBase{

    private String consent = "consents";
    private String authorisationURL;
    private String uuid;
    private String PSU_ID = "aspsp2";
    private String providePSUCredentialsURL;
    private String chooseSCAMethodURL;
    private String authoriseTransactionURL;
    private HashMap<String, String> URLs = new HashMap<>();
    private String basePath;
    private String consentId;
    private String authorisationId;
    private Response response;

    @BeforeClass
    public void setup() {
        basePath = "/v1";
        RestAssured.basePath = basePath;
    }

    @BeforeMethod
    public void setPath(){
        setup();
    }

    @Features("4A. Consent Authorization Service")
    @Stories({"1. Positive scenario"})
    @TestCaseId("Consents Initiation Service - Initiate Payment for PSU")
    @Test()
    public void initiatePaymentTest() {
        uuid = uuid();
        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/authorization/createConsent.json")).
                when().post(consent);
        consentId = response.then().statusCode(201).extract().path("consentId");
        URLs = response.then().
                header("Aspsp-Sca-Approach", equalTo("EMBEDDED")).
                body("consentId", notNullValue()).
                body("consentStatus", equalTo("received")).
                body("_links.startAuthorisation", equalTo(basePath + "/" + consent + "/" + consentId + "/authorisations")).
                extract().path("_links");
        authorisationURL=URLs.get("startAuthorisation").split("/v1")[1];
    }


    @Features("4A. Consent Authorization Service")
    @Stories({"1. Positive scenario"})
    @TestCaseId("Consents Initiation Service - Start authentication")
    @Test(dependsOnMethods = "initiatePaymentTest")
    public void startAuthentication() {
        response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                post(authorisationURL);
//        authorisationId = response.then().statusCode(200).
//                extract().path("authorisationId");
        URLs =response.then().statusCode(200).
                header("Aspsp-Sca-Approach", equalTo("EMBEDDED")).
                body("scaStatus", equalTo("received")).
//                body("authorisationId", notNullValue()).
//                body("_links.updatePsuAuthentication", equalTo(basePath + "/" + consent + "/" + consentId + "/authorisations/"+ authorisationId)).
                extract().path("_links");
        providePSUCredentialsURL = URLs.get("updatePsuAuthentication").split("/v1")[1];
        authorisationId = providePSUCredentialsURL.split("authorisations/")[1];
    }


    @Features("4A. Consent Authorization Service")
    @Stories({"1. Positive scenario"})
    @TestCaseId("Consents Initiation Service - Provide PSU credentials")
    @Test(dependsOnMethods = "startAuthentication")
    public void sendPassword() {
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/authorization/passwordInit2.json")).
                put(providePSUCredentialsURL).
        then().statusCode(200).
                        header("Aspsp-Sca-Approach", equalTo("EMBEDDED")).
                        body("scaStatus", equalTo("psuAuthenticated")).
                        body("scaMethods ", notNullValue()).
                        body("_links.selectAuthenticationMethod", equalTo(basePath + "/" + consent + "/" + consentId + "/authorisations/"+ authorisationId)).
                        extract().path("_links");
        chooseSCAMethodURL = URLs.get("selectAuthenticationMethod").split("/v1")[1];
    }

    @Features("4A. Consent Authorization Service")
    @Stories({"1. Positive scenario"})
    @TestCaseId("Consents Initiation Service - Get authorization ID")
    @Test(dependsOnMethods = "sendPassword")
    public void getAuthorizationID() {
        Response response = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                get(  "/" +consent+"/" + consentId + "/authorisations"+"/").
                then().statusCode(200).extract().response();
        response.path("authorisationIds").equals(consentId);
    }

    @Features("4A. Consent Authorization Service")
    @Stories({"1. Positive scenario"})
    @TestCaseId("Consents Initiation Service - Get SCA status")
    @Test(dependsOnMethods = "getAuthorizationID")
    public void getScaStatus() {
        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                get("/" + consent + "/" + consentId + "/authorisations"+"/" + authorisationId).
                then().statusCode(200).
                body("scaStatus", equalTo("psuAuthenticated"));
    }

    @Features("4A. Consent Authorization Service")
    @Stories({"1. Positive scenario"})
    @TestCaseId("Consents Initiation Service - Provide SCA method")
    @Test(dependsOnMethods = "getScaStatus")
    public void provideSCAMethod() {
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/authorization/authenticationMethodInit.json")).
                put(chooseSCAMethodURL).
//                put(chooseSCAMethodURL).
                then().statusCode(200).
                header("Aspsp-Sca-Approach", equalTo("EMBEDDED")).
                body("scaStatus", equalTo("scaMethodSelected")).
                body("chosenScaMethod", notNullValue()).
//                body("challengeData", notNullValue()).
                body("_links.authoriseTransaction", equalTo(basePath + "/" + consent + "/" + consentId + "/authorisations/"+ authorisationId)).
                extract().path("_links");
        authoriseTransactionURL = URLs.get("authoriseTransaction").split("/v1")[1];
    }

    @Features("4A. Consent Authorization Service")
    @Stories({"1. Positive scenario"})
    @TestCaseId("Consents Initiation Service - Provide challenge")
    @Test(dependsOnMethods = "provideSCAMethod")
    public void provideChallenge() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat date = new SimpleDateFormat("ddmm");
        URLs = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).contentType("application/json").
                body("{  \"scaAuthenticationData\": \""+date.format(cal.getTime())+"\" } ").
                put(authoriseTransactionURL).
                then().statusCode(200).
                header("Aspsp-Sca-Approach", equalTo("EMBEDDED")).
                body("scaStatus", equalTo("finalised")).
                body("_links.scaStatus", equalTo(basePath + "/" + consent + "/" + consentId + "/authorisations/"+ authorisationId)).
                extract().path("_links");
    }

}
