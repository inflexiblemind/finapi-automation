package core.ReportingService;

import core.base.TestBase;
import core.helpers.Actions;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import static core.helpers.Actions.getValidConsent;
import static core.helpers.Actions.getValidPayment;
import static core.helpers.Actions.uuid;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@Listeners({TestExecutor.class})
public class PaymentReportingTest extends TestBase {

    private String uuid = uuid();
    private String PSUId = "aspsp2";
    private String basePath;
    private String paymentIdAccountDetails;
    private Response response;

    @BeforeClass
    public void setup() {
        paymentIdAccountDetails = getValidPayment(PSUId, "src/main/resources/payment/paymentInitiate.json","src/main/resources/authorization/passwordInit2.json");

    }

    @BeforeMethod
    public void setPath(){
        this.basePath = "/v1/reports/payment-statistic";
        RestAssured.basePath = this.basePath;
    }


    /*************************** Not Implemented *************************/


//    @Features("5B. Reporting Service - Payment Reporting")
//    @Stories({"1. Get Payment Statistic"})
//    @TestCaseId("Reporting Service - Get Payment Statistic Test by Sepa-credit-transfer")
//    @Test()
//    public void getPaymentStatisticTestByProductSepa() {
//        given().header("X-Request-ID", uuid).queryParam("product","sepa-credit-transfers").
//                when().get().
//                then().statusCode(200).
//                header("X-Request-ID", equalTo(uuid));
//    }
//
//    @Features("5B. Reporting Service - Payment Reporting")
//    @Stories({"1. Get Payment Statistic"})
//    @TestCaseId("Reporting Service - Get Payment Statistic Test by Instant-sepa-credit-transfer")
//    @Test()
//    public void getPaymentStatisticTestByProductInstantSepa() {
//        given().header("X-Request-ID", uuid).queryParam("product","instant-sepa-credit-transfers").
//                when().get().
//                then().statusCode(200).
//                header("X-Request-ID", equalTo(uuid));
//    }
//
//    @Features("5B. Reporting Service - Payment Reporting")
//    @Stories({"1. Get Payment Statistic"})
//    @TestCaseId("Reporting Service - Get Payment Statistic Test by Target-2-credit-transfer")
//    @Test()
//    public void getPaymentStatisticTestByProductTarget2() {
//        given().header("X-Request-ID", uuid).queryParam("product","target-2-credit-transfers").
//                when().get().
//                then().statusCode(200).
//                header("X-Request-ID", equalTo(uuid));
//    }
//
//    @Features("5B. Reporting Service - Payment Reporting")
//    @Stories({"1. Get Payment Statistic"})
//    @TestCaseId("Reporting Service - Get Payment Statistic Test by Cross-border-credit-transfer")
//    @Test()
//    public void getPaymentStatisticTestByProductCrossBorder() {
//        given().header("X-Request-ID", uuid).queryParam("product","cross-border-credit-transfers").
//                when().get().
//                then().statusCode(200).
//                header("X-Request-ID", equalTo(uuid));
//    }
//
//    @Features("5B. Reporting Service - Payment Reporting")
//    @Stories({"1. Get Payment Statistic"})
//    @TestCaseId("Reporting Service - Get Payment Statistic Test by Single type")
//    @Test()
//    public void getPaymentStatisticTestBySingleType() {
//        given().header("X-Request-ID", uuid).queryParam("type","single").
//                when().get().
//                then().statusCode(200).
//                header("X-Request-ID", equalTo(uuid));
//    }
//
//    @Features("5B. Reporting Service - Payment Reporting")
//    @Stories({"1. Get Payment Statistic"})
//    @TestCaseId("Reporting Service - Get Payment Statistic Test  by Bulk type")
//    @Test()
//    public void getPaymentStatisticTestByBulkType() {
//        given().header("X-Request-ID", uuid).queryParam("type","bulk").
//                when().get().
//                then().statusCode(200).
//                header("X-Request-ID", equalTo(uuid));
//    }
//
//    @Features("5B. Reporting Service - Payment Reporting")
//    @Stories({"1. Get Payment Statistic"})
//    @TestCaseId("Reporting Service - Get Payment Statistic Test by Reccurent type")
//    @Test()
//    public void getPaymentStatisticTestByReccurentType() {
//        given().header("X-Request-ID", uuid).queryParam("type","recurrent").
//                when().get().
//                then().statusCode(200).
//                header("X-Request-ID", equalTo(uuid));
//    }
//
//    @Features("5B. Reporting Service - Payment Reporting")
//    @Stories({"1. Get Payment Statistic"})
//    @TestCaseId("Reporting Service - Get Payment Statistic Test by TPP-ID")
//    @Test()
//    public void getPaymentStatisticTestByTppId() {
//        given().header("X-Request-ID", uuid).queryParam("tppId",paymentIdAccountDetails).
//                when().get().
//                then().statusCode(200).
//                header("X-Request-ID", equalTo(uuid));
//    }
//
//    @Features("5B. Reporting Service - Payment Reporting")
//    @Stories({"1. Get Payment Statistic"})
//    @TestCaseId("Reporting Service - Get Payment Statistic Test by PSU-ID")
//    @Test()
//    public void getPaymentStatisticTestByPsuId() {
//        given().header("X-Request-ID", uuid).queryParam("psuId",PSUId).
//                when().get().
//                then().statusCode(200).
//                header("X-Request-ID", equalTo(uuid));
//    }
//    @Features("5B. Reporting Service - Payment Reporting")
//    @Stories({"1. Get Payment Statistic"})
//    @TestCaseId("Reporting Service - Get Payment Statistic Test by PSU-ID")
//    @Test()
//    public void getPaymentStatisticTestByPaymentId() {
//        given().header("X-Request-ID", uuid).queryParam("id",paymentIdAccountDetails).
//                when().get().
//                then().statusCode(200).
//                header("X-Request-ID", equalTo(uuid));
//    }
//    @Features("5B. Reporting Service - Payment Reporting")
//    @Stories({"1. Get Payment Statistic"})
//    @TestCaseId("Reporting Service - Get Payment Statistic Test by PSU-ID")
//    @Test()
//    public void getPaymentStatisticTestByRCVDStatus() {
//        given().header("X-Request-ID", uuid).queryParam("transactionStatus","RCVD").
//                when().get().
//                then().statusCode(200).
//                header("X-Request-ID", equalTo(uuid));
//    }
//
//    @Features("5B. Reporting Service - Payment Reporting")
//    @Stories({"1. Get Payment Statistic"})
//    @TestCaseId("Reporting Service - Get Payment Statistic Test by PSU-ID")
//    @Test()
//    public void getPaymentStatisticTestByACTCStatus() {
//        given().header("X-Request-ID", uuid).queryParam("transactionStatus","ACTC").
//                when().get().
//                then().statusCode(200).
//                header("X-Request-ID", equalTo(uuid));
//    }
//
//    @Features("5B. Reporting Service - Payment Reporting")
//    @Stories({"1. Get Payment Statistic"})
//    @TestCaseId("Reporting Service - Get Payment Statistic Test by PSU-ID")
//    @Test()
//    public void getPaymentStatisticTestByCANCStatus() {
//        given().header("X-Request-ID", uuid).queryParam("transactionStatus","CANC").
//                when().get().
//                then().statusCode(200).
//                header("X-Request-ID", equalTo(uuid));
//    }



}
