package core.ReportingService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.util.Arrays;

import static core.helpers.Actions.uuid;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;

@Listeners({TestExecutor.class})
public class ReportingNegativeTest extends TestBase {

    private String basePath;
    private String uuid = uuid();
    private String dateFrom;
    private String dateTo;

    @BeforeClass
    public void setup() {

    }

    @BeforeMethod
    public void setPath(){
        basePath = "/v1/reports";
        RestAssured.basePath = basePath;
    }


    /**
     * tests for Consent Reporting
     */



    /**
     * tests for Payment Reporting
     */



    /**
     * tests for Request Reporting
     */



    @Features("5E. Reporting Service - Negative")
    @Stories({"3. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic off period missing mandatory header Negative Test")
    @Test()
    public void getRequestStatisticOffPeriodMissingHeaderTest() {

        dateFrom = "2018-12-21";
        dateTo = "2019-01-31";

        given().
                queryParams("dateFrom",dateFrom,"dateTo",dateTo).
                when().get().
                then().statusCode(400).
                body("tppMessages.category", equalTo("ERROR")).
                body("tppMessages.code", equalTo("FORMAT_ERROR")).
                body("tppMessages.text", equalTo("Mandatory header 'X-Request-ID' is missing or empty."));
    }

    @Features("5E. Reporting Service - Negative")
    @Stories({"3. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic off period unsupported HTTP method Negative Test")
    @Test()
    public void getRequestStatisticOffPeriodUnsupportedHTTPMethodTest() {

        dateFrom = "2018-12-21";
        dateTo = "2019-01-31";

        given().header("X-Request-ID", uuid()).
                queryParams("dateFrom",dateFrom,"dateTo",dateTo).
                when().post().
                then().statusCode(405).
                body("tppMessages.category", equalTo("ERROR")).
                body("tppMessages.code", equalTo("FORMAT_ERROR")).
                body("tppMessages.text", equalTo("Method POST is not supported in accounts service."));
    }

    @Features("5E. Reporting Service - Negative")
    @Stories({"3. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic off period missing mandatory query parameter dateFrom Negative Test")
    @Test()
    public void getRequestStatisticOffPeriodWithoutDateFromTest() {

        dateTo = "2019-01-31";

        given().header("X-Request-ID", uuid()).
                queryParam("dateTo",dateTo).
                when().get().
                then().statusCode(400).
                body("tppMessages.category", equalTo("ERROR")).
                body("tppMessages.code", equalTo("FORMAT_ERROR")).
                body("tppMessages.text", equalTo("Mandatory query parameter 'dateFrom' is missing or empty."));
    }

    @Features("5E. Reporting Service - Negative")
    @Stories({"3. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic off period missing mandatory query parameter dateTo Negative Test")
    @Test()
    public void getRequestStatisticOffPeriodWithoutDateToTest() {

        dateFrom = "2018-12-21";

        given().header("X-Request-ID", uuid()).
                queryParam("dateFrom",dateFrom).
                when().get().
                then().statusCode(400).
                body("tppMessages.category", equalTo("ERROR")).
                body("tppMessages.code", equalTo("FORMAT_ERROR")).
                body("tppMessages.text", equalTo("Mandatory query parameter 'dateTo' is missing or empty."));
    }

    @Features("5E. Reporting Service - Negative")
    @Stories({"3. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic off period incorrect date format Negative Test")
    @Test()
    public void getRequestStatisticOffPeriodIncorrectDateFormatTest() {

        dateFrom = "20-12-2018";
        dateTo = "2019-01-31";

        given().header("X-Request-ID", uuid()).
                queryParams("dateFrom",dateFrom,"dateTo",dateTo).
                when().get().
                then().statusCode(400).
                body("tppMessages.category", equalTo("ERROR")).
                body("tppMessages.code", equalTo("FORMAT_ERROR")).
                body("tppMessages.text", equalTo("Incorrect date format"));
    }

    @Features("5E. Reporting Service - Negative")
    @Stories({"3. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic off period non-existing tppId Negative Test")
    @Test()
    public void getRequestStatisticOffPeriodNonExistingTppIdTest() {

        dateFrom = "2018-12-21";
        dateTo = "2019-01-31";

        given().header("X-Request-ID", uuid()).
                queryParams("dateFrom",dateFrom,"dateTo",dateTo).
                queryParam("tppId",uuid()).
                when().get().
                then().statusCode(400).
                body("tppMessages.category", equalTo("ERROR")).
                body("tppMessages.code", equalTo("FORMAT_ERROR")).
                body("tppMessages.text", equalTo("Unknown tppId"));
    }

    @Features("5E. Reporting Service - Negative")
    @Stories({"3. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic off period dateFrom after dateTo Negative Test")
    @Test()
    public void getRequestStatisticDateFromAfterDateToTest() {

        dateFrom = "2019-02-01";
        dateTo = "2019-01-31";

        given().header("X-Request-ID", uuid()).
                queryParams("dateFrom",dateFrom,"dateTo",dateTo).
                when().get().
                then().statusCode(400).
                body("tppMessages.category", equalTo("ERROR")).
                body("tppMessages.code", equalTo("FORMAT_ERROR")).
                body("tppMessages.text", equalTo("Incorrect query parameter 'dateFrom'."));
    }

    @Features("5E. Reporting Service - Negative")
    @Stories({"3. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic off period dateTo after current date Negative Test")
    @Test()
    public void getRequestStatisticDateToAfterCurrentDateTest() {

        dateFrom = "2018-12-21";
        dateTo = "2049-01-31";

        given().header("X-Request-ID", uuid()).
                queryParams("dateFrom",dateFrom,"dateTo",dateTo).
                when().get().
                then().statusCode(400).
                body("tppMessages.category", equalTo("ERROR")).
                body("tppMessages.code", equalTo("FORMAT_ERROR")).
                body("tppMessages.text", equalTo("Incorrect query parameter 'dateTo'."));
    }


    /**
     * tests for TPP-info Reporting
     */



    @Features("5E. Reporting Service - Negative")
    @Stories({"4. Get tpp-info statistic"})
    @TestCaseId("Reporting Service - Get tpp-info Statistic missing mandatory header Negative Test")
    @Test()
    public void getTppInfoStatisticMissingHeaderNegativeTest() {
        given().
                when().get().
                then().statusCode(400).
                body("tppMessages.category", equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code", equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Mandatory header 'X-Request-ID' is missing or empty.")));
    }

    @Features("5E. Reporting Service - Negative")
    @Stories({"4. Get tpp-info statistic"})
    @TestCaseId("Reporting Service - Get tpp-info Statistic use unsupported HTTP method Negative Test")
    @Test()
    public void getTppInfoStatisticUnsupportedMethodNegativeTest() {
        given().header("X-Request-ID", uuid()).
                when().post().
                then().statusCode(405).
                body("tppMessages.category", equalTo("ERROR")).
                body("tppMessages.code", equalTo("SERVICE_INVALID")).
                body("tppMessages.text", equalTo("Method POST is not supported in accounts service."));
    }

    @Features("5E. Reporting Service - Negative")
    @Stories({"4. Get tpp-info statistic"})
    @TestCaseId("Reporting Service - Get tpp-info Statistic with non-existing tppId Negative Test")
    @Test()
    public void getTppInfoStatisticNonExistingTppIdNegativeTest() {
        given().header("X-Request-ID", uuid()).
                queryParam("tppId",uuid()).
                when().get().
                then().statusCode(400).
                body("tppMessages.category", equalTo("ERROR")).
                body("tppMessages.code", equalTo("FORMAT_ERROR")).
                body("tppMessages.text", equalTo("Mandatory header 'X-Request-ID' is missing or empty."));
    }

    @Features("5E. Reporting Service - Negative")
    @Stories({"4. Get tpp-info statistic"})
    @TestCaseId("Reporting Service - Get tpp-info Statistic with non-existing role Negative Test")
    @Test()
    public void getTppInfoStatisticNonExistingRoleNegativeTest() {
        given().header("X-Request-ID", uuid()).
                queryParam("role",uuid()).
                when().get().
                then().statusCode(400).
                body("tppMessages.category", equalTo("ERROR")).
                body("tppMessages.code", equalTo("FORMAT_ERROR")).
                body("tppMessages.text", equalTo("Mandatory header 'X-Request-ID' is missing or empty."));
    }

}
