package core.ReportingService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.hamcrest.Matchers;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static core.helpers.Actions.date;
import static core.helpers.Actions.getValidConsent;
import static core.helpers.Actions.uuid;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

@Listeners({TestExecutor.class})
public class ConsentReportingTest extends TestBase {

    private String uuid = uuid();
    private String PSUId = "aspsp2";
    private String basePath;
    private Response response;
    private String consentIdAccountDetails;
    private Pattern datePattern = Pattern.compile(
            "^\\d{4}-\\d{2}-\\d{2}$");

    @BeforeClass
    public void setup() {

        consentIdAccountDetails = getValidConsent(PSUId, "src/main/resources/consent/detailedConsent/createConsentAccountDetailsAccessOnly.json",
                "src/main/resources/authorization/passwordInit2.json");

    }

    @BeforeMethod
    public void setPath(){
        this.basePath = "/v1/reports/consent-statistic";
        RestAssured.basePath = this.basePath;
    }

    @Features("5A. Reporting Service - Consent Reporting")
    @Stories({"1. Get Consent Statistic"})
    @TestCaseId("Reporting Service - Get Consent Statistic Test")
    @Test()
    public void getConsentStatisticTest() {
        response =given().header("X-Request-ID", uuid).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
        extract().response();
        Pattern p = Pattern.compile("[0-9]+");
        ArrayList<String> consentStatus = response.then().extract().path("consentStatus");
        ArrayList<Integer> frequencyPerDay = response.then().extract().path("frequencyPerDay");
        ArrayList<String> id = response.then().extract().path("id");
        ArrayList<String> lastActionDate = response.then().extract().path("lastActionDate");
        lastActionDate.removeAll(Arrays.asList("", null));
        ArrayList<String> psuId = response.then().extract().path("psuId");
        ArrayList<Boolean> recurringIndicator = response.then().extract().path("recurringIndicator");
        ArrayList<String> tppId = response.then().extract().path("tppId");
        ArrayList<Boolean> tppRedirectPreferred = response.then().extract().path("tppRedirectPreferred");
        ArrayList<String> validUntil = response.then().extract().path("validUntil");
        ArrayList<Boolean> withBalance = response.then().extract().path("withBalance");
        ArrayList<HashMap<String,String>> access = response.then().extract().path("access");
        int i = 1;
        for (HashMap<String,String> a : access){
            Assert.assertTrue(a.containsKey("balances"), i+"-th element doesn't contain Balances field in Access");
            Assert.assertTrue(a.containsKey("accounts"), i+"-th element doesn't contain Accounts field in Access");
            Assert.assertTrue(a.containsKey("transactions"), i+"-th element doesn't contain Transactions field in Access");
            Assert.assertTrue(a.containsKey("availableAccounts"), i+"-th element doesn't contain AvailableAccounts field in Access");
            Assert.assertTrue(a.containsKey("allPsd2"), i+"-th element doesn't contain AllPsd2 field in Access");
            Assert.assertTrue(a.containsKey("psuId"), i+"-th element doesn't contain PSUId field in Access");
            i++;
        }
        for (int j=0; j<i-1; j++){
            Assert.assertTrue(consentStatus.get(j).equals("VALID")||consentStatus.get(j).equals("RECEIVED")||consentStatus.get(j).equals("TERMINATED_BY_TPP")
                            ||consentStatus.get(j).equals("REJECTED")||consentStatus.get(j).equals("EXPIRED"),
                    i+"-th element doesn't contain valid consent status");
            Assert.assertTrue(frequencyPerDay.get(j)>=0, j+"-th element doesn't contain valid value in frequencyPerDay field");
            Assert.assertTrue(id.get(j).contains("-"), j+"-th element doesn't contain valid value in id field");
            Assert.assertTrue(psuId.get(j).contains("aspsp"), j+"-th element doesn't contain valid value in psuId field");
            Assert.assertTrue(recurringIndicator.get(j).equals(true)||recurringIndicator.get(j).equals(false), j+"-th element doesn't contain valid value in recurringIndicator field");
            Matcher m = p.matcher(tppId.get(j));
            Assert.assertTrue(m.matches(), j+"-th element doesn't contain valid value in tppId field");
            Assert.assertTrue(tppRedirectPreferred.get(j).equals(true)||tppRedirectPreferred.get(j).equals(false), j+"-th element doesn't contain valid value in tppRedirectPreferred field");
            Assert.assertTrue(datePattern.matcher(validUntil.get(j)).matches(), j+"-th element doesn't contain valid value in validUntil field");
            Assert.assertTrue(withBalance.get(j).equals(true)||withBalance.get(j).equals(false), j+"-th element doesn't contain valid value in withBalance field");
        }
        for (String date:lastActionDate){
            Assert.assertTrue(datePattern.matcher(date).matches());
        }
    }

    @Features("5A. Reporting Service - Consent Reporting")
    @Stories({"1. Get Consent Statistic"})
    @TestCaseId("Reporting Service - Get Consent Statistic by consent ID Test")
    @Test()
    public void getConsentStatisticByConsentIDTest() {
        response =given().header("X-Request-ID", uuid).queryParam("consentId",consentIdAccountDetails).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                extract().response();
        ArrayList<String> id = response.then().extract().path("id");
        int i = 1;
        for (String consentId:id){
            Assert.assertEquals(consentId, consentIdAccountDetails, i+"-th element doesn't contain valid value in id field");

        }

    }

    @Features("5A. Reporting Service - Consent Reporting")
    @Stories({"1. Get Consent Statistic"})
    @TestCaseId("Reporting Service - Get Consent Statistic by consent status TERMINATED_BY_TPP Test")
    @Test()
    public void getConsentStatisticByConsentStatusTerminateTest() {
        response =given().header("X-Request-ID", uuid).queryParam("consentStatus","terminatedByTpp").
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                extract().response();
        ArrayList<String> consentStatus = response.then().extract().path("consentStatus");
        int i = 1;
        for (String status:consentStatus){
            Assert.assertEquals(status, "TERMINATED_BY_TPP",
                    i+"-th element doesn't contain valid consent status");
        }

    }


    @Features("5A. Reporting Service - Consent Reporting")
    @Stories({"1. Get Consent Statistic"})
    @TestCaseId("Reporting Service - Get Consent Statistic by consent status RevokedByPsu Test")
    @Test()
    public void getConsentStatisticByConsentStatusRevokedByPsuTest() {
        response =given().header("X-Request-ID", uuid).queryParam("consentStatus","revokedByPsu").
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                extract().response();
        ArrayList<String> consentStatus = response.then().extract().path("consentStatus");
        int i = 1;
        for (String status:consentStatus){
            Assert.assertEquals(status, "RevokedByPsu",
                    i+"-th element doesn't contain valid consent status");
        }

    }

    @Features("5A. Reporting Service - Consent Reporting")
    @Stories({"1. Get Consent Statistic"})
    @TestCaseId("Reporting Service - Get Consent Statistic by consent status Expired Test")
    @Test()
    public void getConsentStatisticByConsentStatusExpiredTest() {
        response = given().header("X-Request-ID", uuid).queryParam("consentStatus", "expired").
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
        extract().response();
        ArrayList<String> consentStatus = response.then().extract().path("consentStatus");
        int i=1;
        for (String status:consentStatus) {
            Assert.assertEquals(status, "EXPIRED",
                    i + "-th element doesn't contain valid consent status");
            i++;
        }
    }

    @Features("5A. Reporting Service - Consent Reporting")
    @Stories({"1. Get Consent Statistic"})
    @TestCaseId("Reporting Service - Get Consent Statistic by consent status Valid Test")
    @Test()
    public void getConsentStatisticByConsentStatusValidTest() {
        response = given().header("X-Request-ID", uuid).queryParam("consentStatus","valid").
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                extract().response();
        ArrayList<String> consentStatus = response.then().extract().path("consentStatus");
        int i=1;
        for (String status:consentStatus) {
            Assert.assertEquals(status, "VALID",
                    i + "-th element doesn't contain valid consent status");
            i++;
        }
    }

    @Features("5A. Reporting Service - Consent Reporting")
    @Stories({"1. Get Consent Statistic"})
    @TestCaseId("Reporting Service - Get Consent Statistic by consent status Rejected Test")
    @Test()
    public void getConsentStatisticByConsentStatusRejectedTest() {
        response = given().header("X-Request-ID", uuid).queryParam("consentStatus","rejected").
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                extract().response();
        ArrayList<String> consentStatus = response.then().extract().path("consentStatus");
        int i=1;
        for (String status:consentStatus) {
            Assert.assertEquals(status, "REJECTED",
                    i + "-th element doesn't contain valid consent status");
            i++;
        }
    }

    @Features("5A. Reporting Service - Consent Reporting")
    @Stories({"1. Get Consent Statistic"})
    @TestCaseId("Reporting Service - Get Consent Statistic by consent status Received Test")
    @Test()
    public void getConsentStatisticByConsentStatusReceivedTest() {
        response = given().header("X-Request-ID", uuid).queryParam("consentStatus","received").
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                extract().response();
        ArrayList<String> consentStatus = response.then().extract().path("consentStatus");
        int i=1;
        for (String status:consentStatus) {
            Assert.assertEquals(status, "RECEIVED",
                    i + "-th element doesn't contain valid consent status");
            i++;
        }
    }

    @Features("5A. Reporting Service - Consent Reporting")
    @Stories({"1. Get Consent Statistic"})
    @TestCaseId("Reporting Service - Get Consent Statistic, consent expired today Test")
    @Test()
    public void getConsentStatisticExpiredTodayTest() {
        response = given().header("X-Request-ID", uuid).queryParam("expireToday","true").
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
        extract().response();
        ArrayList<String> validUntil = response.then().extract().path("validUntil");
        int i=1;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        String currentDate= formatter.format(cal.getTime());
        for (String date:validUntil) {
            Assert.assertEquals(date, currentDate,
                    i + "-th element doesn't contain valid validUntil date");
            i++;
        }    }

    @Features("5A. Reporting Service - Consent Reporting")
    @Stories({"1. Get Consent Statistic"})
    @TestCaseId("Reporting Service - Get Consent Statistic, consent not expired today Test")
    @Test()
    public void getConsentStatisticNotExpiredTodayTest() {
        response = given().header("X-Request-ID", uuid).queryParam("expireToday","false").
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
        extract().response();
        ArrayList<String> validUntil = response.then().extract().path("validUntil");
        int i=1;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        Calendar cal = Calendar.getInstance();
        String currentDate= formatter.format(cal.getTime());
        for (String date:validUntil) {
            Assert.assertTrue(!date.equals(currentDate),
                    i + "-th element doesn't contain valid validUntil date");
            i++;
        }    }

    @Features("5A. Reporting Service - Consent Reporting")
    @Stories({"1. Get Consent Statistic"})
    @TestCaseId("Reporting Service - Get Consent Statistic by psuId Test")
    @Test()
    public void getConsentStatisticByPSUIDTest() {
        response = given().header("X-Request-ID", uuid).queryParam("psuId","aspsp2").
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
        extract().response();
        ArrayList<String> psuId = response.then().extract().path("psuId");
        int i=1;
        for (String id:psuId) {
            Assert.assertEquals(id, "aspsp2",
                    i + "-th element doesn't contain valid psuId");
            i++;
        }    }

    @Features("5A. Reporting Service - Consent Reporting")
    @Stories({"1. Get Consent Statistic"})
    @TestCaseId("Reporting Service - Get Consent Statistic by tppId Test")
    @Test()
    public void getConsentStatisticByTPPIDTest() {
        response = given().header("X-Request-ID", uuid).queryParam("tppId","12345").
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
        extract().response();
        ArrayList<String> tppId = response.then().extract().path("tppId");
        int i=1;
        for (String id:tppId) {
            Assert.assertEquals(id, "12345",
                    i + "-th element doesn't contain valid tppId");
            i++;
        }    }
}
