package core.ReportingService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;

import static core.helpers.Actions.uuid;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.equalTo;

@Listeners({TestExecutor.class})
public class TPPInfoReportingTest extends TestBase {

    private String uuid = uuid();
    private String PSUId = "aspsp2";
    private String basePath;
    private String tppId = "12345";
    private String tppName = "www.example.tpp.com";
    private Response response;

    @BeforeClass
    public void setup() {

    }

    @BeforeMethod
    public void setPath(){
        this.basePath = "/v1/reports/tpp-info";
        RestAssured.basePath = this.basePath;
    }



    @Features("5D. Reporting Service - tpp-info Reporting")
    @Stories({"1. Get tpp-info statistic"})
    @TestCaseId("Reporting Service - Get tpp-info Statistic Test")
    @Test()
    public void tppInfoStatisticTest() {
        given().header("X-Request-ID", uuid).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid));
    }

    @Features("5D. Reporting Service - tpp-info Reporting")
    @Stories({"1. Get tpp-info statistic"})
    @TestCaseId("Reporting Service - Get tpp-info Statistic by tppId Test")
    @Test()
    public void tppInfoStatisticByTppIdTest() {
        given().header("X-Request-ID", uuid).
                queryParam("tppId",tppId).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid));
    }

    @Features("5D. Reporting Service - tpp-info Reporting")
    @Stories({"1. Get tpp-info statistic"})
    @TestCaseId("Reporting Service - Get tpp-info Statistic by tppName Test")
    @Test()
    public void getTppInfoStatisticByTppNameTest() {
        given().header("X-Request-ID", uuid).
                queryParam("tppName",tppName).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid));
    }

    @Features("5D. Reporting Service - tpp-info Reporting")
    @Stories({"1. Get tpp-info statistic"})
    @TestCaseId("Reporting Service - Get tpp-info Statistic by PSP_AS role Test")
    @Test()
    public void tppInfoStatisticByPSP_ASRoleTest() {
        given().header("X-Request-ID", uuid).
                queryParam("role","PSP_AS").
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid));
    }

    @Features("5D. Reporting Service - tpp-info Reporting")
    @Stories({"1. Get tpp-info statistic"})
    @TestCaseId("Reporting Service - Get tpp-info Statistic by PSP_PI role Test")
    @Test()
    public void tppInfoStatisticByPSP_PIRoleTest() {
        given().header("X-Request-ID", uuid).
                queryParam("role","PSP_PI").
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid));
    }

    @Features("5D. Reporting Service - tpp-info Reporting")
    @Stories({"1. Get tpp-info statistic"})
    @TestCaseId("Reporting Service - Get tpp-info Statistic by PSP_AI role Test")
    @Test()
    public void tppInfoStatisticByPSP_AIRoleTest() {
        given().header("X-Request-ID", uuid).
                queryParam("role","PSP_AI").
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid));
    }

    @Features("5D. Reporting Service - tpp-info Reporting")
    @Stories({"1. Get tpp-info statistic"})
    @TestCaseId("Reporting Service - Get tpp-info Statistic by PSP_IC role Test")
    @Test()
    public void tppInfoStatisticByPSP_ICRoleTest() {
        given().header("X-Request-ID", uuid).
                queryParam("role","PSP_IC").
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid));
    }

    @Features("5D. Reporting Service - tpp-info Reporting")
    @Stories({"1. Get tpp-info statistic"})
    @TestCaseId("Reporting Service - Get tpp-info Statistic Schema Conform Test")
    @Test()
    public void tppInfoStatisticSchemaConformTest() {
        given().header("X-Request-ID", uuid).
                when().get().
                then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/reporting/tppInfo.json")));
    }

}
