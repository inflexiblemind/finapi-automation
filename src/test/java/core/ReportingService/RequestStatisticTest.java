package core.ReportingService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.lang.reflect.Field;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import static core.helpers.Actions.*;
import static io.restassured.RestAssured.given;
import static io.restassured.RestAssured.withArgs;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.*;

@Listeners({TestExecutor.class})
public class RequestStatisticTest extends TestBase {

    private String uuid = uuid();
    private String PSUId = "aspsp2";
    private String basePath;
    private Response response;
    private String dateFrom = "2018-12-21";
    private String dateTo = "2019-02-19";
    private String tppId = "12345";
    private String dateToday;
    private int timeOut = 3500;
    private static String consentIdAccountDetails;


    @BeforeClass
    public void setup() {
        consentIdAccountDetails = getValidConsent(PSUId, "src/main/resources/consent/detailedConsent/createConsentAccountDetailsAccessOnly.json","src/main/resources/authorization/passwordInit2.json");
        this.basePath = "/v1/reports/request-statistic";
        RestAssured.basePath = this.basePath;
    }

    @BeforeMethod
    public void setPath(){
        setup();
    }



    @Features("5C. Reporting Service - Request Reporting")
    @Stories({"1. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic for current day Test")
    @Test()
    public void requestStatisticReportForCurrentDayTest() {

        DateFormat current = new SimpleDateFormat("yyyy-MM-dd");
        dateToday = current.format(new Date());

        given().header("X-Request-ID", uuid).
                queryParams("dateFrom", dateToday,"dateTo", dateToday).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("accounts.error",notNullValue()).
                body("accounts.success",notNullValue()).
                body("accounts.averageResponseTime",notNullValue()).

                body("payments.error",notNullValue()).
                body("payments.success",notNullValue()).
                body("payments.averageResponseTime",notNullValue()).

                body("consents.error",notNullValue()).
                body("consents.success",notNullValue()).
                body("consents.averageResponseTime",notNullValue());
    }

    @Features("5C. Reporting Service - Request Reporting")
    @Stories({"1. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic for period Test")
    @Test()
    public void requestStatisticReportForPeriodTest() {

        DateFormat current = new SimpleDateFormat("yyyy-MM-dd");
        dateToday = current.format(new Date());

        given().header("X-Request-ID", uuid).
                queryParams("dateFrom",dateFrom,"dateTo",dateToday).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("accounts.error",notNullValue()).
                body("accounts.success",notNullValue()).
                body("accounts.averageResponseTime",notNullValue()).

                body("payments.error",notNullValue()).
                body("payments.success",notNullValue()).
                body("payments.averageResponseTime",notNullValue()).

                body("consents.error",notNullValue()).
                body("consents.success",notNullValue()).
                body("consents.averageResponseTime",notNullValue());
    }

    @Features("5C. Reporting Service - Request Reporting")
    @Stories({"1. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic by tppId Test")
    @Test()
    public void requestStatisticReportByTppIdTest() {

        given().header("X-Request-ID", uuid).
                queryParams("dateFrom",dateFrom,"dateTo",dateTo).
                queryParam("tppId",tppId).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("accounts.error",notNullValue()).
                body("accounts.success",notNullValue()).
                body("accounts.averageResponseTime",notNullValue()).

                body("payments.error",notNullValue()).
                body("payments.success",notNullValue()).
                body("payments.averageResponseTime",notNullValue()).

                body("consents.error",notNullValue()).
                body("consents.success",notNullValue()).
                body("consents.averageResponseTime",notNullValue());
    }

    @Features("5C. Reporting Service - Request Reporting")
    @Stories({"1. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic for current day Schema conform test Test")
    @Test()
    public void requestStatisticReportSchemaConformTest() {

        DateFormat current = new SimpleDateFormat("yyyy-MM-dd");
        dateToday = current.format(new Date());

        given().header("X-Request-ID", uuid).
                queryParams("dateFrom", dateToday,"dateTo", dateToday).
                when().get().
                then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/reporting/RequestStatistic.json")));
    }

    @Features("5C. Reporting Service - Request Reporting")
    @Stories({"1. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic for current day Account Error test Test")
    @Test()
    public void requestStatisticReportAccountErrorTest() throws InterruptedException{
        DateFormat current = new SimpleDateFormat("yyyy-MM-dd");
        dateToday = current.format(new Date());

        int numberOfErrorRequests = given().header("X-Request-ID", uuid).
                queryParams("dateFrom", "2019-02-01","dateTo", dateToday).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).extract().path("accounts.error");

        given().header("X-Request-ID", uuid()).header("Consent-ID", uuid).
                when().get(HOST+"/v1/accounts").
                then().statusCode(400);

        Thread.sleep(timeOut + 2000);

        given().header("X-Request-ID", uuid).
                queryParams("dateFrom", "2019-02-01","dateTo", dateToday).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("accounts.error",equalTo(numberOfErrorRequests + 1));
    }

    @Features("5C. Reporting Service - Request Reporting")
    @Stories({"1. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic for current day Account Success test Test")
    @Test()
    public void requestStatisticReportAccountSuccessTest() throws InterruptedException{
        DateFormat current = new SimpleDateFormat("yyyy-MM-dd");
        dateToday = current.format(new Date());

        int numberOfSuccessRequests = given().header("X-Request-ID", uuid).
                queryParams("dateFrom", "2019-02-01","dateTo", dateToday).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).extract().path("accounts.success");

        given().header("X-Request-ID", uuid()).header("Consent-ID", consentIdAccountDetails).
                when().get(HOST+"/v1/accounts").
                then().statusCode(200);

        Thread.sleep(timeOut + 1000);

        given().header("X-Request-ID", uuid).
                queryParams("dateFrom", "2019-02-01","dateTo", dateToday).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("accounts.success",equalTo(numberOfSuccessRequests + 1));
    }

    @Features("5C. Reporting Service - Request Reporting")
    @Stories({"1. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic for current day Payment Error test Test")
    @Test()
    public void requestStatisticReportPaymentErrorTest() throws InterruptedException{
        DateFormat current = new SimpleDateFormat("yyyy-MM-dd");
        dateToday = current.format(new Date());

        int numberOfErrorRequests = given().header("X-Request-ID", uuid).
                queryParams("dateFrom", "2019-02-01","dateTo", dateToday).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).extract().path("payments.error");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSUId).
                pathParam("paymentId", uuid).
                when().delete(HOST+"/v1/payments/instant-sepa-credit-transfers" + "/{paymentId}").
                then().statusCode(403);

        Thread.sleep(timeOut + 1500);

        given().header("X-Request-ID", uuid).
                queryParams("dateFrom", "2019-02-01","dateTo", dateToday).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("payments.error",equalTo(numberOfErrorRequests + 1));
    }

    @Features("5C. Reporting Service - Request Reporting")
    @Stories({"1. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic for current day Payment Success test Test")
    @Test()
    public void requestStatisticReportPaymentSuccessTest() throws InterruptedException{
        DateFormat current = new SimpleDateFormat("yyyy-MM-dd");
        dateToday = current.format(new Date());

        int numberOfSuccessfulRequests = given().header("X-Request-ID", uuid).
                queryParams("dateFrom", "2019-02-01","dateTo", dateToday).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).extract().path("payments.success");

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSUId).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/payment/sepa-credit-transfer/paymentInitiateSepaMinValues.json")).
                when().post(HOST+"/v1/payments/sepa-credit-transfers").
                then().statusCode(201);

        Thread.sleep(timeOut + 1500);

        given().header("X-Request-ID", uuid).
                queryParams("dateFrom", "2019-02-01","dateTo", dateToday).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("payments.success",equalTo(numberOfSuccessfulRequests + 1));
    }

    @Features("5C. Reporting Service - Request Reporting")
    @Stories({"1. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic for current day Consent Error test Test")
    @Test()
    public void requestStatisticReportConsentErrorTest() throws InterruptedException {
        DateFormat current = new SimpleDateFormat("yyyy-MM-dd");
        dateToday = current.format(new Date());

        int numberOfErrorRequests = given().header("X-Request-ID", uuid).
                queryParams("dateFrom", "2019-02-01","dateTo", dateToday).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).extract().path("consents.error");

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/availableAccounts/createAvailableAccountsConsentDateInThePast.json")).
                when().post(HOST+"/v1/consents").then().statusCode(400);

        Thread.sleep(timeOut + 1500);

        given().header("X-Request-ID", uuid).
                queryParams("dateFrom", "2019-02-01","dateTo", dateToday).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("consents.error",equalTo(numberOfErrorRequests + 1));
    }

    @Features("5C. Reporting Service - Request Reporting")
    @Stories({"1. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic for current day Consent Success test Test")
    @Test()
    public void requestStatisticReportConsentSuccessTest() throws InterruptedException {
        DateFormat current = new SimpleDateFormat("yyyy-MM-dd");
        dateToday = current.format(new Date());

        int numberOfSuccessfulRequests = given().header("X-Request-ID", uuid).
                queryParams("dateFrom", "2019-02-01","dateTo", dateToday).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).extract().path("consents.success");

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/availableAccounts/createAvailableAccountsConsent.json")).
                when().post(HOST+"/v1/consents").then().statusCode(201);

    Thread.sleep(timeOut + 1500);

        given().header("X-Request-ID", uuid).
                queryParams("dateFrom", "2019-02-01","dateTo", dateToday).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("consents.success",equalTo(numberOfSuccessfulRequests + 1));
    }

    @Features("5C. Reporting Service - Request Reporting")
    @Stories({"1. Get Request Statistic"})
    @TestCaseId("Reporting Service - Get Request Statistic for Account service with a non-authorized Consent Test")
    @Test()
    public void requestStatisticReportForAccountWithNonAuthorizedConsentTest() throws InterruptedException {

        DateFormat current = new SimpleDateFormat("yyyy-MM-dd");
        dateToday = current.format(new Date());

        String consent = getConsent("aspsp2", "src/main/resources/consent/availableAccounts/createAvailableAccountsConsent.json");

        int numberOfErrorRequests = given().header("X-Request-ID", uuid).
                queryParams("dateFrom", "2019-02-01","dateTo", dateToday).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).extract().path("accounts.error");

        given().header("X-Request-ID", uuid()).header("Consent-ID", consent).
                when().get(HOST+"/v1/accounts").
                then().statusCode(401).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("CONSENT_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent " + consent + " is not authorised.")));

        Thread.sleep(timeOut + 1500);

        given().header("X-Request-ID", uuid).
                queryParams("dateFrom", "2019-02-01","dateTo", dateToday).
                when().get().
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("accounts.error",equalTo(numberOfErrorRequests + 1));
    }

}
