package core.ConsentsService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.util.Arrays;

import static core.helpers.Actions.uuid;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;

/**
 * Created by Sergey.Syomin on 1/10/2019.
 */

@Listeners({TestExecutor.class})
public class ConsentsNegativeTest extends TestBase {

    private String uuid = uuid();
    private String PSUId = "aspsp2";
    private String PSUIdNoAccounts = "aspsp7";
    private String basePath;
    private String ERROR_FIELD = "code";
    private Response response;
    private String deletedConsent;

    @BeforeClass
    public void setup() {
        this.basePath = "/v1/consents";
        RestAssured.basePath = this.basePath;

        //get Consent ID which already deleted
        uuid = uuid();
        response = given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentAccountDetailsAccessOnly.json")).
                when().post().
                then().statusCode(201).extract().response();
        deletedConsent = response.then().extract().path("consentId");
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).
                pathParam("consentId", deletedConsent).
                when().delete("{consentId}").
                then().statusCode(204);
    }





    /*****************************************************************
     * Available Accounts Consent
     *****************************************************************/





    @Features("2D. Consents Service - Negative")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Available Accounts Consent Date In The Past Negative Test")
    @Test()
    public void createAvailableAccountsConsentPastDateNegativeTest() {

        given().header("X-Request-ID", uuid()).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/availableAccounts/createAvailableAccountsConsentDateInThePast.json")).
                when().post().
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code", equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Consent validity date 2018-01-01 is in the past.")));


        given().header("X-Request-ID", uuid()).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/availableAccounts/createAvailableAccountsConsentDateInThePast.json")).
                when().post().
                then().statusCode(400).
                body("tppMessages." + "code" + "[0]", equalTo("FORMAT_ERROR")).
                body("tppMessages.text[0]", equalTo("Consent validity date 2018-01-01 is in the past."));
    }

    @Features("2D. Consents Service - Negative")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Available Accounts Consent Without Frequency Per Day Negative Test")
    @Test()
    public void createAvailableAccountsConsentWithoutFrequencyNegativeTest() {

        given().header("X-Request-ID", uuid()).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/availableAccounts/createAvailableAccountsConsentWithoutFrequency.json")).
                when().post().
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code", equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text",equalTo(Arrays.asList("Invalid null value submitted for frequencyPerDay")));
    }






    /********************************************************************
     * Detailed Consent
     ********************************************************************/





    @Features("2D. Consents Service - Negative")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Consent with invalid request syntax Negative Test")
    @Test()
    public void createConsentInvalidRequestSyntaxNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentInvalidRequestSyntax.json")).
                when().post().
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text",equalTo(Arrays.asList("Format of certain request fields are not matching the XS2A requirements.")));
    }

    @Features("2D. Consents Service - Negative")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Consent valid is in the past Negative Test")
    @Test()
    public void createConsentValidInPastNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentValidInPast.json")).
                when().post().
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code",equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent validity date 2018-12-20 is in the past.")));
    }

    @Features("2D. Consents Service - Negative")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Consent With None Existing Iban Negative Test")
    @Test()
    public void createConsentNotExistIbanNegativeTest() {
        uuid = uuid();
//really create consent with not existing Iban, if this test fall it will means what service work is correct, and test need to be updated
        String consNonExistIb = given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentNotExistIban.json")).
                when().post().
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                extract().response().then().extract().path("consentId");

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consNonExistIb).
                when().get("{consentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid));
    }

    @Features("2D. Consents Service - Negative")
    @Stories({"2. Consent Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of is non existent ConsentId Negative Test")
    @Test()
    public void getStatusNonExistentConsentIdNegativeTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                pathParam("consentId", uuid).
                when().get("{consentId}/status").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code", equalTo(Arrays.asList("CONSENT_UNKNOWN"))).
                body("tppMessages.text",equalTo(Arrays.asList("Consent "+uuid+" doesn't exist.")));
    }

    @Features("2D. Consents Service - Negative")
    @Stories({"3. Consent Details Request"})
    @TestCaseId("Consent Details Request - Get Content when ConsentId is non existent Negative Test")
    @Test()
    public void getDetailsConsentIdNonExistentNegativeTest() {

        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                pathParam("consentId", uuid).
                when().get("{consentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code", equalTo(Arrays.asList("CONSENT_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Consent "+uuid+" doesn't exist.")));
    }

    @Features("2D. Consents Service - Negative")
    @Stories({"4. Delete Consent"})
    @TestCaseId("Delete Consent - Revoke Consent when ConsentId is non-existent Negative Test")
    @Test()
    public void deleteConsentIsNonExistedNegativeTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).pathParam("consentId", uuid).
                when().delete("{consentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code", equalTo(Arrays.asList("CONSENT_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Consent "+uuid+" doesn't exist.")));
    }

    @Features("2D. Consents Service - Negative")
    @Stories({"2. Consent Status Request"})
    @TestCaseId("Consent Status Request - Get the Status when ConsentId already deleted Negative Test")
    @Test
    public void getStatusConsentAlreadyDeletedNegativeTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", deletedConsent).
                when().get("{consentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("consentStatus",equalTo("terminatedByTpp"));
    }

    @Features("2D. Consents Service - Negative")
    @Stories({"3. Consent Details Request"})
    @TestCaseId("Consent Details Request - Get Consent Details when the ConsentId already deleted Negative Test")
    @Test
    public void getDetailsConsentAlreadyDeletedNegativeTest() {

        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", deletedConsent).
                when().get("{consentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code", equalTo(Arrays.asList("CONSENT_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Consent "+deletedConsent+" doesn't exist.")));
    }

    @Features("2D. Consents Service - Negative")
    @Stories({"4. Delete Consent"})
    @TestCaseId("Delete Consent - Revoke Consent when ConsentId already deleted Negative Test")
    @Test
    public void deleteConsentAlreadyDeletedNegativeTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).pathParam("consentId", deletedConsent).
                when().delete("{consentId}").
                then().statusCode(403).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code", equalTo(Arrays.asList("CONSENT_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("Consent "+deletedConsent+" doesn't exist.")));
    }

    @Features("2D. Consents Service - Negative")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Consent Account Details Transactions Without Date Negative Test")
    @Test()
    public void createConsentAccountDetailsTransactionsWithoutDateNegativeTest() {

        given().header("X-Request-ID", uuid()).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentAccountDetailsTransactionsWithoutDate.json")).
                when().post().
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code", equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for validUntil")));
    }

    @Features("2D. Consents Service - Negative")
    @Stories("1. Create Consent")
    @TestCaseId("Create Consent - Create Consent UUID is missing Negative Test")
    @Test()
    public void createConsentUUIDIsMissingNegativeTest() {

        given().header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalancesTransactions.json")).
                when().post().
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code", equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Mandatory header 'X-Request-ID' is missing or empty.")));
    }

    @Features("2D. Consents Service - Negative")
    @Stories("1. Create Consent")
    @TestCaseId("Create Consent - Create Consent PSU_ID is missing Negative Test")
    @Test()
    public void createConsentPSUIDIsMissingNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalancesTransactions.json")).
                when().post().
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code", equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Format of certain request fields are not matching the XS2A requirements.")));
    }

    @Features("2D. Consents Service - Negative")
    @Stories("1. Create Consent")
    @TestCaseId("Create Consent - Create Consent with unknown PSU_ID Negative Test")
    @Test()
    public void createConsentWithUnknownPSUIDNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("PSU-ID", "unknown").contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalancesTransactions.json")).
                when().post().
                then().statusCode(401).
                body("tppMessages.category", equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code", equalTo(Arrays.asList("PSU_CREDENTIALS_INVALID"))).
                body("tppMessages.text",equalTo(Arrays.asList("PSU-ID validation failed")));
    }

    @Features("2D. Consents Service - Negative")
    @Stories("1. Create Consent")
    @TestCaseId("Create Consent - Create Consent using PUT Method Negative Test")
    @Test()
    public void createConsentUsingPUTMethodNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalancesTransactions.json")).
                when().put().
                then().statusCode(405).
//                body("tppMessages.category", equalTo(Arrays.asList("ERROR"))).
//                body("tppMessages.code", equalTo(Arrays.asList("SERVICE_INVALID"))).
//                body("tppMessages.text",equalTo(Arrays.asList("Method PUT is not supported in accounts service.")));
                body("error", equalTo("Method Not Allowed")).
                body("message",equalTo("Request method 'PUT' not supported")).
                body("path",equalTo("/v1/consents"));
    }

    @Features("2D. Consents Service - Negative")
    @Stories("1. Create Consent")
    @TestCaseId("Create Consent - Create Consent for account details, balances, transactions access Unsupported field Negative Test")
    @Test()
    public void createConsentAccountDetailsBalancesTransactionsAccessUnsupportedFieldNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalancesTransactionsUnsupportedField.json")).
                when().post().
                then().statusCode(201).
                header("X-Request-ID", equalTo(uuid)).
                body("consentStatus", equalTo("received")).
                body("unsupportedField", nullValue());
    }






    /*******************************************************
     *Global Consent
     ********************************************************/





    @Features("2D. Consents Service - Negative")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Global Consent Date In The Past Negative Test")
    @Test()
    public void createGlobalConsentPastDateNegativeTest() {

        given().header("X-Request-ID", uuid()).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/globalConsent/createGlobalConsentDateInThePast.json")).
                when().post().
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code", equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Consent validity date 2018-01-01 is in the past.")));
    }

    @Features("2D. Consents Service - Negative")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Global Consent Without Frequency Per Day Negative Test")
    @Test()
    public void createGlobalConsentWithoutFrequencyNegativeTest() {

        given().header("X-Request-ID", uuid()).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/globalConsent/createGlobalConsentWithoutFrequency.json")).
                when().post().
                then().statusCode(400).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code", equalTo(Arrays.asList("FORMAT_ERROR"))).
                body("tppMessages.text", equalTo(Arrays.asList("Invalid null value submitted for frequencyPerDay")));
    }

    @Features("2D. Consents Service - Negative")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Available Accounts Consent with PSU which don't have accounts Negative Test")
    @Test()
    public void createAvailableAccountsConsentPSUNoAccountsNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUIdNoAccounts).contentType("application/json").
                body(new File("src/main/resources/consent/availableAccounts/createAvailableAccountsConsent.json")).
                when().post().
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code", equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("There are no available accounts for requested consent.")));
    }

    @Features("2D. Consents Service - Negative")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Global Consent with PSU which don't have accounts Negative Test")
    @Test()
    public void createGlobalAccountsConsentPSUNoAccountsNegativeTest() {
        uuid = uuid();

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUIdNoAccounts).contentType("application/json").
                body(new File("src/main/resources/consent/globalConsent/createGlobalConsent.json")).
                when().post().
                then().statusCode(400).
                header("X-Request-ID", equalTo(uuid)).
                body("tppMessages.category",equalTo(Arrays.asList("ERROR"))).
                body("tppMessages.code", equalTo(Arrays.asList("RESOURCE_UNKNOWN"))).
                body("tppMessages.text", equalTo(Arrays.asList("There are no available accounts for requested consent.")));
    }

}
