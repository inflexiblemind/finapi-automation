package core.ConsentsService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.util.Map;

import static core.helpers.Actions.convertFileToHashMap;
import static core.helpers.Actions.uuid;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

/**
 * Created by Sergey.Syomin on 1/2/2019.
 */

@Listeners({TestExecutor.class})
public class AvailableAccountsConsentTest extends TestBase {

    private String uuid = uuid();
    private String PSUId = "aspsp2";
    private String availableAccountsConsentId;
    private Response response;
    private String basePath;
    private String ERROR_FIELD = "code";

    @BeforeClass
    public void setup() {
        this.basePath = "/v1/consents";
        RestAssured.basePath = this.basePath;
    }

    @BeforeMethod
    public void setPath(){
        setup();
    }

    /**
     * Consent creation
     */

    @Features("2A. Consents Service - Available Accounts Consent")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Available Accounts Consent")
    @Test()
    public void createAvailableAccountsConsentTest() {
        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/availableAccounts/createAvailableAccountsConsent.json")).
                when().post();

        availableAccountsConsentId = response.then().statusCode(201).extract().path("consentId");

        response.then().
                header("X-Request-ID", equalTo(uuid)).
                header("Location", equalTo(basePath + "/" + availableAccountsConsentId)).
                body("consentStatus", equalTo("received")).
                body("consentId", notNullValue());
    }

    /**
     * Consent status
     */

    @Features("2A. Consents Service - Available Accounts Consent")
    @Stories({"2. Consent Status Request"})
    @TestCaseId("Consent Status Request - Get the Status Available Accounts Consent")
    @Test(dependsOnMethods = {"createAvailableAccountsConsentTest"})
    public void getAvailableAccountsConsentStatusTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", availableAccountsConsentId).
                when().get("{consentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("consentStatus", equalTo("received"));
    }

    /**
     * Consent details
     */

    @Features("2A. Consents Service - Available Accounts Consent")
    @Stories({"3. Consent Details Request"})
    @TestCaseId("Consent Details Request - Get Available Accounts Consent Details")
    @Test(dependsOnMethods = {"createAvailableAccountsConsentTest"})
    public void getAvailableAccountsConsentDetailsTest() {

        Map json = convertFileToHashMap("src/main/resources/consent/availableAccounts/createAvailableAccountsConsent.json");
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", availableAccountsConsentId).
                when().get("{consentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("access",equalTo(json.get("access"))).
                body("consentStatus", equalTo("received")).
                body("recurringIndicator",equalTo(json.get("recurringIndicator"))).
                body("frequencyPerDay",equalTo(Integer.valueOf(json.get("frequencyPerDay").toString()))).
                body("validUntil",equalTo(json.get("validUntil")));
    }

    @Features("2A. Consents Service - Available Accounts Consent")
    @Stories({"4. Delete Consent"})
    @TestCaseId("Delete Consent - Revoke Available Accounts Consent")
    @Test(alwaysRun = true, dependsOnMethods = {"createAvailableAccountsConsentTest", "getAvailableAccountsConsentStatusTest", "getAvailableAccountsConsentDetailsTest"})
    public void deleteAvailableAccountsConsentTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).pathParam("consentId", availableAccountsConsentId).
                when().delete("{consentId}").
                then().statusCode(204).
                header("X-Request-ID", equalTo(uuid));

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", availableAccountsConsentId).
                when().get("{consentId}/status").
                then().statusCode(200).
                body("consentStatus", equalTo("terminatedByTpp"));
    }
}
