package core.ConsentsService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.util.List;
import java.util.Map;

import static core.helpers.Actions.convertFileToHashMap;
import static core.helpers.Actions.uuid;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.*;

/**
 * Created by Sergey.Syomin on 1/2/2019.
 */

@Listeners({TestExecutor.class})
public class GlobalConsentTest extends TestBase {

    private String uuid = uuid();
    private String PSUId = "aspsp2";
    private String globalAccountsConsentId;
    private Response response;
    private String basePath;
    private String ERROR_FIELD = "code";

    @BeforeClass
    public void setup() {
        this.basePath = "/v1/consents";
        RestAssured.basePath = this.basePath;
    }

    @BeforeMethod
    public void setPath(){
        setup();
    }

    /**
     * Consent creation
     */

    @Features("2C. Consents Service - Global Consent")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Global Consent")
    @Test()
    public void createGlobalAccountsConsentTest() {
        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/globalConsent/createGlobalConsent.json")).
                when().post();

        globalAccountsConsentId = response.then().statusCode(201).extract().path("consentId");

        response.then().
                header("X-Request-ID", equalTo(uuid)).
                header("Location", equalTo(basePath + "/" + globalAccountsConsentId)).
                body("consentStatus", equalTo("received")).
                body("consentId", notNullValue());
    }

    /**
     * Consent status
     */

    @Features("2C. Consents Service - Global Consent")
    @Stories({"2. Consent Status Request"})
    @TestCaseId("Consent Status Request - Get the Status Global Consent")
    @Test(dependsOnMethods = {"createGlobalAccountsConsentTest"})
    public void getGlobalAccountsConsentStatusTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", globalAccountsConsentId).
                when().get("{consentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("consentStatus", equalTo("received"));
    }

    /**
     * Consent details
     */

    @Features("2C. Consents Service - Global Consent")
    @Stories({"3. Consent Details Request"})
    @TestCaseId("Consent Details Request - Get Global Consent Details")
    @Test(dependsOnMethods = {"createGlobalAccountsConsentTest"})
    public void getGlobalAccountsConsentDetailsTest() {

        Map json = convertFileToHashMap("src/main/resources/consent/globalConsent/createGlobalConsent.json");
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", globalAccountsConsentId).
                when().get("{consentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("access",equalTo(json.get("access"))).
                body("consentStatus", equalTo("received")).
                body("recurringIndicator",equalTo(json.get("recurringIndicator"))).
                body("frequencyPerDay",equalTo(Integer.valueOf(json.get("frequencyPerDay").toString()))).
                body("validUntil",equalTo(json.get("validUntil")));
    }

    @Features("2C. Consents Service - Global Consent")
    @Stories({"4. Delete Consent"})
    @TestCaseId("Delete Consent - Revoke Global Consent")
    @Test(alwaysRun = true, dependsOnMethods = {"createGlobalAccountsConsentTest", "getGlobalAccountsConsentStatusTest", "getGlobalAccountsConsentDetailsTest"})
    public void deleteGlobalAccountsConsentTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).pathParam("consentId", globalAccountsConsentId).
                when().delete("{consentId}").
                then().statusCode(204).
                header("X-Request-ID", equalTo(uuid));

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", globalAccountsConsentId).
                when().get("{consentId}/status").
                then().statusCode(200).
                body("consentStatus", equalTo("terminatedByTpp"));
    }
}
