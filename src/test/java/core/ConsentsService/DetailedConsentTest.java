package core.ConsentsService;

import core.base.TestBase;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.util.Map;

import static core.helpers.Actions.convertFileToHashMap;
import static core.helpers.Actions.uuid;
import static io.restassured.RestAssured.given;
import static io.restassured.module.jsv.JsonSchemaValidator.matchesJsonSchema;
import static org.hamcrest.Matchers.*;

/**
 * Created by Sergey.Syomin on 12/4/2018.
 */

@Listeners({TestExecutor.class})
public class DetailedConsentTest extends TestBase{

    private String uuid = uuid();
    private static String PSUId = "aspsp2";
    private String consentIdAccountDetailsBalancesTransactions = "";
    private String consentIdAccountDetails = "";
    private String consentIdAccountDetailsBalances = "";
    private String consentIdAccountDetailsTransactions = "";
    private Response response;
    private String basePath;

    @BeforeClass
    public void setup() {
        basePath = "/v1/consents";
        RestAssured.basePath = basePath;
    }

    @BeforeMethod
    public void setPath(){
        setup();
    }

    /**
     * Consent creation
     */

    @Features("2B. Consents Service - Detailed Consent")
    @Stories("1. Create Consent")
    @TestCaseId("Create Consent - Create Consent for account details, balances, transactions access")
    @Test()
    public void createConsentAccountDetailsBalancesTransactionsAccessTest() {
        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalancesTransactions.json")).
                when().post().
                then().statusCode(201).
                extract().response();

        consentIdAccountDetailsBalancesTransactions = response.then().extract().path("consentId");

        response.then().
                header("X-Request-ID", equalTo(uuid)).
                header("Location", equalTo(basePath + "/" + consentIdAccountDetailsBalancesTransactions)).
                body("consentStatus", equalTo("received")).
                body("consentId", notNullValue());
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Consent for account details access only")
    @Test()
    public void createConsentAccountDetailsAccessOnlyTest() {
        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentAccountDetailsAccessOnly.json")).
                when().post().
                then().statusCode(201).extract().response();

        consentIdAccountDetails = response.then().extract().path("consentId");

        response.then().
                header("X-Request-ID", equalTo(uuid)).
                body("consentStatus", equalTo("received")).
                body("consentId", notNullValue());
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Consent for account details, balances access")
    @Test()
    public void createConsentAccountDetailsBalancesAccessTest() {
        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalances.json")).
                when().post().
                then().statusCode(201).extract().response();

        consentIdAccountDetailsBalances = response.then().extract().path("consentId");

        response.then().
                header("X-Request-ID", equalTo(uuid)).
                body("consentStatus", equalTo("received")).
                body("consentId", notNullValue());
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Consent for account details, transactions access")
    @Test()
    public void createConsentAccountDetailsTransactionsAccessTest() {
        uuid = uuid();

        response = given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentAccountDetailsTransactions.json")).
                when().post().
                then().statusCode(201).extract().response();

        consentIdAccountDetailsTransactions = response.then().extract().path("consentId");

        response.then().
                header("X-Request-ID", equalTo(uuid)).
                body("consentStatus", equalTo("received")).
                body("consentId", notNullValue());
    }

    //schema for creation

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Consent for account details, balances, transactions access the Schema Conform Test")
    @Test()
    public void createConsentAccountDetailsBalancesTransactionsSchemaConformTest() {
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalancesTransactions.json")).
                when().post().
                then().statusCode(201).
                body(matchesJsonSchema(new File("src/main/resources/schemas/consent/CreateConsent.json")));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Consent for Account Details Access Only the Schema Conform Test")
    @Test()
    public void createConsentAccountDetailsAccessOnlySchemaConformTest() {
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentAccountDetailsAccessOnly.json")).
                when().post().
                then().statusCode(201).
                body(matchesJsonSchema(new File("src/main/resources/schemas/consent/CreateConsent.json")));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Consent for account details, balances access the Schema Conform Test")
    @Test()
    public void createConsentAccountDetailsBalancesSchemaConformTest() {
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalances.json")).
                when().post().
                then().statusCode(201).
                body(matchesJsonSchema(new File("src/main/resources/schemas/consent/CreateConsent.json")));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"1. Create Consent"})
    @TestCaseId("Create Consent - Create Consent for account details, transactions access the Schema Conform Test")
    @Test()
    public void createConsentAccountDetailsTransactionsSchemaConformTest() {
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                body(new File("src/main/resources/consent/detailedConsent/createConsentAccountDetailsTransactions.json")).
                when().post().
                then().statusCode(201).
                body(matchesJsonSchema(new File("src/main/resources/schemas/consent/CreateConsent.json")));
    }

    /**
     * Consent status
     */

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"2. Consent Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of the Consent for account details, balances, transactions access")
    @Test(dependsOnMethods = {"createConsentAccountDetailsBalancesTransactionsAccessTest"})
    public void getConsentStatusAccountDetailsBalancesTransactionsTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetailsBalancesTransactions).
                when().get("{consentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("consentStatus", equalTo("received"));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"2. Consent Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of the Consent for account details access only")
    @Test(dependsOnMethods = {"createConsentAccountDetailsAccessOnlyTest"})
    public void getConsentStatusAccountDetailsAccessOnlyTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetails).
                when().get("{consentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("consentStatus", equalTo("received"));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"2. Consent Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of the Consent for account details, balances access")
    @Test(dependsOnMethods = {"createConsentAccountDetailsBalancesAccessTest"})
    public void getConsentStatusAccountDetailsBalancesTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetailsBalances).
                when().get("{consentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("consentStatus", equalTo("received"));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"2. Consent Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of the Consent for account details, transactions access")
    @Test(dependsOnMethods = {"createConsentAccountDetailsTransactionsAccessTest"})
    public void getConsentStatusAccountDetailsTransactionsTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetailsTransactions).
                when().get("{consentId}/status").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("consentStatus", equalTo("received"));
    }

    //schema for consent status

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"2. Consent Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of the Consent for account details, balances, transactions access the Schema Conform Test")
    @Test(dependsOnMethods = {"createConsentAccountDetailsBalancesTransactionsAccessTest"})
    public void getConsentStatusAccountDetailsBalancesTransactionsSchemaConformTest(){
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetailsBalancesTransactions).
                when().get("{consentId}/status").
                then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/consent/ConsentStatus.json")));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"2. Consent Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of the Consent for account details access only the Schema Conform Test")
    @Test(dependsOnMethods = {"createConsentAccountDetailsAccessOnlyTest"})
    public void getConsentStatusAccountDetailsSchemaConformTest(){
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetails).
                when().get("{consentId}/status").
                then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/consent/ConsentStatus.json")));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"2. Consent Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of the Consent for account details, balances access the Schema Conform Test")
    @Test(dependsOnMethods = {"createConsentAccountDetailsBalancesAccessTest"})
    public void getConsentStatusAccountDetailsBalancesSchemaConformTest(){
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetailsBalances).
                when().get("{consentId}/status").
                then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/consent/ConsentStatus.json")));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"2. Consent Status Request"})
    @TestCaseId("Consent Status Request - Get the Status of the Consent for account details, transactions access the Schema Conform Test")
    @Test(dependsOnMethods = {"createConsentAccountDetailsTransactionsAccessTest"})
    public void getConsentStatusAccountDetailsTransactionsSchemaConformTest(){
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetailsTransactions).
                when().get("{consentId}/status").
                then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/consent/ConsentStatus.json")));
    }

    /**
     * Consent details
     */

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"3. Consent Details Request"})
    @TestCaseId("Consent Details Request - Get Content of the Consent for account details, balances, transactions access")
    @Test(dependsOnMethods = {"createConsentAccountDetailsBalancesTransactionsAccessTest"})
    public void getDetailsConsentStatusAccountDetailsBalancesTransactionsTest() {

        Map json = convertFileToHashMap("src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalancesTransactions.json");
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetailsBalancesTransactions).
                when().get("{consentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("accounts",equalTo(json.get("balances"))).
                body("balances",equalTo(json.get("balances"))).
                body("transactions",equalTo(json.get("transactions"))).
                body("consentStatus", equalTo("received")).
                body("recurringIndicator",equalTo(true)).
                body("frequencyPerDay",equalTo(40)).
                body("validUntil",equalTo(json.get("validUntil")));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"3. Consent Details Request"})
    @TestCaseId("Consent Details Request - Get Content of the Consent for account details access only")
    @Test(dependsOnMethods = {"createConsentAccountDetailsAccessOnlyTest"})
    public void getDetailsConsentStatusAccountDetailsTest() {

        Map json = convertFileToHashMap("src/main/resources/consent/detailedConsent/createConsentAccountDetailsAccessOnly.json");
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetails).
                when().get("{consentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("accounts",equalTo(json.get("accounts"))).
                body("consentStatus", equalTo("received")).
                body("recurringIndicator",equalTo(true)).
                body("frequencyPerDay",equalTo(10)).
                body("validUntil",equalTo(json.get("validUntil")));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"3. Consent Details Request"})
    @TestCaseId("Consent Details Request - Get Content of the Consent for account details, balances access")
    @Test(dependsOnMethods = {"createConsentAccountDetailsBalancesAccessTest"})
    public void getDetailsConsentStatusAccountDetailsBalancesTest() {

        Map json = convertFileToHashMap("src/main/resources/consent/detailedConsent/createConsentAccountDetailsBalances.json");
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetailsBalances).
                when().get("{consentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("accounts",equalTo(json.get("balances"))).
                body("balances",equalTo(json.get("balances"))).
                body("consentStatus", equalTo("received")).
                body("recurringIndicator",equalTo(true)).
                body("frequencyPerDay",equalTo(40)).
                body("validUntil",equalTo(json.get("validUntil")));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"3. Consent Details Request"})
    @TestCaseId("Consent Details Request - Get Content of the Consent for account details, transactions access")
    @Test(dependsOnMethods = {"createConsentAccountDetailsTransactionsAccessTest"})
    public void getDetailsConsentStatusAccountDetailsTransactionsTest() {

        Map json = convertFileToHashMap("src/main/resources/consent/detailedConsent/createConsentAccountDetailsTransactions.json");

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").
                pathParam("consentId", consentIdAccountDetailsTransactions).
                when().get("{consentId}").
                then().statusCode(200).
                header("X-Request-ID", equalTo(uuid)).
                body("accounts",equalTo(json.get("transactions"))).
                body("transactions",equalTo(json.get("transactions"))).
                body("consentStatus", equalTo("received")).
                body("recurringIndicator",equalTo(true)).
                body("frequencyPerDay",equalTo(10)).
                body("validUntil",equalTo(json.get("validUntil")));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"3. Consent Details Request"})
    @TestCaseId("Consent Details Request - Get Content of the Consent for account details, balances, transactions access the Schema Conform Test")
    @Test(dependsOnMethods = {"createConsentAccountDetailsBalancesTransactionsAccessTest","getDetailsConsentStatusAccountDetailsBalancesTransactionsTest"})
    public void getDetailsConsentStatusAccountDetailsBalancesTransactionsSchemaConformTest(){
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetailsBalancesTransactions).
                when().get("{consentId}").
                then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/consent/ConsentDetails.json")));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"3. Consent Details Request"})
    @TestCaseId("Consent Details Request - Get Content of the Consent for account details access only the Schema Conform Test")
    @Test(dependsOnMethods = {"createConsentAccountDetailsAccessOnlyTest","getDetailsConsentStatusAccountDetailsTest"})
    public void getDetailsConsentStatusAccountDetailsSchemaConformTest(){
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetails).
                when().get("{consentId}").
                then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/consent/ConsentDetails.json")));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"3. Consent Details Request"})
    @TestCaseId("Consent Details Request - Get Content of the Consent for account details, balances access the Schema Conform Test")
    @Test(dependsOnMethods = {"createConsentAccountDetailsBalancesAccessTest","getDetailsConsentStatusAccountDetailsBalancesTest"})
    public void getDetailsConsentStatusAccountDetailsBalancesSchemaConformTest(){
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetailsBalances).
                when().get("{consentId}").
                then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/consent/ConsentDetails.json")));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"3. Consent Details Request"})
    @TestCaseId("Consent Details Request - Get Content of the Consent for account details, transactions access the Schema Conform Test")
    @Test(dependsOnMethods = {"createConsentAccountDetailsTransactionsAccessTest","getDetailsConsentStatusAccountDetailsTransactionsTest"})
    public void getDetailsConsentStatusAccountDetailsTransactionsSchemaConformTest(){
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetailsTransactions).
                when().get("{consentId}").
                then().statusCode(200).
                body(matchesJsonSchema(new File("src/main/resources/schemas/consent/ConsentDetails.json")));
    }

    /**
     * Consent deletion
     */

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"4. Delete Consent"})
    @TestCaseId("Delete Consent - Revoke Consent for account details, balances, transactions access by PSU")
    @Test(dependsOnMethods = {"createConsentAccountDetailsBalancesTransactionsAccessTest", "getConsentStatusAccountDetailsBalancesTransactionsTest", "getDetailsConsentStatusAccountDetailsBalancesTransactionsTest", "getDetailsConsentStatusAccountDetailsBalancesTransactionsSchemaConformTest"})
    public void deleteConsentAccountDetailsBalancesTransactionsTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).pathParam("consentId", consentIdAccountDetailsBalancesTransactions).
                when().delete("{consentId}").
                then().statusCode(204).
                header("X-Request-ID", equalTo(uuid));

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetailsBalancesTransactions).
                when().get("{consentId}/status").
                then().statusCode(200).
                body("consentStatus", equalTo("terminatedByTpp"));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"4. Delete Consent"})
    @TestCaseId("Delete Consent - Revoke Consent for account details access only by PSU")
    @Test(dependsOnMethods = {"createConsentAccountDetailsAccessOnlyTest", "getConsentStatusAccountDetailsAccessOnlyTest","getDetailsConsentStatusAccountDetailsTest", "getDetailsConsentStatusAccountDetailsSchemaConformTest"})
    public void deleteConsentAccountDetailsTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).pathParam("consentId", consentIdAccountDetails).
                when().delete("{consentId}").
                then().statusCode(204).
                header("X-Request-ID", equalTo(uuid));

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetails).
                when().get("{consentId}/status").
                then().statusCode(200).
                body("consentStatus", equalTo("terminatedByTpp"));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"4. Delete Consent"})
    @TestCaseId("Delete Consent - Revoke Consent for account details, balances access by PSU")
    @Test(dependsOnMethods = {"createConsentAccountDetailsBalancesAccessTest", "getConsentStatusAccountDetailsBalancesTest","getDetailsConsentStatusAccountDetailsBalancesTest", "getDetailsConsentStatusAccountDetailsBalancesSchemaConformTest"})
    public void deleteConsentAccountDetailsBalancesTest() {
        uuid= uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).pathParam("consentId", consentIdAccountDetailsBalances).
                when().delete("{consentId}").
                then().statusCode(204).
                header("X-Request-ID", equalTo(uuid));

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetailsBalances).
                when().get("{consentId}/status").
                then().statusCode(200).
                body("consentStatus", equalTo("terminatedByTpp"));
    }

    @Features("2B. Consents Service - Detailed Consent")
    @Stories({"4. Delete Consent"})
    @TestCaseId("Delete Consent - Revoke Consent for account details, transactions access by PSU")
    @Test(dependsOnMethods = {"createConsentAccountDetailsTransactionsAccessTest", "getConsentStatusAccountDetailsTransactionsTest","getDetailsConsentStatusAccountDetailsTransactionsTest", "getDetailsConsentStatusAccountDetailsTransactionsSchemaConformTest"})
    public void deleteConsentAccountDetailsTransactionsTest() {
        uuid = uuid();
        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).pathParam("consentId", consentIdAccountDetailsTransactions).
                when().delete("{consentId}").
                then().statusCode(204).
                header("X-Request-ID", equalTo(uuid));

        given().header("X-Request-ID", uuid).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consentIdAccountDetailsTransactions).
                when().get("{consentId}/status").
                then().statusCode(200).
                body("consentStatus", equalTo("terminatedByTpp"));
    }


    //@Test(enabled = false)
    private static void getConsentDetails() {

        String consent = "c4e3fdcd-431c-480f-8363-2c5713250354";

        Map response = given().header("X-Request-ID", uuid()).header("PSU-ID", PSUId).contentType("application/json").pathParam("consentId", consent).
                when().get("{consentId}").
                then().statusCode(200).extract().path("$");

        System.out.println(response);
    }

}
