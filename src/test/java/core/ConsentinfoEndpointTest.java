package core;

import core.base.TestBase;
import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;

import java.util.Arrays;
import java.util.Map;

import static core.helpers.Actions.convertFileToHashMap;
import static core.helpers.Actions.getValidConsent;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;


public class ConsentinfoEndpointTest extends TestBase {
    private String consentIdAccountDetails;
    private String PSUId="aspsp2";
    private String basePath;
    private String consentLocation = "src/main/resources/authorization/createConsent.json";

    @BeforeClass
    public void setup() {

        consentIdAccountDetails = getValidConsent(PSUId, consentLocation,
                "src/main/resources/authorization/passwordInit2.json");

    }

    @BeforeMethod
    public void setPath(){
        this.basePath = "/v1/bank/consents/";
        RestAssured.basePath = this.basePath;
    }

    @Features("6. Consents info endpoint")
    @Stories({"Get consent info"})
    @Test()
    public void createAvailableAccountsConsentPastDateNegativeTest() {
        Map json = convertFileToHashMap(consentLocation);
        given().contentType("application/json").
                get(consentIdAccountDetails).
                then().statusCode(200).
//                body("access", contains(json.get("access"))).
                body("accounts",equalTo(json.get("transactions"))).
                body("transactions",equalTo(json.get("transactions"))).
                body("balances", equalTo(json.get("balances"))).
                body("consentStatus", equalTo("valid")).
                body("recurringIndicator",equalTo(json.get("recurringIndicator"))).
                body("frequencyPerDay",equalTo(Integer.valueOf(json.get("frequencyPerDay").toString()))).
                body("validUntil",equalTo(json.get("validUntil")));
    }

}
