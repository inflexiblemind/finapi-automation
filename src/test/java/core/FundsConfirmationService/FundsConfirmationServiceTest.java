package core.FundsConfirmationService;

import core.base.TestBase;
import io.restassured.RestAssured;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.Features;
import ru.yandex.qatools.allure.annotations.Stories;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.File;
import java.util.ArrayList;

import static core.helpers.Actions.date;
import static core.helpers.Actions.getValidConsent;
import static core.helpers.Actions.uuid;
import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

public class FundsConfirmationServiceTest extends TestBase {
    private String PSUId="aspsp2";
    private String basePath;
    private String uuid;
    private String consentIdBalances;
    private String ACCOUNT_ID = "77777-999999999";
    private Integer balance;

    @BeforeClass
    public void setup() {
        consentIdBalances = getValidConsent(PSUId, "src/main/resources/fundsConfirmation/createConsentAccountDetailsBalances.json","src/main/resources/authorization/passwordInit2.json");
        ArrayList<Float> balances = given().pathParam("account-id", ACCOUNT_ID).header("X-Request-ID", uuid()).
                header("Consent-ID", consentIdBalances).
                when().get("v1/accounts/{account-id}/balances").
                then().statusCode(200).
                extract().
                path("balances.balanceAmount.amount");
         balance = balances.get(0).intValue();
    }

    @BeforeMethod
    public void setPath() {
        this.basePath = "/v1/funds-confirmations";
        RestAssured.basePath = this.basePath;
    }

    @Features("8. Funds Confirmation Service")
    @Stories({"1. Funds Confirmation Service - Valid amount"})
    @Test()
    public void fundsConfirmationValidAmountTest() {

        uuid = uuid();

        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSUId).header("date", date()).contentType("application/json").
                body("{\"account\": {\"iban\":\"DE89370400440532013000\"},\"instructedAmount\": {\"currency\": \"USD\",\"amount\": "+balance+"}}").
                when().post().
                then().
                header("X-Request-ID", equalTo(uuid)).
                body("fundsAvailable", equalTo(true));

    }

    @Features("8. Funds Confirmation Service")
    @Stories({"1.  Positive Funds Confirmation - Excess amount"})
    @Test()
    public void fundsConfirmationServiceWithExcessAmountTest() {

        uuid = uuid();
        balance = balance+10;
        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSUId).header("date", date()).contentType("application/json").
                body("{\"account\": {\"iban\":\"DE89370400440532013000\"},\"instructedAmount\": {\"currency\": \"USD\",\"amount\": "+balance+"}}").
                when().post().
                then().
                header("X-Request-ID", equalTo(uuid)).
                body("fundsAvailable", equalTo(false));

    }

    @Features("8. Funds Confirmation Service")
    @Stories({"1. Funds Confirmation Service - Negative Amount"})
    @Test()
    public void fundsConfirmationServiceWithNegativeAmountTest() {

        uuid = uuid();
        balance = balance-balance-20;
        given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSUId).header("date", date()).contentType("application/json").
                body("{\"account\": {\"iban\":\"DE89370400440532013000\"},\"instructedAmount\": {\"currency\": \"USD\",\"amount\": "+balance+"}}").
                when().post().
                then().
                header("X-Request-ID", equalTo(uuid)).
                body("fundsAvailable", equalTo(true));

    }


}
