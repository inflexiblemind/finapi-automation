package core.helpers;
import java.sql.*;


/**
 * Created by Sergey.Syomin on 10/19/2018.
 */
public class PostgreSQLHelper {

    private static String JDBCLink = "";
    private static String username = "";
    private static String password = "";
    protected static Connection connection = null;

    public static void connectToDB(){

        System.out.println("PostgreSQLHelper JDBC Connection Establishing...");
        try {
            Class.forName("org.postgresql.Driver");
        }
        catch (ClassNotFoundException e) {
            System.out.println("PostgreSQLHelper JDBC Driver is NOT found!");
            e.printStackTrace();
            return;
        }
        System.out.println("PostgreSQLHelper JDBC Driver Registered!");
        try {
            connection = DriverManager.getConnection(JDBCLink, username, password);
        } catch (SQLException e) {
            System.out.println("Connection Failed!");
            e.printStackTrace();
            return;
        }

        if (connection != null) {
            System.out.println("Connection to DB is established now!");
        } else {
            System.out.println("Failed to make connection!");
        }
    }

    public static Connection getConnection(){
        return connection;
    }

    public static void disconnectFromDB() {
        try {
            connection.close();
            System.out.println("Disconnection from DB successful.");
        } catch (SQLException e) {
            System.out.println("Disconnection Failed! Check output console");
            e.printStackTrace();
            return;
        }
    }

    private static Statement createStatement() throws SQLException {
        System.out.println("Creating statement…");
        return connection.createStatement();
    }

    public static int executeUpdate(String query) {
        try {
            System.out.println("Executing Update: " + query);
            int count = createStatement().executeUpdate(query);
            System.out.println("--- " + Integer.toString(count) + " records was(were) updated ---");
            return count;
        }
        catch (SQLException e) {
            System.out.println("SQL execution Failed! Check output console");
            e.printStackTrace();
            return 0;
        }
    }

    public static ResultSet executeQuery(String query) {

        try {
            System.out.println("Executing Query: " + query);
            return createStatement().executeQuery(query);
        }
        catch (SQLException e) {
            System.out.println("SQL execution Failed! Check output console");
            e.printStackTrace();
            return null;
        }
    }

    public static String getStringResult (String query) {
        try {
            ResultSet rs = executeQuery(query);
            while (rs.next()) {
                return rs.getString(1);
            }
        }
        catch (SQLException e) {
            System.out.println("SQL execution Failed! Check output console");
            e.printStackTrace();
            return null;
        }
        return null;
    }
}



