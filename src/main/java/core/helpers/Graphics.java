package core.helpers;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;

import static java.lang.System.*;

/**
 * Created by Sergey.Syomin on 10/10/2018.
 */
public class Graphics {

    public static void printFile(String path) {
try {
        out.println(new String(Files.readAllBytes(Paths.get(path))));
    }
    catch (IOException e) {
        e.printStackTrace();
        }
    }
}
