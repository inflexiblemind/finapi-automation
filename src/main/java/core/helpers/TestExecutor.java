package core.helpers;

import io.restassured.RestAssured;
import io.restassured.filter.log.LogDetail;
import io.restassured.filter.log.RequestLoggingFilter;
import io.restassured.filter.log.ResponseLoggingFilter;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.reporters.ExitCodeListener;
import ru.yandex.qatools.allure.annotations.Attachment;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

/**
 * Created by Sergey.Syomin on 10/10/2018.
 */
public class TestExecutor extends ExitCodeListener {

    private static int testCount;

    private ByteArrayOutputStream request = new ByteArrayOutputStream();
    private ByteArrayOutputStream response = new ByteArrayOutputStream();

    private PrintStream requestVar = new PrintStream(request, true);
    private PrintStream responseVar = new PrintStream(response, true);

    @Override
    public void onStart(ITestContext iTestContext) {
        super.onStart(iTestContext);
        RestAssured.filters(new ResponseLoggingFilter(LogDetail.ALL, responseVar),
                new RequestLoggingFilter(LogDetail.ALL, requestVar));
    }

    @Override
    public void onTestStart(ITestResult result) {
        try {
            sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        super.onTestStart(result);
        System.out.println(++testCount + ". " + result.getMethod().getTestClass().getName() +
                "-" + result.getMethod().getMethodName() + " started at " + new Date());
    }

    @Override
    public void onTestFailure(ITestResult result) {
        super.onTestFailure(result);
        logRequest(request);
        logResponse(response);
        System.out.println(result.getMethod().getTestClass().getName() + "-" + result.getMethod().getMethodName() +
                " failed at " + new Date());
        System.out.println("Execution time: " + (result.getEndMillis() - result.getStartMillis()) + " milliseconds");
        result.getThrowable().printStackTrace();
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        super.onTestSuccess(result);
        logRequest(request);
        logResponse(response);
        try {
            sleep(500);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        System.out.println(result.getMethod().getTestClass().getName() + "-" + result.getMethod().getMethodName() +
                " PASSED at " + new Date());
        System.out.println("Execution time: " + (result.getEndMillis() - result.getStartMillis()) + " milliseconds");
    }

    private String millisToMinSec(long millis){
        return String.format("%02d min : %02d sec",
                TimeUnit.MILLISECONDS.toMinutes(millis),
                TimeUnit.MILLISECONDS.toSeconds((millis) -
                        TimeUnit.MINUTES.toMillis(TimeUnit.MILLISECONDS.toMinutes(millis))));
    }

    @Attachment(value = "Request")
    public byte[] logRequest(ByteArrayOutputStream stream) {
        return attach(stream);
    }

    @Attachment(value = "Response")
    public byte[] logResponse(ByteArrayOutputStream stream) {
        return attach(stream);
    }

    public byte[] attach(ByteArrayOutputStream log) {
        byte[] array = log.toByteArray();
        log.reset();
        return array;
    }
}
