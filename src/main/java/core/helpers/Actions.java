package core.helpers;

import com.fasterxml.uuid.Generators;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import com.google.gson.reflect.TypeToken;
import core.base.TestBase;
import io.restassured.response.Response;
import org.testng.Assert;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;

import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.JSON;

/**
 * Created by Sergey.Syomin on 10/10/2018.
 */

public class Actions extends TestBase{

    public static Response getMethodResponseBody(String url){
        Response response = given().auth().oauth2(token).
                when().get(HOST + url + "/").
                then().contentType(JSON).
                extract().response();
        return response;
    }

    public static Response postMethodResponseBody(String url, HashMap<String, Object> inputMap) {
        Response response = given().auth().oauth2(token).body(inputMap).contentType("application/json").
                when().post(url).
                then().extract().response();
        return response;
    }

    public static JsonObject convertFileToJSON (String fileName) {

        // Read from File to String
        JsonObject jsonObject;

        try {
            JsonParser parser = new JsonParser();
            JsonElement jsonElement = parser.parse(new FileReader(fileName));
            jsonObject = jsonElement.getAsJsonObject();
            return jsonObject;
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static HashMap convertFileToHashMap(String fileName) {

        //HashMap<String, Object> map = new Gson().fromJson(convertFileToJSON(fileName), HashMap.class);

        HashMap<String, Object> map = new Gson().fromJson(convertFileToJSON(fileName), new TypeToken<HashMap<String, Object>>(){}.getType());

        return map;
    }

    // This method is introduced for easy readability
    public static HashMap map(String fileName) {
        return convertFileToHashMap(fileName);
    }

    public static String getClientIP() {
        try {
            InetAddress IP = InetAddress.getLocalHost();
            String ip = IP.getHostAddress();
            System.out.println("IP of the Client is := " + ip + "\n");
            return ip;
        }
        catch (UnknownHostException e) {
            e.printStackTrace();
        }
        return "127.0.0.1";
    }

    public static String uuid() {
        return  Generators.randomBasedGenerator().generate().toString();
    }

    public static String date() {
        DateFormat targetFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss z");
        return  targetFormat.format(new Date());
    }

    public static String getConsent (String psu_id, String fileLocation) {

        return  given().header("X-Request-ID", uuid()).header("PSU-ID", psu_id).contentType("application/json").
                body(new File(fileLocation)).
                when().post(HOST + "/v1/consents").
                then().statusCode(201).extract().body().path("consentId");
    }

    public static String getPayment (String psu_id, String fileLocation) {

        return  given().header("X-Request-ID", uuid()).header("psu-ip-address", PSU_IP).header("PSU-ID", psu_id).header("date", date()).contentType("application/json").
                body(new File(fileLocation)).
                when().post(HOST + "/v1/payments/sepa-credit-transfers").
                then().statusCode(201).extract().body().path("paymentId");
    }

    public static String getRecurrentPayment (String psu_id, String fileLocation) {

        return  given().header("X-Request-ID", uuid()).header("psu-ip-address", PSU_IP).header("PSU-ID", psu_id).header("date", date()).contentType("application/json").
                body(new File(fileLocation)).
                when().post(HOST + "/v1/periodic-payments/sepa-credit-transfers").
                then().statusCode(201).extract().body().path("paymentId");
    }


    public static String getDeletedConsent (String psu_id, String consentID) {

         given().header("X-Request-ID", uuid()).header("PSU-ID", psu_id).
                pathParam("consentId", consentID).
                when().delete(HOST + "/v1/consents/{consentId}").
                then().statusCode(204);
        return consentID;
    }

    public static String getValidConsent (String psu_id, String fileLocation, String passwordInitFile) {

        String consent = getConsent(psu_id, fileLocation);
        System.out.println("Consent " + consent + " was successfully generated");
        Assert.assertEquals(authorizeConsent(psu_id, consent, passwordInitFile), "finalised", "SCA status is not FINALIZED");
        System.out.println("Consent " + consent + " was successfully authorized\n\n");
        return  consent;
    }

    public static String getValidPayment (String psu_id, String fileLocation, String passwordInitFile) {

        String payment = getPayment(psu_id, fileLocation);
        System.out.println("Payment " + payment + " was successfully generated");
        Assert.assertEquals(authorizePayment(psu_id, payment, passwordInitFile), "finalised", "SCA status is not FINALIZED");
        System.out.println("Payment " + payment + " was successfully authorized\n\n");
        return  payment;
    }

    public static String getValidRecurrentPayment (String psu_id, String fileLocation, String passwordInitFile) {

        String payment = getRecurrentPayment(psu_id, fileLocation);
        System.out.println("Payment " + payment + " was successfully generated");
        Assert.assertEquals(authorizeRecurrentPayment(psu_id, payment, passwordInitFile), "finalised", "SCA status is not FINALIZED");
        System.out.println("Payment " + payment + " was successfully authorized\n\n");
        return  payment;
    }

    public static String getAvailableAccountsConsent (String psu_id) {
        return getConsent(psu_id, "src/main/resources/consent/availableAccounts/createAvailableAccountsConsent.json");
    }

    public static String  authorizeConsent(String PSU_ID, String consentId){
        return authorizeConsent(PSU_ID, consentId, null);
    }


        public static String authorizeConsent(String PSU_ID, String consentId, String passwordInitFile){
        String uuid = uuid();
        String url = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                post(HOST + "/v1/consents/" + consentId + "/authorisations").
                then().statusCode(200).
                extract().path("_links.updatePsuAuthentication");
        url = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File(passwordInitFile == null ? "src/main/resources/authorization/passwordInit2.json" : passwordInitFile)).
                put(HOST + url).
                then().statusCode(200).
                extract().path("_links.selectAuthenticationMethod");
        url = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/authorization/authenticationMethodInit.json")).
                put(HOST + url).
                then().statusCode(200).
                extract().path("_links.authoriseTransaction");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat date = new SimpleDateFormat("ddmm");
        String scaStatus = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).contentType("application/json").
                body("{  \"scaAuthenticationData\": \""+date.format(cal.getTime())+"\" } ").
                put(HOST + url).
                then().statusCode(200).
                extract().path("scaStatus");
        return scaStatus;
    }

    public static String authorizePayment(String PSU_ID, String paymentId, String passwordInitFile){
        String uuid = uuid();
        String url = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                post(HOST + "/v1/payments/sepa-credit-transfers/" + paymentId + "/authorisations").
                then().statusCode(200).
                extract().path("_links.updatePsuAuthentication");
        url = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File(passwordInitFile == null ? "src/main/resources/authorization/passwordInit2.json" : passwordInitFile)).
                put(HOST + url).
                then().statusCode(200).
                extract().path("_links.selectAuthenticationMethod");
        url = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/authorization/authenticationMethodInit.json")).
                put(HOST + url).
                then().statusCode(200).
                extract().path("_links.authoriseTransaction");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat date = new SimpleDateFormat("ddmm");
        String scaStatus = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).contentType("application/json").
                body("{  \"scaAuthenticationData\": \""+date.format(cal.getTime())+"\" } ").
                put(HOST + url).
                then().statusCode(200).
                extract().path("scaStatus");
        return scaStatus;
    }

    public static String authorizeRecurrentPayment(String PSU_ID, String paymentId, String passwordInitFile){
        String uuid = uuid();
        String url = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).
                post(HOST + "/v1/periodic-payments/sepa-credit-transfers/" + paymentId + "/authorisations").
                then().statusCode(200).
                extract().path("_links.updatePsuAuthentication");
        url = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File(passwordInitFile == null ? "src/main/resources/authorization/passwordInit2.json" : passwordInitFile)).
                put(HOST + url).
                then().statusCode(200).
                extract().path("_links.selectAuthenticationMethod");
        url = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).header("date", date()).contentType("application/json").
                body(new File("src/main/resources/authorization/authenticationMethodInit.json")).
                put(HOST + url).
                then().statusCode(200).
                extract().path("_links.authoriseTransaction");
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat date = new SimpleDateFormat("ddmm");
        String scaStatus = given().header("X-Request-ID", uuid).header("psu-ip-address", PSU_IP).header("PSU-ID", PSU_ID).contentType("application/json").
                body("{  \"scaAuthenticationData\": \""+date.format(cal.getTime())+"\" } ").
                put(HOST + url).
                then().statusCode(200).
                extract().path("scaStatus");
        return scaStatus;
    }
}
