package core.base;

import core.helpers.Graphics;
import core.helpers.TestExecutor;
import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.*;
import org.testng.ITestContext;

import java.io.File;

import static core.helpers.Actions.getClientIP;
import static io.restassured.RestAssured.enableLoggingOfRequestAndResponseIfValidationFails;
import static java.lang.System.out;

/**
 * Created by Sergey.Syomin on 10/08/2018.
 */

@Listeners({TestExecutor.class})
public class TestBase {

    private static final String username = "autotest";
    private static final String password = "autotest";
    protected static String token;
    protected static String WINDOWS_HOST = "http://10.9.3.220";
    protected static String LINUX_HOST = "http://localhost";
    protected static String HOST;
    private static final String GRAPHICS_FILE_LOCATION = "src/main/resources/Graphics.txt";
    public static RequestSpecification requestAuth;
    protected static String PSU_IP;

    @BeforeSuite
    public void run(){
        Graphics.printFile(GRAPHICS_FILE_LOCATION);
        out.println("@BeforeSuite called on " + getClass().getName());
        String port = System.getProperty("server.port");
        if (port == null) {
            RestAssured.port = Integer.valueOf(8081);
        } else {
            RestAssured.port = Integer.valueOf(port);
        }
        String baseHost = System.getProperty("server.host");
        String osName = System.getProperty("os.name");
        System.out.println("\nThe current system is: " + osName + "\n");
        if (osName.toLowerCase().contains("windows")) {
            HOST = WINDOWS_HOST;
            RestAssured.proxy("localhost", 8888);
        }
        else
            HOST = LINUX_HOST;
        System.out.println("\nThe HOST is : " + HOST + "\n");
        if (baseHost == null) {
            baseHost = HOST;
        }
        RestAssured.baseURI = baseHost;

        PSU_IP = getClientIP();
    }

    @BeforeTest(alwaysRun = true)
    public void setup(ITestContext context) {

        System.out.println("@BeforeTest called on " + getClass().getName());

        enableLoggingOfRequestAndResponseIfValidationFails();

        //PostgreSQLHelper.connectToDB();

/*        token = given()
                .contentType("application/x-www-form-urlencoded")
                .header("Authorization", "Basic d2ViX2FwcDo=")
                .formParam("grant_type", "password")
                .formParam("username", username)
                .formParam("password", password)
                .when().post(HOST + "/uaa/oauth/token")
                .then().statusCode(200)
                .header("Content-Type", "application/json;charset=UTF-8")
                .extract().path("access_token");

        context.setAttribute("token", token);
        token = (String) context.getAttribute("token");

        System.out.println("Token is ready: " + token);*/


        //RestAssured.authentication = oauth2(token, OAuthSignature.HEADER);
        //RequestSpecification requestAuth = new RequestSpecBuilder().setAuth(oauth2(token)).build();//oauth2(token, OAuthSignature.HEADER).build();
        //requestAuth = new RequestSpecBuilder().setAuth(oauth2(token, OAuthSignature.HEADER)).build();//oauth2(token, OAuthSignature.HEADER).build();
    }

    @AfterTest
    public void tearDown(){
    }

    @AfterSuite
    public void stop() {
        //PostgreSQLHelper.disconnectFromDB();
    }
}
